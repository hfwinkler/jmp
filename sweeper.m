% take the learning solution and sweep through the parameter space to
% understand dependence of IRFs, moments...

clear;
opengl neverselect;

%% preliminaries

mlist={'my_bgg2_learn' 'my_bgg2_RE'};
mnames={'learn' 'RE'};
styles={'r' '--b'};
widths=[2, 2];
alphalist=[1 .5];

N=11; %gridsize
parname='kappa';
parmin=0.0001;
parmax=0.95;
parscale=linspace(parmin,parmax,N);

varnames={'Y' 'I' 'C' 'L' 'V' 'pi'};
vartitles=varnames;
maxy=zeros(size(varnames));
maxcorr=0;
IRFk=[]; %shock for which to display IRF
T=50; %for IRFs
order=2;
filtering=1; %0=no filtering, 1=HP, 2=FD

%% parse parameter file to use in functions

dynare my_bgg2_parameters onlymacro savemacro;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', strtrim(tline));
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% run Dynare for every model

for i=1:length(mlist)
    model=mlist{i};
    dynare(model,'noclearall','nostrict');
    nendo=M_.endo_nbr; %number of endogenous variables
    nexo=M_.exo_nbr-2*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)
    nendolag=sum(strncmp('AUX_ENDO_LAG',cellstr(M_.endo_names),12)); %number of endogenous lagged variables appended by Dynare
    M.(model)=M_;
    oo.(model)=oo_;

    SS.(model)=cell(N,1);
    A.(model)=cell(N,1);
    B.(model)=cell(N,1);
    VV.(model)=cell(N,1);
    Loss.(model)=zeros(N,1);
    params=cell(N,1);
    eval(['refpar=' parname ';']);
    
    fprintf('Looping');
    for n=[1 1:N] %why the repeat is necessary I don't know, but it is!

        %set parameter to loop over
        eval([parname '=parscale(' num2str(n) ');']);
        set_param_value(parname,parscale(n));
        
        if any(strcmp(parname,{'gama','xi'}))
            %solve for endogenous parameters
            xstar=fsolve(@(x) funBKRk(x,[gama,xi],M_), [BK, Rk0]);
            disp(xstar);
            BK=xstar(1); Rk0=xstar(2);
            disp([parscale(n) Rk0-R0]);
        end
        
        endoparamupdate;
        for j=1:M_.param_nbr
            eval(['set_param_value(deblank(M_.param_names(j,:)),' M_.param_names(j,:) ');']);
            eval(['M_.params(j)=' M_.param_names(j,:) ';']);
        end
        
        options_.noprint=1;
        options_.order=order;
        options_.irf=0;
        options_.ar=maxcorr;
        options_.nomoments=0;
        options_.hp_filter=0;
        options_.pruning=1;
        options_.periods=1e4;
    
        %try %if Dynare produces an error at any stage we need to catch it
        oo_.dr.ghx=[];
        %resid(1);
        try
            fprintf('.');
            stream = RandStream.getGlobalStream;
            reset(stream,0);
            stoch_simul('');
            params{n}=M_.params;
            C=zeros(length(oo_.dr.state_var),nendo);
            for i=1:length(oo_.dr.state_var)
                C(i,oo_.dr.state_var(i))=1;
            end
            A.(model){n}=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
            B.(model){n}=oo_.dr.ghu(oo_.dr.inv_order_var,:);
            %make learning adjustment
            B.(model){n}=B.(model){n}(:,1:nexo);
            %stability:
            if max(abs(eig(A.(model){n})))>1
                disp(['Instability at ' parname '=' num2str(parscale(n))]);
                A.(model){n}=[];
                continue;
            end
            %variance matrix, steady state, parameters
            switch filtering
                case 0 %no filtering
                    VV.(model){n}=oo_.var;
                case 1 %HP
                    VV.(model){n}=cov(oo_.endo_simul'-hpfilter(oo_.endo_simul,1600));
                case 2 %FD
                    VV.(model){n}=cov(diff(oo_.endo_simul'));
            end
            SS.(model){n}=oo_.dr.ys;
            %welfare
            [~, m]=my_bgg2_evalpol3([],[],0,1,M_,oo_,options_);
            Loss.(model)(n)=m.Caloss;
        catch 
            disp(['Dynare error at ' parname '= ' num2str(parscale(n))]);
        end
    end
    fprintf('done.\n');
    cleanup_dyn(model);
    
end

%% plot (first-order) IRFs

for k=IRFk
    SigmaIRF=zeros(1,nexo);
    SigmaIRF(k)=1;

    v=zeros(nexo,T);
    v(:,2)=SigmaIRF;
    for i=1:length(mlist)
        model=mlist{i};
        x.(model)=zeros(M.(model).endo_nbr,T,N);
        for n=1:N
            if isempty(A.(model){n}); x.(model)(:,:,n)=NaN; continue; end
            for t=2:T
                x.(model)(:,t,n)=A.(model){n}*x.(model)(:,t-1,n)+B.(model){n}*v(:,t);
            end
        end
    end

    nrows=2;
    ncols=ceil(length(varnames)/nrows);
    figure; set(gcf,'name',['IRFs to shock #' num2str(k)]);
    for j=1:length(varnames)
        subplot(nrows,ncols,j); 
        hold on;
        for i=1; %1:length(mlist)
            model=mlist{i};
            [~,jidx]=ismember(varnames{j},M.(model).endo_names,'rows');
            surf(parscale,1:T-1,squeeze(x.(model)(jidx,2:T,:)),'FaceAlpha',alphalist(i));
        end
        title(varnames{j});
        xlim([parmin parmax]); ylim([1 T]);
    end
end
drawnow;

%return;

%% Moments

for i=1:length(mlist)
    model=mlist{i};
    
    nendo=M.(model).endo_nbr;
    V.(model)=zeros(nendo-nendolag,nendo-nendolag,N);

    for n=1:N
        if isempty(A.(model){n}) || isempty(VV.(model){n})
            V.(model)(:,:,n)=NaN; 
            continue; 
        else
            V.(model)(:,:,n)=VV.(model){n};
        end

        %normalise: diagonal to get stddev, off diagonal to get correlation
        Stddiag=sqrt(diag(V.(model)(:,:,n)));
        V.(model)(:,:,n)=V.(model)(:,:,n)./kron(Stddiag,ones(1,nendo-nendolag))./kron(Stddiag',ones(nendo-nendolag,1));
        V.(model)(:,:,n)=V.(model)(:,:,n)-eye(nendo-nendolag)+diag(Stddiag);

    end
end

set(0,'DefaultAxesColorOrder',[1 0 0;0 0 1;0 0 0]);
  
nrows=2;
ncols=ceil(length(varnames)/nrows);
h1=figure; set(h1,'name','Standard deviations, filtered');
h2=figure; set(h2,'name','Standard deviations relative to Y, filtered');
h3=figure; set(h3,'name','Correlation with Y, filtered');
plotdata1=zeros(length(mlist),N);
plotdata2=zeros(length(mlist),N);
plotdata3=zeros(length(mlist),N);
for j=1:length(varnames)
    for i=1:length(mlist)
        model=mlist{i};
        [~,jidx]=ismember(varnames{j},M.(model).endo_names,'rows');
        [~,jY]=ismember('Y',M.(model).endo_names,'rows');
        plotdata1(i,1:N)=squeeze(100*V.(model)(jidx,jidx,:));
        plotdata2(i,1:N)=squeeze(V.(model)(jidx,jidx,:)./V.(model)(jY,jY,:));
        plotdata3(i,1:N)=squeeze(V.(model)(jidx,jY,:));
    end
    
    figure(h1); subplot(nrows,ncols,j); 
    for i=1:length(mlist)
        plot(parscale,plotdata1(i,:),styles{i},'LineWidth',widths(i)); hold on;
    end
    if maxy(j)==0; maxy(j)=max(plotdata1(:))*1.1; end
    axis([min(parscale) max(parscale) 0 maxy(j)]);
    vline(refpar,'k:');
    ylabel('sd. %'); 
    title(vartitles{j});
    if j==length(varnames)
        %xlabel(parname); 
        legend(mnames,'Location','Best'); 
    end
    set(gcf,'Position',[501   324   392   577]);
    
    figure(h2); subplot(nrows,ncols,j); 
        for i=1:length(mlist)
            plot(parscale,plotdata2(i,:),styles{i},'LineWidth',widths(i)); hold on;
        end

        axis([min(parscale) max(parscale) 0 1.1*max(plotdata2(:))]);
        vline(refpar,'k:');
        ylabel('sd. rel. to output'); 
        title(vartitles{j});
        if j==length(varnames)
            %xlabel(parname); 
            legend(mnames,'Location','Best'); 
        end
        set(gcf,'Position',[501   324   392   577]);
        
    figure(h3); subplot(nrows,ncols,j); 
    for i=1:length(mlist)
        plot(parscale,plotdata3(i,:),styles{i},'LineWidth',widths(i)); hold on;
    end
    axis([min(parscale) max(parscale) -1 1]);
    vline(refpar,'k:');
    ylabel('corr.'); 
    title(vartitles{j});
    if j==length(varnames)
        %xlabel(parname); 
        legend(mnames,'Location','Best'); 
    end
    set(gcf,'Position',[501   324   392   577]);
end


%% welfare

plotdata1=zeros(length(mlist),N);
for i=1:length(mlist)
        model=mlist{i};
        plotdata1(i,:)=Loss.(model);
end
figure; 
    plot(parscale,plotdata1,'LineWidth',2); 
    axis tight;
    title('Welfare loss, %SS consumption'); 
    vline(refpar,'k:');
    