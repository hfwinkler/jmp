#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Learning about asset prices and the real economy:
\begin_inset Newline newline
\end_inset

Previous research
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Itemize
Explain growth rate learning instead of level learning
\end_layout

\begin_layout Itemize
level-learning can never produce second-order difference equations by itself,
 which limits the response to Ar(1)-type processes, unless there is another
 type of feedback
\end_layout

\begin_deeper
\begin_layout Itemize
second-order difference equations can lead to self-amplifying boom-bust
 patterns, while first-order ones will at best get amplification and propagation
, at worst dampening of reactions
\end_layout

\end_deeper
\begin_layout Itemize
Wealth effect on consumer in RBC model
\end_layout

\begin_deeper
\begin_layout Itemize
high price increases demand for consumption
\end_layout

\begin_layout Itemize
interest rate rises, investment falls, dividends rise
\end_layout

\begin_layout Itemize
positive feedback loop initially, but then negative as low capital stock
 makes dividends fall again
\end_layout

\begin_layout Itemize
problem: labour supply also falls, so in eq.
 Y, I, N all fall in an asset price boom
\end_layout

\end_deeper
\begin_layout Itemize
Firm value in matching model
\end_layout

\begin_deeper
\begin_layout Itemize
high price increases firm entry, and hence employment and output
\end_layout

\begin_layout Itemize
the 
\begin_inset Quotes eld
\end_inset

dividend
\begin_inset Quotes erd
\end_inset

 is the profit per period and firm.
 this will fall as factors of production become more expensive in the boom:
 wage and rental rate of capital rise
\end_layout

\begin_layout Itemize
so there is dampening, not amplification from dividends.
 problem: aggregate profits do rise, but per project they fall.
 the project represents something like the marginal profit per worker, and
 that falls in an expansion absent increasing returns to scale.
\end_layout

\end_deeper
\begin_layout Itemize
Stock market value entering the borrowing constraint
\end_layout

\begin_deeper
\begin_layout Itemize
high price increases capacity to borrow, which expands investment, output
 and employment
\end_layout

\begin_layout Itemize
dividends now need to be defined properly as there is debt and equity
\end_layout

\begin_layout Itemize
first attempt: only capital, DRS, debt equals capital, and pure profits
 are equity (dividends).
 Profits rise with investment, at the frictionless level they are maximal.
\end_layout

\begin_layout Itemize
second attempt: dividend policy is a choice, but dividends cannot be negative.
 It is generally impossible to have positive dividends and a binding borrowing
 constraint, because when the latter is binding it is advantageous to lower
 dividends.
 translate this into a cost of funds argument? in any case it's boring because
 when the firm is really borowing constrained dividends will be constant
 at their minimum.
\end_layout

\begin_layout Itemize
third attempt: BGG-style.
 all earnings retained until exogenous death, and aggregation.
 intertemporal loans with a limit.
 because the internal return on capital is higher than the borrowing rate
 it makes sense to lever up, and profits rise with borrowing initially.
 but at some point general equilibrium effects push down the return on capital
 until it equals the borrowing rate.
 then equity and debt become increasingly substitutable, and dividends fall
 because there is no return premium on scarce equity anymore.
 This effect can be made small through elastic labour labour supply and
 a borrowing rate that rises with investment because households have concave
 utility.
\end_layout

\begin_layout Itemize
fourth attempt: intratemporal loans, and no intertemporal debt.
 the intratemporal loans are free, and more of them means more investment.
 this lowers dividends today but raises them tomorrow.
 if the share price is about future dividends, this makes things go the
 right way: positive feedback loop.
 This setup is largely identical to Miao & Wang - should also have their
 bubbly equilibrium.
\end_layout

\begin_layout Itemize
problem: financial frictions operate on the investment margin, and we know
 from business cycle accounting that this is not so important.
 incorporate jermann-quadrini working capital constraint instead?
\end_layout

\end_deeper
\begin_layout Itemize
examine Miao&Wang model
\end_layout

\begin_layout Itemize
examine Farmer model
\end_layout

\end_inset

 This note outlines my previous two attempts on how asset price learning
 in the spirit of Adam, Marcet and Nicolini can interact with the real economy.
 The idea is to create positive feedback loops that lead to persistent deviation
s from the rational expectations equilibrium and create business-cycle style
 fluctuations.
 My first attempt, using an RBC model, had a positive feedback loop but
 odd impulse responses; the second, using a search model, couldn't get the
 positive feedback.
\end_layout

\begin_layout Section
AMN learning scheme
\end_layout

\begin_layout Standard
I am always relying on variations of the same learning scheme that comes
 from a series of papers by Albert, Marcet and Nicolini.
 I will describe it here.
\end_layout

\begin_layout Standard
An asset trades at price 
\begin_inset Formula $P_{t}$
\end_inset

 and pays dividends 
\begin_inset Formula $D_{t}$
\end_inset

.
 The agent holding the asset discounts the future using the pricing kernel
 
\begin_inset Formula $Q_{t,t+s}$
\end_inset

 between dates 
\begin_inset Formula $t$
\end_inset

 and 
\begin_inset Formula $t+s$
\end_inset

.
 Then the asset price needs to satisfy
\begin_inset Formula 
\[
P_{t}=\hat{E}_{t}Q_{t,t+1}\left(P_{t+1}+D_{t+1}\right)
\]

\end_inset

where 
\begin_inset Formula $\hat{E}$
\end_inset

 represents the agent's belief system.
 If the agent has rational expectations and believes in the transversality
 condition, then the price is just the discounted sum of dividends:
\begin_inset Formula 
\[
P_{t}=E_{t}\sum_{s=1}^{\infty}Q_{t,t+s}D_{t+s}
\]

\end_inset

But this is not the only possible belief system.
 Adam, Marcet and Nicolini provide arguments why imposing rational expectations
 in this way is a strong assumption as long as there is a slight degree
 of heterogeneity among agents.
 Instead, they propose simple learning rules to form expectations.
\end_layout

\begin_layout Enumerate
Agents form expectations about the growth rate of next period's asset price:
 
\begin_inset Formula $\hat{E}_{t}Q_{t,t+1}P_{t+1}=\hat{\mu}_{t}P_{t}$
\end_inset

.
 Their belief is updated using constant-gain learning: 
\begin_inset Formula $\hat{\mu}_{t+1}=\hat{\mu}_{t}+g\left(Q_{t-1,t}\frac{P_{t}}{P_{t-1}}-\hat{\mu}_{t}\right)$
\end_inset

.
 They have rational expectations about discounted future dividends though.
 Then the current asset price must satisfy
\begin_inset Formula 
\[
P_{t}=\frac{E_{t}Q_{t,t+1}D_{t+1}}{1-\hat{\mu}_{t}}
\]

\end_inset


\end_layout

\begin_layout Enumerate
The previous specification only stated the expectation of price growth.
 But it is possible to have a fully Bayesian formulation for this type of
 belief.
 Suppose agents think that prices follow a random walk with time-varying
 drift: 
\begin_inset Formula 
\begin{eqnarray*}
\log P_{t} & = & \log P_{t-1}+\mu_{t}+\varepsilon_{t}\\
\mu_{t} & = & \mu_{t-1}+\nu_{t}
\end{eqnarray*}

\end_inset

where 
\begin_inset Formula $\varepsilon_{t},\nu_{t}$
\end_inset

 are Gaussian white noise.
 Suppose further that in forming beliefs at time 
\begin_inset Formula $t$
\end_inset

 the agent can only use data up to date 
\begin_inset Formula $t-1$
\end_inset

.
 Then optimal forecast of future prices are made through the Kalman filter,
 and 
\begin_inset Formula $E_{t}P_{t+1}=P_{t}\exp\left(\hat{\mu}_{t}+\sigma^{2}/2\right)$
\end_inset

 where 
\begin_inset Formula $\hat{\mu}_{t+1}=\hat{\mu}_{t}+g\left(\log\frac{P_{t}}{P_{t-1}}-\sigma_{P}^{2}-\hat{\mu}_{t}\right)$
\end_inset

 and 
\begin_inset Formula $\sigma_{P}^{2}$
\end_inset

 and 
\begin_inset Formula $g$
\end_inset

 are functions of the variances of 
\begin_inset Formula $\varepsilon_{t}$
\end_inset

 and 
\begin_inset Formula $\nu_{t}$
\end_inset

.
 The actual asset price is obtained through the Euler equation:
\begin_inset Formula 
\[
P_{t}=\frac{E_{t}Q_{t,t+1}D_{t+1}}{1-\exp\left(\hat{\mu_{t}}+\sigma_{P}^{2}/2\right)}
\]

\end_inset


\end_layout

\begin_layout Enumerate
The previous approaches assume that the object to be learnt is the growth
 rate of the asset price.
 Adam, Marcet & Nicolini also offer an approach where it is the return 
\begin_inset Formula $R_{t}=\left(P_{t}+D_{t}\right)/P_{t-1}$
\end_inset

 that is learned.
 In particular, they believe the return is the sum of a random walk and
 some noise: 
\begin_inset Formula 
\begin{eqnarray*}
\log R_{t} & = & \mu_{t}+\varepsilon_{t}\\
\mu_{t} & = & \mu_{t-1}+\nu_{t}
\end{eqnarray*}

\end_inset

Again, agents believe that 
\begin_inset Formula $\varepsilon_{t},\nu_{t}$
\end_inset

 are Gaussian white noise and that in forming beliefs at time 
\begin_inset Formula $t$
\end_inset

 the agent can only use data up to date 
\begin_inset Formula $t-1$
\end_inset

, so the Kalman filter can be used.
 One obtains 
\begin_inset Formula $E_{t}R_{t+1}=\exp\left(\hat{\mu}_{t}+\sigma^{2}/2\right)$
\end_inset

 where 
\begin_inset Formula $\hat{\mu}_{t+1}=\hat{\mu}_{t}+g\left(\log\frac{P_{t}}{P_{t-1}}-\sigma_{P}^{2}-\hat{\mu}_{t}\right)$
\end_inset

.
 However, unlike in the previous approaches, this does not as easily permit
 to solve for the current asset price.
 The Euler equation now reads
\begin_inset Formula 
\[
\hat{E}_{t}Q_{t,t+1}R_{t+1}=1
\]

\end_inset

The actual asset price 
\begin_inset Formula $P_{t}$
\end_inset

 will be the one for which the agent's discount factor 
\begin_inset Formula $Q_{t,t+1}$
\end_inset

 satisfies the Euler equation above.
 AMN obtain an analytical solution in the case of vanishing risk, and an
 agent whose only source of income is the asset, so that with power utility
 his discount factor is 
\begin_inset Formula $Q_{t,t+1}=\beta\left(D_{t+1}/D_{t}\right)^{-\gamma}$
\end_inset

.
 They also constrain the supply of the asset to unity.
 In this case, they obtain
\begin_inset Formula 
\[
\frac{P_{t}}{D_{t}}=\frac{1}{1-\left(\beta\exp\left(\hat{\mu}_{t}\right)^{1-\gamma}\right)^{\frac{1}{\gamma}}}-1
\]

\end_inset

In particular, asset prices now rise when beliefs become more optimistic
 only when 
\begin_inset Formula $\gamma<1$
\end_inset

, i.e.
 when the wealth effect is sufficiently small.
 Otherwise, a high return belief increases the demand for consumption through
 a strong wealth effect, which actually reduces current asset demand so
 that the asset price has to fall to restore equilibrium.
\end_layout

\begin_layout Standard
All these learning schemes have one thing in common: they feature a 
\begin_inset Quotes eld
\end_inset

momentum effect
\begin_inset Quotes erd
\end_inset

.
 After an initial positive shock e.g.
 to dividends, prices and beliefs continue to rise for a while and collapse
 afterwards.
 This leads to hump-shaped responses or even dampened oscillations, depending
 on the choice of parameters.
 Learning about future levels of prices, as had been tried previously in
 the literature, cannot produce these results.
 This is because these learning schemes only lead to first-order difference
 equations, which by definition can only produce AR(1)-type decay responses.
\end_layout

\begin_layout Section
Desired contribution
\end_layout

\begin_layout Standard
AMN have only considered the case of exogenously given dividends.
 However, this setup begs the question what types of interaction can arise
 between financial markets with learning and the real economy.
 
\end_layout

\begin_layout Standard
I therefore embed AMN's learning scheme into an economy with production.
 My aim is to establish the following:
\end_layout

\begin_layout Itemize
a positive feedback loop between the asset price and dividends.
 I.e.
 higher asset prices, caused e.g.
 by optimistic beliefs, raise dividends, which further raises asset prices
 and makes beliefs even more optimistic.
 This should amplify the boom-bust cycles already present in AMN.
\end_layout

\begin_layout Itemize
a boom in asset prices should coincide with a business-cycle type response
 of the economy, i.e.
 one in which employment and all components of demand rise.
\end_layout

\begin_layout Standard
There is one important observation to make at this general stage.
 A positive feedback loop (expectations of higher prices 
\begin_inset Formula $\Rightarrow$
\end_inset

 higher dividends 
\begin_inset Formula $\Rightarrow$
\end_inset

 higher prices) is exactly what is needed to establish multiple equilibria
 as well.
 But having multiple equilibria would void the need for learning as a source
 of fluctuations, since then sunspots can take up that role under the more
 preferred rational expectations.
 In such a situation, learning becomes merely one of many possibilities
 for specifying the sunspot process.
 The feedback loop I want to generate should therefore be large enough to
 generate considerable volatility under learning, but small enough to preserve
 unicity of the equilibrium.
\end_layout

\begin_layout Section
1st try: Linkage through the wealth effect in the RBC model
\end_layout

\begin_layout Standard
My first attempt started from a frictionless RBC model without labour.
 The representative firm manages the capital stock and pay dividends 
\begin_inset Formula 
\[
D_{t}=A_{t}K_{t-1}^{\alpha}+\left(1-\delta\right)K_{t-1}-K_{t}
\]

\end_inset

The representative household can purchase shares in the firm, which are
 in fixed supply of unity.
 Dividends from firm ownership are its only source of income: 
\begin_inset Formula $C_{t}=S_{t-1}\left(D_{t}+P_{t}\right)-S_{t}P_{t}$
\end_inset

 and 
\begin_inset Formula $S_{t}=1$
\end_inset

 in equilibrium.
\end_layout

\begin_layout Standard
I adopted the third approach of AMN, learning about the return: 
\begin_inset Formula $E_{t}\log R_{t+1}=\hat{\mu}_{t}$
\end_inset

 and 
\begin_inset Formula 
\[
\frac{P_{t}}{D_{t}}=\frac{1}{1-\left(\beta\exp\left(\hat{\mu}_{t}\right)^{1-\gamma}\right)^{\frac{1}{\gamma}}}-1
\]

\end_inset


\end_layout

\begin_layout Standard
The interaction between asset prices and the real economy takes place through
 the discount rate used to determine the firm's optimal investment decision.
 This is given by the Euler equation
\begin_inset Formula 
\[
1=\hat{E}_{t}Q_{t,t+1}\left(A_{t+1}\alpha K_{t}^{\alpha-1}+1-\delta\right)
\]

\end_inset

I simplified by abstracting from uncertainty, so that 
\begin_inset Formula $Q_{t,t+1}=R_{t+1}^{-1}=\exp\left(-\hat{\mu}_{t}\right)$
\end_inset

.
 So 
\begin_inset Formula $K_{t}$
\end_inset

 depends negatively on the return belief.
 
\end_layout

\begin_layout Standard
Intuitively, the higher the expected rate of return on stocks, the higher
 is the opportunity cost of investing in capital instead.
 The firm, acting on behalf of households, therefore reduces investment
 and increases its dividends.
 Households find it optimal to increase consumption in turn as they want
 to consume some of their expected future wealth from stock returns.
 However, since the capital stock falls, eventually dividends will have
 to fall again.
 This reduces prices and return beliefs.
\end_layout

\begin_layout Standard
In sum, this model is able to generate a positive feedback loop between
 asset prices and dividends.
 But it seems questionable whether there are many episodes in the data that
 fit the pattern generated by this model: an asset price boom increases
 consumption at the expense of investment and output.
 What's more, when labour is introduced into the model, an asset price boom
 reduces both labour supply (through a positive wealth effect) and labour
 demand (because the capital stock shrinks), so hours worked and wages fall.
 
\end_layout

\begin_layout Section
2nd try: Linkage through firm value in the matching model
\end_layout

\begin_layout Standard
In a matching model, firm value determines the entry decision of firms.
 A natural way to get an interaction between asset price learning and the
 real economy is therefore to treat firm value as the asset price in AMN's
 setting.
 The Euler equation for firm value in a simple matching is
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
p_{t}=E_{t}Q_{t,t+1}\left(d_{t+1}+\left(1-s\right)p_{t+1}\right)
\]

\end_inset

where the factor 
\begin_inset Formula $1-s$
\end_inset

 accounts for the exogenous exit of firms at the rate 
\begin_inset Formula $s$
\end_inset

.
 I use lower-case letters here to denote single-firm quantities and upper-case
 letters for aggregate quantities.
 Entry by firms is given by the free-entry condition
\begin_inset Formula 
\[
q\left(\theta_{t}\right)p_{t}=\chi
\]

\end_inset

where 
\begin_inset Formula $\chi$
\end_inset

 is the cost of attempted entry and 
\begin_inset Formula $q\left(\theta_{t}\right)$
\end_inset

 the probability of successful entry, A higher firm value creates entry
 and employment which increases labour market tightness 
\begin_inset Formula $\theta_{t}$
\end_inset

 and therefore the entry probability 
\begin_inset Formula $q\left(\theta_{t}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
Here, AMN's price growth learning equation reads 
\begin_inset Formula 
\[
p_{t}=\frac{E_{t}Q_{t,t+1}d_{t+1}}{1-\left(1-s\right)\hat{\mu}_{t}}
\]

\end_inset

and the fluctuations in 
\begin_inset Formula $p_{t}$
\end_inset

 readily induce fluctuations in employment and consequently output, investment
 and consumption.
 However, these generally do not induce positive comovements in dividends
 
\begin_inset Formula $d_{t}$
\end_inset

 as well.
 Rather, dividends tend to move in the opposite direction of asset prices.
 
\end_layout

\begin_layout Standard
The reason is that dividends in the model are the profits of a single-worker
 firm.
 They are given by
\begin_inset Formula 
\[
d_{t}=\max_{k_{t}}y_{t}-w_{t}-r_{t}k_{t}
\]

\end_inset

where 
\begin_inset Formula $k_{t}=K_{t}/N_{t}$
\end_inset

 and 
\begin_inset Formula $y_{t}=Y_{t}/N_{t}=a_{t}^{1-\alpha}k_{t}^{\alpha}$
\end_inset

.
 The Cobb-Douglas aggregate production function 
\begin_inset Formula $Y_{t}=a_{t}^{1-\alpha}N_{t}^{1-\alpha}K_{t}^{\alpha}$
\end_inset

 allows to aggregate and obtain a representative firm, and it also means
 that the profits of a single firm are proportional to the marginal profits
 per worker for the economy as a whole:
\begin_inset Formula 
\[
d_{t}=\frac{1}{1-\alpha}\frac{\partial D_{t}}{\partial N_{t}}
\]

\end_inset


\end_layout

\begin_layout Standard
While in an expansion due to high firm value, 
\emph on
aggregate
\emph default
 profits do rise, 
\emph on
marginal
\emph default
 profits fall.
 This is natural given that the marginal benefit of adding another worker
 to the economy should decrease with the number of workers.
 In practice, this comes about through an increase in the wage rate (usually
 determined by Nash bargaining), or a fall in the capital stock per worker
 (although the aggregate capital stock still rises).
 
\end_layout

\begin_layout Section
Existing literature
\end_layout

\begin_layout Standard

\emph on
to be completed
\end_layout

\begin_layout Itemize
Adam, Kuang & Marcet: learning house prices in a production economy.
 But compared to the case of exogenous dividends, there is dampening.
\end_layout

\begin_layout Itemize
Miao & Wang: no learning, but positive feedback loop through rational bubbles.
 Intratemporal loans and a borrowing constraint that depends on firm value.
\end_layout

\begin_layout Itemize
Farmer: replaces the wage equation in a standard search model by a belief
 about the value of the 
\begin_inset Quotes eld
\end_inset

stock market
\begin_inset Quotes erd
\end_inset

 (ownership of capital that is in fixed supply).
 Indeterminacy of the equilibrium wage translates into a sunspot on stock
 market expectations.
 Can do learning about the stock market to choose a path for the sunspot
 variable.
\end_layout

\end_body
\end_document
