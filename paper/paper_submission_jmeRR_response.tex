%% LyX 2.2.0 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{geometry}
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}
\usepackage{float}
\usepackage{setspace}
\usepackage[authoryear]{natbib}
\onehalfspacing

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}

\makeatother

\usepackage{babel}
\begin{document}

\section*{Responses to the referee's comments}

Thank you for your comments, which have helped me to focus the message
of the paper and provide more clarity on the underlying mechanisms.
Below, I will respond to each of your comments in turn.

\emph{1) There are few papers that can be added as references in the
literature review section. When discussing models with asset prices
and learning (paragraph starting with \textquotedblleft There are
a number of papers in the adaptive learning literature\textquotedblright ),
the paper by Rychalovska (2016) seems relevant (clearly the list cannot
be complete, but I think that this paper should be mentioned). Right
below, in the sentence starting with \textquotedblleft This \textquotedblleft adaptive
learning\textquotedblright{} approach has a long tradition, and has
recently been shown to improve the fit \dots \textquotedblright ,
the reference Milani (2007) should be added (in addition to the more
recent Slobodyan and Wouters, 2012), as it showed that a New Keynesian
model with adaptive learning fits the data better than the same model
under rational expectations. }

Thank you for pointing out these additional references, which I agree
are appropriate to mention. I have added them in this revised version.

\emph{2) A question that arises from the results in the paper is to
what extent monetary policy can reduce the role of asset prices for
fluctuations. The paper could include a short section in the robustness
section in which it shows how the results would change if a response
to asset prices was included in the Taylor rule.}

Indeed, monetary policy can be quite powerful in mitigating the stock
price fluctuations from learning and associated spillovers in the
real economy. I have now included a section on monetary policy in
the robustness section as you suggested. There, I discuss the way
in which changes in nominal interest rates affect asset price learning
and the real economy, and also compare several modifications to the
baseline policy rule, including one with a response to asset prices.
This latter specification turns out to be quite powerful in stabilizing
the economy. In this context, I have also allowed myself to cite a
follow-up paper of mine \citet{Caines.Winkler2018} that analyzes
optimal monetary policy with asset price learning in detail.

\emph{3a) There should be some additional robustness analysis, also
to obtain some more intuition on the channels for the results. One
particular sensitivity exercise that I would like to see is checking
the results under non-rational, extrapolative, expectations for asset
prices, without imposing the conditionally model-consistent restriction.
How would the results change? I understand the theoretical benefit,
but how much of a difference does it make quantitatively? }

In the new revised version, I have added a section on model-consistent
expectations to the robustness section. There, I discuss two alternative
ways of belief formation:
\begin{itemize}
\item In the ``adaptive learning'' version, I follow the approach of the
adaptive learning literature and parameterize every expectation\textemdash except
that of stock prices\textemdash occuring in the first-order conditions
with a linear function of the minimum state variables. The coefficients
of the function are updated using recursive least squares learning.
The subjective expectation for stock prices is kept as in the original
model.
\item In the ``mixed RE'' version, I impose rational expectations for
all variables in the model except stock prices. This effectively implies
that agents are unaware of predictability of their stock price forecast
errors, yet take that predictability into account when forming expectations
about, say, interest rates. I find this concept somewhat unintuitive,
but it can serve as a useful contrast to conditionally model-consistent
expectations.
\end{itemize}
You will see that both alternative versions do deliver a similar or
even greater amount of stock price volatility for a given set of parameter
values, but nevertheless produce weaker amplification of shocks. This
is because they lack the aggregate demand channels of the baseline
version. When households are overoptimistic about stock prices, their
optimism does not spill over into forecasts of their income and therefore
not increase aggregate demand as much as with conditionally model-consistent
expectations. Because of this lack of ``expectational spillovers'',
these alternative versions also cannot even qualitatively reproduce
the forecast error predictability in variables other than the stock
price.

\emph{3b) The paper includes some robustness evidence related to the
use of different gain coefficients and changes in the assumed evolution
for stock prices. Ideally, it would be nice to see more analysis of
the effects of different gains. Which moments aren\textquoteright t
matched anymore? Which estimated parameters would change substantially? }

I have extended the robustness analysis in this direction. The gain
parameter $g$ controls the volatility of stock prices for a given
process of dividends, the predictability of stock returns, and the
size of subjective forecast errors that agents make. A lower value
of the gain will reduce the volatility of output, and also the relative
volatility of investment since investment is driven partially by stock
price movements. When re-estimating the model, these shortcomings
are made up for by increasing the shock sizes and reducing the size
of investment adjustment costs. Even under re-estimated parameters
though, the degree of return predictability will be too low, and forecast
error predictability patterns will be too weak will be too low, relative
to what is found in the data. The discussed changes to the assumed
evolution for long-run stock prices do not materially affect the results
if they are small enough ($\rho_{\mu}\approx1$ and $a_{0}\approx0$).
When they become larger, they induce predictable high-frequency oscillations
in stock prices (at below yearly frequency) which is at odds with
the data. These findings are contained in Section 7.2 of the revised
version.

\emph{4) Table 2: the paper could discuss and show the changes in
the parameters that are necessary under RE (in the re-estimated model)
to allow model to fit as well as learning (besides changes in the
volatilities of the shocks).}

Following your suggestion and that of the editor, I now discuss each
parameter estimate and the differences between the learning and the
RE estimates in Section 5.1.

\emph{5) The paper detrends variables using the HP filter with lambda
1,600 for the SMM estimation reported in Table 2. Again, there can
be some robustness checks to make sure that the conclusions are similar
with different detrending choices, or using the variables in growth
rates. }

The conclusions of the paper are fairly robust to the detrending choice.
To address your concern, I have attached two tables to this response.
The first table shows the parameter estimates under different detrending
choices of the data. The second table shows the business cycle moments
reported in the paper under different detrending choices, but evaluated
at the baseline parameter values.

From the first table, you can see that the parameter estimates are
quite stable when instead of the Hodrick-Prescott filter, one applies
either the \citet{Christiano.Fitzgerald2003} filter, the \citet{Hamilton2017}
filter or a simple linear time trend removal. The only detrending
method that does make a difference to the estimates is first-differencing,
which substantially lowers the estimate of wage rigidity $\kappa_{w}$.
The value of this parameter estimate is tied to the volatility of
employment in the data, and this volatility is substantially lowered
when applying first differences because employment is a slow-moving
variable relative to GDP. When a low value of $\kappa_{w}$ is used,
the model will fail to generate amplification of consumption in response
to shocks and also will not match the predictability patterns of consumption
forecast errors. I explain the importance of $\kappa_{w}$ in detail
in my response to the editor.

From the second table, you can see that the baseline parameterization
also matches the business cycle moments well when they are detrended
differently. Again, the only material discrepancy is the volatility
of employment under first differences, which is substantially lower
in the data than in the model, as discussed above. 

In the revised version of the paper, I do not include these tables
in order to respect the page limit of the JME. Instead, I mention
the robustness to the detrending method when discussing the estimation
procedure.

\emph{6) Page 21: \textquotedblleft in general equilibrium wages rise
by so much that dividends fall.\textquotedblright{} This is, however,
due to the lack of nominal rigidities in this exercise though.}

Thank you for pointing this out. I have added a footnote to clarify
this point to the reader.

\section*{Responses to the editor's comments}

Thank you for your comments, especially on the estimation of the parameters.
Your comments prompted me to inspect the mechanism of the model more
closely and have led to what I hope will be a much clearer exposition
to the reader. Below, I will respond to each of your comments in turn.

\emph{1) The reviewer raises an important point about using HP filtered
data in estimation. The JME has published extensively on dangers of
using filtered data in estimation of DSGE models (e.g., I have a paper
with Serena Ng on this topic but there are many more papers). It was
not clear for me in the paper if you HP-filter both model and data
series when you do estimation. If not, you should fix this.}

I agree that the practice of comparing filtered data with unfiltered
model simulations is flawed. In the paper, I consistently applied
the same filter to both model and data series.

\emph{2) I am concerned about your parameter estimates. For example,
the learning model estimates effectively no volatility in the Taylor
rule. Likewise, I am puzzled by high degree of price flexibility in
prices and high degree of wage rigidity. These estimates appear to
be inconsistent with the estimates reported in other studies measuring
these parameters directly. Perhaps, these estimates are driven by
using filtered data. }

In the estimation, both data and simulated series are filtered the
same way. Moreover, the parameter estimates depend little on the detrending
method (as I explain in my response to the referee). I agree though
that the reported parameter estimates are unusual. Let me comment
on each of the three parameters you mention in turn.
\begin{itemize}
\item The volatility of the monetary policy shock in the Taylor rule $\sigma_{i}$
was estimated to be effectively zero. At the same time, the volatility
of the nominal interest rate $\sigma_{hp}\left(i\right)$ was only
a third of that in the data, despite it being a moment targeted by
the estimation. This implies that increasing $\sigma_{i}$ will deteriorate
considerably the fit of other moments. 
\begin{itemize}
\item The moments that are fit worse here are the volatilities of output
$\sigma_{hp}\left(Y\right)$ and inflation $\sigma_{hp}\left(\pi\right)$.
In the model with learning, it is not true that policy shocks mainly
affect the volatility of the nominal interest rate without having
much effect on output and inflation volatility. On the contrary, the
strong amplification of policy shocks in the learning model means
that adding policy shocks induces increases in the ratios $\sigma_{hp}\left(Y\right)/\sigma_{hp}\left(i\right)$
and $\sigma_{hp}\left(\pi\right)/\sigma_{hp}\left(i\right)$ that
are further away from the corresponding ratios in the data than if
the model is only driven by productivity shocks. As a result, the
estimator can achieve a better overall fit by discarding the interest
rate shock.
\item Of course, a simple comparison of the time series of nominal interest
rates in the data and the prescriptions of any simple Taylor rule
such as the one in this paper reveals that there are important residuals,
which is not consistent with $\sigma_{i}=0$. But I would like to
stress that the estimation performed here is not a full information
method. The goal is not to fit entire time series, but to fit a set
of business cycle and asset price moments that are generally accepted
as important benchmarks for a model. 
\item In the revised version of the paper, I have now excluded monetary
policy shocks altogether from the baseline parameterization. I only
discuss a (fully unexpected) monetary policy shock in the new Section
7.4, which I added following a suggestion by the referee.
\end{itemize}
\item The estimation chooses a relatively high degree of wage rigidity $\kappa_{w}$.
In the revised version, the parameter is estimated at 0.96, implying
a 4 percent probability of a nominal wage change per quarter. Studies
that use micro data to assess the frequency of wage changes usually
arrive at lower numbers (e.g. around 25 percent in \citealp{Barattieri.etal2014}),
but it is not an unusual value in estimated DSGE models (e.g. \citealp{Ajello2016}). 
\begin{itemize}
\item In the context of this estimation, a high value of $\kappa_{w}$ is
needed to match the relative volatility of employment $\sigma_{hp}\left(L\right)/\sigma_{hp}\left(Y\right)=1.13$
in the data. This moment is increasing in $\kappa_{w}$ because higher
wage rigidities effectively make the labor supply more elastic.
\item However, a high degree of wage rigidity also helps the model in important
other ways. A high value of $\kappa_{w}$ implies that changes in
subjective expectations shift consumption and investment in the same
direction. Intuitively, changes in subjective asset price expectations,
though endogenously generated through the learning mechanism, act
much like a combination of a wealth effect on household and a collateral
constraint shock on firms. It is well known that in one-sector models
with flexible prices and wages, any non-TFP shock cannot generate
positive compovement between consumption, investment and employment
\citep{Barro.King1984}, and also that price and wage rigidities can
overcome this limitation \citep{Justiniano.etal2010}. In my model
with learning, I find that to obtain positive comovement, $\kappa_{w}$
has to be at least about 0.92. Only in this case will learning amplify
not only output but also consumption fluctuations. 
\item This comovement is also important for the model to match the forecast
error predictability in consumption. Because expectations are model-consistent
conditional on asset prices, agents who are too optimistic about asset
prices will only be too optimistic about their forecasts of future
consumption if there is positive comovement of consumption and subjective
asset price expectations. If the degree of wage rigidity is not high
enough, then this comovement fails and the signs of the three consumption
forecast error predictability regressions reported in Table ??? all
switch signs, at odds with the data.
\end{itemize}
\item The estimation also chooses a relatively low degree of price rigidity
$\kappa$. In the revised version, the parameter is estimated at 0.41,
implying an average price duration of about five months. While this
is within the range of microdata estimates reported e.g. in \citet{Nakamura.Steinsson2013},
it is at the lower end of that range. 
\begin{itemize}
\item The estimator wants to pick a low value for $\kappa$ in order to
match volatility of inflation $\sigma_{hp}\left(\pi\right)$, which
is decreasing in the amount of price rigidity. Under learning, productivity
shocks have two opposing effects on inflation: The direct effect of
a productivity improvement is a fall in marginal costs which lowers
inflation. At the same time, the ensuing stock price boom relaxes
credit constraints and causes a positive wealth effect on households,
which raises inflation. The resulting net change in inflation is much
smaller than under rational expectations, where the second effect
is much more muted. As a result, the learning model produces sizeably
lower inflation volatility than its rational expectations counterpart,
which explains why a lower value of $\kappa$ is needed to match the
inflation volatility in the data.
\item However, a low degree of price rigidity also helps the learning model
generate realistic macroeconomic dynamics. The firms that are financially
constrained are intermediate goods producers who sell at a price $q_{t}$.
This price is the inverse of the markup charged by wholesalers in
monopolistic competition who buy intermediate goods and face nominal
price rigidities. After a positive productivity shock, inflation falls,
markups rise and $q_{t}$ falls. This reduces earnings and dividend
payments of financially constrained firms. When price rigidities are
large, this fall in dividends can lead to a fall in asset prices under
learning, tightening credit constraints and reducing output on impact.
\end{itemize}
\end{itemize}
\emph{3) It is well known that DSGE models are poorly identified.
You resolve this issue to some extent by calibrating many parameters
so that you need to estimate only a handful of parameters. But even
in this case, it is not clear how well these parameters are identified.
For example, the large standard error for \textbackslash{}sigma\_i
in the learning model may be consistent with poor identification. }

Thank you for raising this point. After some thought, I realized that
my calculations of the standard errors were invalid in the original
submission. I had imposed a constraint on the impulse responses to
avoid large deterministic oscillations in the learning model, and
this constraint on the admissible parameter set was binding at the
point estimates. Also, the estimate for $\sigma_{i}$ (the standard
deviation of the monetary policy shock) was very close to the lower
boundary of zero. \citep{Andrews1999} shows that standard errors
constructed using the standard delta method, which I had used, are
invalid if the point estimates are close to the boundary of admissible
parameter set. In most cases, they are too large.

In this revised version, I have dropped the constraint on impulse
responses and have also removed the monetary policy shock. The constraint
on impulse responses turned out to not affect the results much, and
an effective zero estimate of the monetary policy shock effectively
means that the shock can be dropped. In addition, to be sure that
my standard errors are valid, I now report standard errors that are
adjusted for the non-negativity constraints on the parameters following
\citet{Andrews1999}. You will see that these standard errors are
generally smaller than in the original submission. I can now confirm
that all parameters are well identified in the sense of the estimation
procedure.

\emph{4) Unless you can resolve estimation issues in 2) and 3), I
suggest you calibrate the model.}

I am hopeful that you will find my response to the issues you raised
satisfactory. That said, I am open to calibrating the model to parameters
that are more in line with the existing literature. To this end, I
have included a calibrated version of the model in an appendix to
the revised version of the paper. In this calibrated version, the
price and wage rigidity parameters are set to $\kappa=0.7$ and $\kappa_{w}=0.93$,
and the dividend payout ratio is set to its historical average at
$\zeta=0.45$. Also, just as in the revised estimated model, there
are no monetary policy shocks. 

As I have discussed above, a higher value of $\kappa$ can lead to
a fall in dividends, asset prices and output after a positive productivity
shock. In order to alleviate this problem, I have made some slight
changes to this calibrated version of the model. The dividend policy
of firms is adjusted to smooth out changes in the price $q_{t}$ of
intermediate goods, to preserve procyclicality of dividends with substantial
price rigidities. Further,beliefs about asset price growth are updated
with a one-period lag. This change turned out to be necessary in order
to preserve the local stability of the learning equilibrium under
the above dividend policy. Such ``lagged belief updating'' is quite
common in models of learning to guarantee stability. Finally, the
dependency of the borrowing constraint on stock prices is lowered
somewhat in order to avoid deterministic oscillations or explosive
solutions. The appendix to the revised version spells out the exact
changes made to the model and repeats the key results of the paper
for the calibrated model.

\emph{5) Figures 4 and 5 should report impulse responses of the learning
model w/o financial frictions.}

Thank you for this suggestion. Learning in the frictionless benchmark
also leads to some amplification of shocks because of the aggregate
demand effects discussed in the paper, but they are not as powerful
as with financial frictions, for two reasons. First, without the credit
constraint there is no amplifying effect of stock price movements
on investment. On the contrary, wealth effects tends to lower investment
during asset price booms, as households want to carry consumption
forward. Second, there is no amplifying effect through dividends:
Because firms are unconstrained to start with, there is little scope
for firm profitability to increase during asset price booms. I have
provided a detailed definition of the frictionless benchmark and its
learning counterpart in the appendix of the revised version.

\emph{6) The paper needs careful proof-reading. For example: a. P.29,
last line: I think it should be \textquotedblleft \dots also decreasing\dots \textquotedblright{}
rather than \textquotedblleft \dots also increasing\dots \textquotedblright{}
b. Figure 6: the dashed line is the RE model (rather than the learning
model) c. Figure 5: \textbackslash{}espilon\{mt\} denote the monetary
policy shock while in the paper you use \textbackslash{}epsilon\{it\}
(equation 3.9)}

My apologies for these errors. I have tried my best to eliminate such
errors from the revised version.

\emph{7) The JME has a page limit (see below). Please shorten the
paper.}

The revised version is considerably shortened. The exposition of adjustment
costs and nominal rigidities has been relegated to the appendix. The
discussion of the simplified version of the model has been shortened.
The section previously titled ``Interaction between learning and
financial frictions'' has been removed, as it is partly covered by
the newly added impulse response of the model with learning and without
financial frictions. The section previously titled ``Asset price
volatility with rational expectations'' has also been removed, as
the idea is covered in the new sensitivity section 7.1 ``Belief formation
concept''.

The revised version of the paper is formatted in JME draft format,
and runs 30 pages of text and footnotes, 3 pages of references, 5
tables and 5 exhibits. At this stage, I do not feel confident shortening
the paper further without making the paper unsatisfactory to you or
the referee. I will of course happily follow your suggestions for
shortening further parts of the paper, should you decide to proceed
with this submission.

\section*{Additional tables: Sensitivity to detrending method}

\begin{table}[H]
\caption{Estimated parameters using different filters.}
\medskip{}

\begin{centering}
{\small{}}%
\begin{tabular}{c|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l}
{\small{}parameter} & \multicolumn{2}{c}{{\small{}$\sigma_{a}$}} & \multicolumn{2}{c}{{\small{}$\kappa$}} & \multicolumn{2}{c}{{\small{}$\kappa_{w}$}} & \multicolumn{2}{c}{{\small{}$\psi$}} & \multicolumn{2}{c}{{\small{}$\zeta$}} & \multicolumn{2}{c}{{\small{}$g$}}\tabularnewline
\hline 
{\small{}Hodrick-Prescott} & &{\small{}660\%} & &{\small{}408} & &{\small{}961} & {\small{}25}&{\small{}28} & &{\small{}562} & &{\small{}00498}\tabularnewline
 & {\small{}(}&{\small{}091\%)} & {\small{}(}&{\small{}024)} & {\small{}(}&{\small{}020)} & {\small{}(1}&{\small{}98)} & {\small{}(}&{\small{}007)} & {\small{}(}&{\small{}00005)}\tabularnewline
\hline 
{\small{}Christiano-Fitzgerald} & &{\small{}880\%} & &{\small{}436} & &{\small{}930} & {\small{}23}&{\small{}83} & &{\small{}568 } & &{\small{}00478}\tabularnewline
 & {\small{}(}&{\small{}112\%)} & {\small{}(}&{\small{}030)} & {\small{}(}&{\small{}030)} & {\small{}(1}&{\small{}62)} & {\small{}(}&{\small{}.007)} & {\small{}(}&{\small{}00007)}\tabularnewline
\hline 
{\small{}Hamilton} & &{\small{}839\%} & &{\small{}386} & &{\small{}973} & {\small{}17}&{\small{}06} & &{\small{}544 } & &{\small{}00488}\tabularnewline
 & {\small{}(}&{\small{}190\%)} & {\small{}(}&{\small{}240)} & {\small{}(}&{\small{}005)} & {\small{}(10}&{\small{}25)} & {\small{}(}&{\small{}070)} & {\small{}(}&{\small{}00089)}\tabularnewline
\hline 
{\small{}Linear Trend} & {\small{}1}&{\small{}263\%} & &{\small{}384} & &{\small{}988} & {\small{}32}&{\small{}76} & &{\small{}487 } & &{\small{}00497}\tabularnewline
 & {\small{}(}&{\small{}286\%)} & {\small{}(}&{\small{}123)} & {\small{}(}&{\small{}004)} & {\small{}(5}&{\small{}53)} & {\small{}(}&{\small{}007)} & {\small{}(}&{\small{}00007)}\tabularnewline
\hline 
{\small{}First Differences} & &{\small{}849\%} & &{\small{}378} & &{\small{}331} & {\small{}19}&{\small{}06} & &{\small{}565 } & &{\small{}00565}\tabularnewline
 & {\small{}(}&{\small{}066\%)} & {\small{}(}&{\small{}020)} & {\small{}(}&{\small{}083)} & {\small{}(2}&{\small{}37)} & {\small{}(}&{\small{}009)} & {\small{}(}&{\small{}00009)}\tabularnewline
\hline 
\end{tabular}
\par\end{centering}{\small \par}
{\footnotesize{}Parameters of the learning model as estimated by simulated
method of moments. Asymptotic standard errors in parentheses are adjusted
for boundary constraints on the parameters following \citet{Andrews1999}.
Targeted data moments and estimated standard errors. Hodrick-Prescott
filter with smoothing parameter $\lambda=1600$. Christiano-Fitzgerald
bandpass filter with frequency cutoffs 1.5 and 8 years. Hamilton filter
with parameters $h=8$ and $p=4$. In each case, the same detrending
method is applied to the data and the simulated model series.}{\footnotesize \par}
\end{table}
\begin{table}[H]
\caption{Business cycle statistics using different filters, baseline parameter
values.}
\medskip{}

\begin{centering}
{\small{}}%
\begin{tabular}{ll|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l|r@{\extracolsep{0pt}.}lr@{\extracolsep{0pt}.}l}
 &  & \multicolumn{4}{c|}{{\small{}HP}} & \multicolumn{4}{c|}{{\small{}CF}} & \multicolumn{4}{c|}{{\small{}Hamilton}} & \multicolumn{4}{c|}{{\small{}Linear Trend}} & \multicolumn{4}{c}{{\small{}FD}}\tabularnewline
 & {\small{}moment} & \multicolumn{2}{c}{{\small{}data}} & \multicolumn{2}{c|}{{\small{}model}} & \multicolumn{2}{c}{{\small{}data}} & \multicolumn{2}{c|}{{\small{}model}} & \multicolumn{2}{c}{{\small{}data}} & \multicolumn{2}{c|}{{\small{}model}} & \multicolumn{2}{c}{{\small{}data}} & \multicolumn{2}{c|}{{\small{}model}} & \multicolumn{2}{c}{{\small{}data}} & \multicolumn{2}{c}{{\small{}model}}\tabularnewline
\hline 
{\small{}output vol.} & {\small{}$\sigma\left(Y_{t}\right)$} & {\small{}1}&{\small{}43\%} & {\small{}1}&{\small{}51\%} & {\small{}1}&{\small{}43\%} & {\small{}1}&{\small{}36\%} & {\small{}3}&{\small{}26\%} & {\small{}2}&{\small{}31\%} & {\small{}5}&{\small{}00\%} & {\small{}2}&{\small{}50\%} & {\small{}0}&{\small{}81\%} & &{\small{}90\%}\tabularnewline
\hline 
{\small{}vol. rel.} & {\small{}$\sigma\left(I_{t}\right)/\sigma\left(Y_{t}\right)$} & {\small{}2}&{\small{}90} & {\small{}2}&{\small{}99} & {\small{}2}&{\small{}86} & {\small{}3}&{\small{}06} & {\small{}2}&{\small{}82} & {\small{}2}&{\small{}84} & {\small{}1}&{\small{}79} & {\small{}2}&{\small{}83} & {\small{}2}&{\small{}70} & {\small{}1}&{\small{}96}\tabularnewline
{\small{}to output} & {\small{}$\sigma\left(C_{t}\right)/\sigma\left(Y_{t}\right)$} & &{\small{}60} & &{\small{}58} & &{\small{}58} & &{\small{}58} & &{\small{}64} & &{\small{}68} & &{\small{}99} & &{\small{}75} & &{\small{}75} & &{\small{}59}\tabularnewline
 & {\small{}$\sigma\left(L_{t}\right)/\sigma\left(Y_{t}\right)$} & {\small{}1}&{\small{}13} & {\small{}1}&{\small{}11} & {\small{}1}&{\small{}00} & {\small{}1}&{\small{}13} & {\small{}1}&{\small{}01} & {\small{}1}&{\small{}02} & &{\small{}93} & &{\small{}97} & &{\small{}80} & {\small{}1}&{\small{}24}\tabularnewline
 & {\small{}$\sigma\left(D_{t}\right)/\sigma\left(Y_{t}\right)$} & {\small{}3}&{\small{}00} & {\small{}2}&{\small{}97} & {\small{}2}&{\small{}56} & {\small{}3}&{\small{}00} & {\small{}2}&{\small{}61} & {\small{}1}&{\small{}64} & {\small{}2}&{\small{}65} & {\small{}2}&{\small{}46} & {\small{}2}&{\small{}19} & {\small{}2}&{\small{}13}\tabularnewline
\hline 
{\small{}correlation} & {\small{}$\rho\left(I_{t},Y_{t}\right)$} & &{\small{}95} & &{\small{}75} & &{\small{}96} & &{\small{}73} & &{\small{}92} & &{\small{}84} & &{\small{}81} & &{\small{}83} & &{\small{}87} & &{\small{}57}\tabularnewline
{\small{}w/ output} & {\small{}$\rho\left(C_{t},Y_{t}\right)$} & &{\small{}94} & &{\small{}81} & &{\small{}96} & &{\small{}84} & &{\small{}94} & &{\small{}90} & &{\small{}94} & &{\small{}92} & &{\small{}91} & &{\small{}64}\tabularnewline
 & {\small{}$\rho\left(L_{t},Y_{t}\right)$} & &{\small{}85} & &{\small{}93} & &{\small{}86} & &{\small{}95} & &{\small{}80} & &{\small{}82} & &{\small{}56} & &{\small{}67} & &{\small{}56} & &{\small{}87}\tabularnewline
 & {\small{}$\rho\left(D_{t},Y_{t}\right)$} & &{\small{}56} & &{\small{}60} & &{\small{}54} & &{\small{}26} & &{\small{}47} & &{\small{}25} & &{\small{}16} & &{\small{}25} & &{\small{}28} & &{\small{}19}\tabularnewline
\hline 
{\small{}inflation} & {\small{}$\sigma\left(\pi_{t}\right)$} & &{\small{}27\%} & &{\small{}31\%} & &{\small{}23\%} & &{\small{}16\%} & &{\small{}47\%} & &{\small{}29\%} & &{\small{}57\%} & &{\small{}29\%} & &{\small{}28\%} & &{\small{}39\%}\tabularnewline
{\small{}nom. rate} & {\small{}$\sigma\left(i_{t}\right)$} & &{\small{}37\%} & &{\small{}10\%} & &{\small{}33\%} & &{\small{}08\%} & &{\small{}68\%} & &{\small{}14\%} & &{\small{}80\%} & &{\small{}15\%} & &{\small{}22\%} & &{\small{}07\%}\tabularnewline
\hline 
\end{tabular}
\par\end{centering}{\small \par}
{\footnotesize{}Quarterly U.S. data 1962Q1\textendash 2012Q4. Standard
errors in parentheses. $\pi_{t}$ is quarterly CPI inflation. $i_{t}$
is the federal funds rate. All following variables are in logarithms.
$L_{t}$ is total non-farm payroll employment. Consumption $C_{t}$
consists of services and non-durable private consumption. Investment
$I_{t}$ consists of private non-residential fixed investment and
durable consumption. Output $Y_{t}$ is the sum of consumption and
investment. Dividends $D_{t}$ are four-quarter moving averages of
S\&P 500 dividends. $\sigma\left(\cdot\right)$ is the standard deviation
and $\rho\left(\cdot,\cdot\right)$ is the correlation coefficient.
Hodrick-Prescott filter with smoothing parameter $\lambda=1600$.
Christiano-Fitzgerald bandpass filter with frequency cutoffs 1.5 and
8 years. Hamilton filter with parameters $h=8$ and $p=4$. In each
case, the same detrending method is applied to the data and the simulated
model series.}{\footnotesize \par}
\end{table}

\bibliographystyle{elsarticle-harv}
\bibliography{finfric}

\end{document}
