function [ res, moments, constraints, W, Shat, mhat, mnames ] = my_bgg2_evalsmm( paramnames, paramvector, v, indata, M_, oo, options)
% MY_BGG2_EVALSMM evaluate SMM
% invoked by smm.m

warning off all;

%% preliminaries

Tpre=indata.Tpre;
T=size(v,2)-Tpre;

%load parameter file
my_bgg2_parameters;

%trending variables
trendingvars={'N','D','B','K','V','Y','C','I','w','Ce'};
if prefs==2
    trendingvars={trendingvars{:}, 'X_L'};
end
    
%% only keep the moments we need

for i=1:indata.I
    ii.(indata.mnames{i})=i;
end

%keepi=[ii.sY, ii.rC ii.rI ii.rL ii.rDy ii.spi ii.si ii.sR];
keepi=[ii.sY, ii.rC ii.rI ii.rL ii.rDy ii.sR];

mhat=indata.mhat(keepi);
mnames=indata.mnames(keepi);
Shat=indata.Shat(keepi,keepi);

N=length(keepi);

%% update parameters
for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
end
endoparamupdate;
for i=1:M_.param_nbr
    eval(['M_.params(i)=' M_.param_names(i,:) ';']);
end

%% run stoch_simul

dynerr=0;
try
    oo=rmfield(oo,'dr.ghx'); 
    oo=rmfield(oo,'drPLM');
end
if options.order==3; options.k_order_solver=1; end
try
    [oo.dr, info]=resol(0,M_,options,oo);
    %if info(1); print_info(info,0,options); end
catch err
    dynerr=1;
    disp(err.message);
end

%if Dynare fails, set the objective very high
if dynerr || isempty(oo.dr.ghx) || any(isnan(oo.dr.ghx(:))) || (options.order==3 && any(isnan(oo.dr.ghxxx(:))))
    %disp('Dynare error');
    res=1e9;
    moments=NaN;
    return;
end

nexo=M_.exo_nbr-strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)

%% moment

[~,jY]=ismember('Y',M_.endo_names,'rows');
[~,jL]=ismember('L',M_.endo_names,'rows');
[~,jC]=ismember('C',M_.endo_names,'rows');
[~,jI]=ismember('I',M_.endo_names,'rows');
[~,jw]=ismember('w',M_.endo_names,'rows');
[~,ji]=ismember('i',M_.endo_names,'rows');
[~,jpi]=ismember('pi',M_.endo_names,'rows');
[~,jV]=ismember('V',M_.endo_names,'rows');
[~,jD]=ismember('D',M_.endo_names,'rows');
[~,jED]=ismember('ED',M_.endo_names,'rows');
[~,jRV]=ismember('RV',M_.endo_names,'rows');
jVD=M_.endo_nbr+1; %compute V/D manually later

%jmoments=[jY jC jI jL jD jpi ji jRV];
jmoments=[jY jC jI jL jD jRV];
idxY=1; %offset of output
idxrelY=2:5; %offsets for sd relative to output
idxnoHP=[6]; %offsets for non HP-filtered sd
idxcorrY=[]; %compute correlations at the end?
computeER=0; %compute equity premium at the end?


%% retrieve first-order policy rules

%retrieve state variables
k2 = oo.dr.kstate(find(oo.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
oo.dr.state_var=oo.dr.order_var(k2);
    
C=zeros(length(oo.dr.state_var),M_.endo_nbr);
    for i=1:length(oo.dr.state_var)
        C(i,oo.dr.state_var(i))=1;
    end

A=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
B=oo.dr.ghu(oo.dr.inv_order_var,1:nexo);
          
if max(abs(eig(A)))>=1
    %disp('Solution explosive');
    res=1e9;
    moments=NaN;
    return;
end

%% generate simulations and calculate moments

ys=oo.dr.ys; dr=oo.dr; order=options.order;

sigmaA1=sigmaA1; %for use with parfor
eqprem=eqprem;
K=20; %number of parallel threads
stackedv=zeros(M_.exo_nbr,T/K+Tpre,K);
stackedv(:,1:Tpre,:)=repmat(v(:,1:Tpre),[1 1 K]);
stackedv(:,Tpre+(1:T/K),:)=reshape(v(:,Tpre+(1:T)),[M_.exo_nbr T/K K]);
moments=zeros(N,K);
simconstraints=zeros(1,K);
parfor k=1:K
    try; dates('initialize'); end;
    myv=stackedv(:,:,k);
    x=simult1(ys,dr,myv',order,M_,options);
    x=x(:,Tpre+1+(1:T/K))-repmat(dr.ys,1,T/K);

    trend=sigmaA1*[0 cumsum(myv(1,Tpre+(2:T/K)),2)];
    for i=1:length(trendingvars)
        [~,jidx]=ismember(trendingvars{i},M_.endo_names,'rows');
        x(jidx,:)=x(jidx,:)+trend;
    end

    %generate Dy
    x(jD,:)=filter(ones(1,4)/4,1,x(jD,:));
    %generate VD
    x(jVD,:)=x(jV,:)-x(jD,:);
    %do HP-filtering
    x_hp=x(jmoments,:)'-hpfilter(x(jmoments,:),1600);
    %calculate moments      
    corrmat=corr(x_hp);
    mymoments=[std(x_hp), corrmat(1,idxcorrY)];
    mymoments(idxnoHP)=std(x(jmoments(idxnoHP),:));
    mymoments(idxrelY)=mymoments(idxrelY)/mymoments(idxY);
    if computeER
        mymoments=[mymoments, log(eqprem)];
    end
    moments(:,k)=mymoments;
    %calculate autocorrelation of return (two-period cycles check)
    simconstraints(k)=corr(x(jRV,2:T/K)',x(jRV,1:T/K-1)');
end
moments=mean(moments,2)';
simconstraints=mean(simconstraints,2)';

%% constraints

n=0;

%negativity of IRF of V after TFP shock
n=n+1;
minIRF=0;
maxIRF=0;
IRFA=B;
for t=1:1000
    minIRF=min(minIRF,IRFA(jV,1)+IRFA(jV,2));
    maxIRF=max(maxIRF,IRFA(jV,1)+IRFA(jV,2));
    IRFA=A*IRFA;
end
if maxIRF>0
    constraints(n)=-minIRF/maxIRF;
elseif minIRF>0
    constraints(n)=Inf;
else
    constraints(n)=0;
end
maxconstraint(n)=0.2;
weights(n)=1e4;

%negativity of IRF of i after monetary shock
n=n+1;
[~,ji]=ismember('i',M_.endo_names,'rows');
[~,ii]=ismember('epsi1',M_.exo_names,'rows');
if ii~=0
    posIRF=0;
    negIRF=0;
    IRFA=B;
    for t=1:20
        posIRF=posIRF+max(IRFA(ji,ii),0);
        negIRF=negIRF-min(IRFA(ji,ii),0);
        IRFA=A*IRFA;
    end
    constraints(n)=posIRF/(negIRF+posIRF);
    if isnan(constraints(n)); constraints(n)=0; end
    maxconstraint(n)=0.1;
    weights(n)=1e4;
end

%two-period cycles
n=n+1;
constraints(n)=-simconstraints(1);
maxconstraint(n)=0;
weights(n)=1e4;


%% apply weighting matrix and compute result

%choice of weighting matrix:

Shat1=(Shat/sqrt(norm(Shat)))^-1; %normalised
%W=eye(N)*1e4; %identity matrix
%W=diag(mhat)^-1; %scaled by size of moments
W=diag(diag(Shat1)); %scaled by inverse of variances
%W(8,8)=W(8,8)*100; %nudge towards return matching
%W=Shat1; %optimal SMM

res=(moments-mhat)*W*(moments-mhat)' + sum(weights.*max(constraints-maxconstraint,0));

end

