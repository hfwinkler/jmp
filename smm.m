% my_bgg2 SMM caller

global M_ oo_ options_;

solver='fmincon'; %choose solver
uniquecheck=0; %perform uniqueness check
loadlast=0; %load last results

mname='my_bgg2_learn'; %model file
T=2e4; %simulation length
Tpre=500; %burn-in
order=2; %order of perturbation

%parameters and ranges to search over
%paramnames={'sigmaA2','kappa','kappaW','psi','c','g'};
%lbounds=[1e-4; 0.001; 0.001;  0;  0; 0];
%ubounds=[0.05; 0.999; 0.999; 30; .99; 0.006];
%paramnames={'sigmaA2','psi','c','g'};
%lbounds=[1e-4; 0; 0; 0];
%ubounds=[1; 40; .999; 0.006];
paramnames={'sigmaA2','psi','g'};
lbounds=[0;  0; 0.004];
ubounds=[2e-2;  40; 0.006];
%paramnames={'sigmaA2' 'psi'};
%lbounds=[0;  0];
%ubounds=[2e-2;  25];
%paramnames={'g'};
%lbounds=[0];
%ubounds=[0.005];

J=length(paramnames);

%NOTE objectives are defined in my_bgg2_evalsmm.m

 
%% parse parameter file

evalc('dynare my_bgg2_parameters onlymacro savemacro');
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% import data from Stata and weighting matrix

%with csvimport located in my files:
smmtable = csvimport('data/smmexport.csv','delimiter','\t','outputAsChar',false);
smmnames=smmtable(1,:);
smmtable=cell2mat(smmtable(2:size(smmtable,1),:));
%extract relevant data from the table:
data.I=smmtable(1,1);
data.Tdata=smmtable(1,2); 
data.mhat=smmtable(1,2+data.I+(1:data.I));
data.mnames=smmnames(2+data.I+(1:data.I));
%remove _cons stub from Stata output
for i=1:data.I
    data.mnames{i}=data.mnames{i}(1:length(data.mnames{i})-5);
end
data.Shat=smmtable(:,2+(1:data.I));

Tdata=data.Tdata; 
data.Tpre=Tpre;
    
%% run Dynare once
dynare(mname, 'noclearall','nostrict');
options_.order=order;
options_.pruning=1;
options_.irf=0;
options_.nomoments=1;
options_.ar=0;
options_.noprint=1;
options_.hp_filter=0;
options_.periods=0;
options_.qz_criterium=1+1e-6;
options_.threads.kronecker.A_times_B_kronecker_C=2;
stoch_simul('');

%% do SMM

tic;

%generate random data
nexo=M_.exo_nbr-2*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)
stream = RandStream.getGlobalStream;
reset(stream,0);
v=[randn(nexo,T+Tpre); zeros(M_.exo_nbr-nexo,T+Tpre)];

param0=zeros(J,1);
for i=1:J
    eval(['param0(' num2str(i) ')=' paramnames{i} ';']);
end
if loadlast %can also start from previously found optimum
    load SMM_result paramstar;
    Jload=min(J,length(paramstar));
    param0(1:Jload)=paramstar(1:Jload); 
end 
fprintf('\nStarting values:\n');
for i=1:length(paramnames)
    fprintf('%8s = %3.4g\n',paramnames{i},param0(i));
end

[res,moments0,constraints0,W,Shat,mhat,mnames]=my_bgg2_evalsmm(paramnames,param0,v,data,M_,oo_,options_);
fprintf('\nInitial loss value:  %3.4g\n',res);


fprintf('Now running miminisation using %s.\n',solver);
if strcmp(solver,'multistart') %multiple point gradient search
    K=uniquecheck;
    options=optimset('Algorithm','sqp','Display','off','MaxFunEvals',1e4,'TolX',1e-8);
    bestparams=zeros(J,K);
    bestres=ones(1,K)*1e9;
    p=ProgressBar(K);
    parfor k=1:uniquecheck
        try; dates('initialize'); end;
        randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        while my_bgg2_evalsmm(paramnames,randompoint,v,data,M_,oo_,options_)>1e8
            randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        end
        [bestparams(:,k),bestres(k)]=fmincon(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,randompoint,[],[],[],[],lbounds,ubounds,[],options);
        p.progress();
    end
    save multistart_result bestparams bestres;
    p.stop;
    sortres=sort(bestres');
    disp(sortres(1:10));
    [res, kstar]=min(bestres);
    paramstar=bestparams(:,kstar);
elseif strcmp(solver,'fmincon') %gradient search
    options=optimset('Algorithm','sqp','UseParallel','never','Display','iter','MaxFunEvals',1e4,'TolX',1e-10);
    paramstar=fmincon(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,param0,[],[],[],[],lbounds,ubounds,[],options);
elseif strcmp(solver,'sa') %simulated annealing:
    options=saoptimset('Display','iter','MaxIter',1e4,'DisplayInterval',50,'StallIterLim',50*J,'InitialTemperature',1e3);
    paramstar=simulannealbnd(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,param0,lbounds,ubounds,options);
elseif strcmp(solver,'ga') %genetic algorithm
    options=gaoptimset('Display','iter','Generations',J*500,'PopulationSize',21);
    paramstar=ga(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,J,[],[],[],[],lbounds,ubounds,[],options)';
    options=optimset('Algorithm','sqp','UseParallel','never','Display','iter','MaxFunEvals',1e4,'TolX',1e-10);
    paramstar=fmincon(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,paramstar,[],[],[],[],lbounds,ubounds,[],options);
end

save SMM_result param0 paramstar;
[res,momentsstar,constraints]=my_bgg2_evalsmm(paramnames,paramstar,v,data,M_,oo_,options_);
disp(paramstar);

%check for uniqueness:
if uniquecheck && ~strcmp(solver,'multistart')
    K=uniquecheck; %number of draws
    a=0.2; %how far away to go to the boundary

    bestparams=zeros(J,K+1);
    bestres=zeros(1,K+1);
    bestparams(:,K+1)=paramstar;
    bestres(:,K+1)=res;
    fprintf('Looking for multiplicity, %i iterations. Initial optimum has loss: %3.4g\n',K,res);
    for k=1:K
        randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        while my_bgg2_evalsmm(paramnames,(1-a)*paramstar+a*randompoint,v,data,M_,oo_,options_)>1e8
            randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        end
        options=optimset('Algorithm','sqp','UseParallel','never','Display','iter','MaxFunEvals',1e4,'TolX',1e-10);
        bestparams(:,k)=fmincon(@(x) my_bgg2_evalsmm(paramnames,x,v,data,M_,oo_,options_) ,(1-a)*paramstar+a*randompoint,[],[],[],[],lbounds,ubounds,[],options);
        distance=norm(bestparams(:,k)-paramstar);
        bestres(k)=my_bgg2_evalsmm(paramnames,bestparams(:,k),v,data,M_,oo_,options_);
        fprintf('Perturbation #%i at distance: %3.4g, improvement: %3.4g\n',k,distance,res-bestres(k));
        if bestres(k)<res
            [res,momentsstar,constraints]=my_bgg2_evalsmm(paramnames,bestparams(:,k),v,data,M_,oo_,options_);
            paramstar=bestparams(:,k);
            save SMM_result param0 paramstar;
        end
        disp(bestparams);
    end

end

timespent=toc;

%% calculate stderrs

% Jacobian
h=sqrt(eps);
I=length(momentsstar);
J=length(paramstar);
jacobian=zeros(I,J);
for j=1:J
    hvec=zeros(J,1);
    hvec(j)=h;
    jacobian(:,j)=(my_bgg2_evalsmm2(paramnames, paramstar+hvec, v, data, M_, oo_, options_) - my_bgg2_evalsmm2(paramnames, paramstar, v, data, M_, oo_, options_) )/h;
end
fprintf('\n');

% %asymptotic variance of estimator, J test NB: matrix Sigmahat is asymptotic
% %variance of moments scaled by 1/sqrt(Tdata)
% if all(all(abs(Shat^-1-W/sqrt(norm(Shat)))<1e-5))
%     Sigmahat=(1+Tdata/T)*(jacobian'*Shat^-1*jacobian)^-1;
%     Jstat=Tdata/(1+Tdata/T)*(momentsstar-mhat)*Shat^-1*(momentsstar-mhat)';
%     fprintf('J-statistic: %4.3g, J-test rejects model with p-value %4.3g.\n',Jstat,chi2pdf(Jstat,length(momentsstar)-length(param0)));
% else
%     Sigmahat=(1+Tdata/T)*(jacobian'*W*jacobian)^-1*jacobian'*W*Shat*W*jacobian*(jacobian'*W*jacobian)^-1;
% end
% sderrs=sqrt(diag(Sigmahat));

%NEW CODE based on Andrews (1997)
%taking into account the box constraints on the parameters
%affecting the asymptotic distribution when we are close to a boundary

Jhat=jacobian'*W*jacobian;
Jhat1=inv(Jhat);
Vhat=(1+Tdata/T)*jacobian'*W*Shat*W*jacobian;
Ndraws=2e4;
paramdist=zeros(J,Ndraws);
J1G_dist=Jhat1*mvnrnd(zeros(J,1),Vhat,Ndraws)';
for i=1:Ndraws
    if any(J1G_dist(:,i)+paramstar<=lbounds) %hit lower bound
        conidx=J1G_dist(:,i)+paramstar<=lbounds;
        paramdist(conidx,i)=lbounds(conidx);
        paramdist(~conidx,i)= +J1G_dist(~conidx,i) ...
                            +inv(Jhat(~conidx,~conidx))*(Jhat(~conidx,conidx) ...
                            *( paramstar(conidx)-lbounds(conidx)+J1G_dist(conidx,i) ));
    else
        if any(J1G_dist(:,i)+paramstar>=ubounds) %hit upper bound
            conidx=J1G_dist(:,i)+paramstar>=ubounds;
            paramdist(conidx,i)=ubounds(conidx);
            paramdist(~conidx,i)=paramstar(~conidx)+J1G_dist(~conidx,i) ...
                                +inv(Jhat(~conidx,~conidx))*Jhat(~conidx,conidx) ...
                                *( paramstar(conidx)-ubounds(conidx)+J1G_dist(conidx,i) );
        else %unconstrained case
            paramdist(:,i)=J1G_dist(:,i)+paramstar;
        end
    end         
end
sderrs=std(paramdist,0,2);

%% print results

fprintf('\nParameters: \tparam0   \tparam*   \t(stderrs)\n');
for j=1:J
    fprintf('%8s: \t%9.4g \t%9.4g \t(%9.4g)\n',paramnames{j},param0(j),paramstar(j),sderrs(j));
end
fprintf('\nMoments: \tmoments0 \tmoments* \tdata    \t(stderrs)\n');
for i=1:I
     fprintf('%8s: \t%9.4g \t%9.4g  \t%9.4g \t(%9.4g)\n',mnames{i},moments0(i),momentsstar(i),mhat(i),sqrt(Shat(i,i)));
end
fprintf('\nconstraints0=\n'); disp(constraints0);
fprintf('constraints*=\n'); disp(constraints);

msgbox(sprintf(['Estimation completed!\nResidual: ' num2str(res) '\nTime (mins): ' num2str(timespent/60)]),'SMM');

%% write new parameter file

fin = fopen('my_bgg2_parameters.mod');
fout = fopen('my_bgg2_parameters_smm.mod', 'wt');

tline = 'dummy';
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) 
        foundparam=0;
        for i=1:J
            foundparam=foundparam+i*strncmp(strtrim(tline),[paramnames{i} '='],length(paramnames{i})+1);
        end
        if foundparam==0
            fprintf(fout, '%s\n', tline);
        else
            fprintf(fout, '%s=%.12g; %%COMPUTED BY SMM\n',paramnames{foundparam},paramstar(foundparam));
        end
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);

%% send an email

% %headers
% fid = fopen('mail.txt','w');
% fprintf(fid,'To: fabian.winkler@frb.gov\n');
% fprintf(fid,'Subject: SMM completed\n');
% fprintf(fid,'Content-Type: text/html\n');
% fprintf(fid,'<html>\n<body>\n<pre style="font: monospace">\n');
% 
% %message
% [~,hostname]=system('hostname');
% fprintf(fid,'SMM just completed on %s',hostname);
% fprintf(fid,'\nModel name: %s\nResidual: %3.4f.\nRuntime was %3.3g minutes.\n',M_.fname,res,timespent/60);
% 
% fprintf(fid,'\n\nParameters: \tparam0   \tparam*   \t(stderrs)\n');
% for j=1:J
%     fprintf(fid,'%8s: \t%9.4g \t%9.4g \t(%9.4g)\n',paramnames{j},param0(j),paramstar(j),sqrt(Sigmahat(j,j)));
% end
% fprintf(fid,'\nMoments: \tmoments0 \tmoments* \tdata    \t(stderrs)\n');
% for i=1:I
%      fprintf(fid,'%8s: \t%9.4g \t%9.4g  \t%9.4g \t(%9.4g)\n',mnames{i},moments0(i),momentsstar(i),mhat(i),sqrt(Shat(i,i)));
% end
% fprintf(fid,'\nConstraints:     \tc0       \tc*\n');
% for i=1:length(constraints0)
%      fprintf(fid,'         \t%9.4g \t%9.4g\n',constraints0(i),constraints(i));
% end
% 
% %footer and send
% fprintf(fid,'</pre>\n</body>\n</html>\n');
% fclose(fid);
% system('cat mail.txt | /usr/lib/sendmail -t');
