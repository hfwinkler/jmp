% take the RE and learning solutions of my_bgg and plot the respective IRFs

T=50;


%RE transition matrices
load my_bgg_dr
varj_RE=varj;

C=zeros(size(decision));
C(3,4)=1;
C(4,5)=1;
C(5,7)=1;

A_RE=decision'*C;
B_RE=decision(6:7,:)';

%learning transition matrices
load my_bgg_learn_dr
varj_learn=varj;

C=zeros(size(decision));
C(3,4)=1;
C(4,5)=1;
C(5,7)=1;
C(6,8)=1;
C(7,6)=1;

A_learn=decision'*C;

B_learn=decision(8:9,:)';

%simulate

shock=[1; 0];

T=50;
xi_RE=zeros(length(varj_RE),T);
xi_learn=zeros(length(varj_learn),T);

for t=2:T
    xi_RE(:,t)=A_RE*xi_RE(:,t-1)+B_RE*shock*(t==2);
    xi_learn(:,t)=A_learn*xi_learn(:,t-1)+B_learn*shock*(t==2);
end

for j=1:length(varj_RE)
    figure;
    for jj=1:length(varj_learn)
        if varj_learn{jj}==varj_RE{j}
            break;
        end
    end
    plot(1:T,xi_RE(j,:),1:T,xi_learn(jj,:));
    title(varj_RE{j});
    legend('RE','learning','Location','Best');
end
    