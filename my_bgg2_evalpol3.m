function [ Loss, m ] = my_bgg2_evalpol3( paramnames, paramvector, noepsi, maxiter, M_, oo, options, y0)
%MY_BGG2_EVALPOL3 evaluate welfare by second-order approximation

%warning off all;
sigma=0;

%initialise values to return if anything goes wrong
Loss=Inf;
m=[];
    
%set steady state
if nargin==7
    y0=oo.dr.ys; %unless specified otherwise, start at non-stochastic SS
end

%% update parameters

%load parameters
my_bgg2_parameters;
for i=1:M_.param_nbr
    eval([M_.param_names(i,:) '=M_.params(i);']);
end

for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
end

if noepsi
    sigmai1=0;
    sigmai2=0;
end

endoparamupdate;
for i=1:M_.param_nbr
    eval(['M_.params(i)=' M_.param_names(i,:) ';']);
end


%% run Dynare (if there are any parameters to update)  

options.pruning=1;
options.irf=0;
options.nomoments=0;
options.ar=0;
options.noprint=1;
options.qz_criterium=1+1e-6;

dynerr=0;
try
    oo.dr=rmfield(oo.dr,'ghx'); 
    oo=rmfield(oo,'drPLM');
end
try
    [~,info,~,~,oo]=resol(0,M_,options,oo);
    if info(1); print_info(info,0,options); end
catch err
      disp(err.message);
      return;
end

%if Dynare fails, set the objective very high
if isempty(oo.dr.ghx) || any(isnan(oo.dr.ghx(:))) || (options.order==3 && any(isnan(oo.dr.ghxxx(:))))
    disp('Solution empty or Inf or NaN.');
    return;
end

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr; %number of exogenous shocks

%% retrieve first-order policy rules

%retrieve state variables
k2 = oo.dr.kstate(find(oo.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
oo.dr.state_var=oo.dr.order_var(k2);
    
C=zeros(length(oo.dr.state_var),M_.endo_nbr);
    for i=1:length(oo.dr.state_var)
        C(i,oo.dr.state_var(i))=1;
    end

A=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
B=oo.dr.ghu(oo.dr.inv_order_var,:);
%disp(max(abs(eig(A))));
          
%% run checks

if max(abs(eig(A)))>1+1e-9
    disp('Solution explosive');
    return;
end

m.penalty=0;
if isfield(oo,'drPLM')
    APLM=oo.drPLM.ghx(oo.dr.inv_order_var,:)*C;
    BPLM=oo.drPLM.ghu(oo.dr.inv_order_var,:);
    %run little sim
    %xtest=zeros(nendo,10);
    %xtest(:,1)=BPLM*[0;1;0;0;0;0];
    %for t=2:10; xtest(:,t)=APLM*xtest(:,t-1); end
    %[~,myi]=ismember('Y',M_.endo_names,'rows');
    %plot(xtest(myi,:)); drawnow;
    %m.penalty=1e3*max(0,-0.5-min(real(eig(APLM))));
    if max(abs(eig(APLM)))>1+1e-6
        disp('PLM explosive');
        %m.penalty=max(m.penalty,1e3*(max(abs(eig(APLM)))-1-1e-6));
        %return;
    end
end

%% calculate SS, means and variances, make indices

varlist={'Y','C','L','I','pi','i','piW','V','K','N','Utility','Ce','Welfare','Welfaree'};
ivar=zeros(length(varlist),1);
for i=1:length(ivar)
    [~,ivar(i)]=ismember(varlist{i},M_.endo_names,'rows');
    if ivar(i)==0; ivar(i)=1; end %quick fix if one of the variables doesn't exist
end

% %make use of Dynare function
% [Gamma_y, stationaryvars]=th_autocovariances(oo.dr,ivar,M_,options,1);
% if length(stationaryvars)<length(varlist)
%     %disp('Some variables are not stationary.');
%     %return;
% end
% 
% inv_order_var=oo.dr.inv_order_var;
% for i=1:length(varlist)
%     jdx.(varlist{i})=inv_order_var(ivar(i));
%     SS.(varlist{i})=y0(ivar(i));
%     if any(stationaryvars==i)
%         variances.(varlist{i})=Gamma_y{1}(i,i);
%         if options.order>1 && length(Gamma_y)>=3
%             means.(varlist{i})=Gamma_y{3}(i);
%         else
%             means.(varlist{i})=oo.dr.ys(ivar(i));
%         end
%     else
%         means.(varlist{i})=nan;
%         variances.(varlist{i})=nan;
%     end
% end
% 
% m.sigmaY=sqrt(variances.Y)*100;
% m.sigmaC=sqrt(variances.C)*100;
% m.sigmaI=sqrt(variances.I)*100;
% m.sigmaV=sqrt(variances.V)*100;
% m.sigmapi=sqrt(variances.pi)*100;
% m.sigmai=sqrt(variances.i)*100;

get_dynare_indices;

x=simult1(oo.dr.ys,oo.dr,(sqrt(M_.Sigma_e)*randn(nexo,options.periods))',options.order,M_,options);
if options.hp_filter>0
    x=x-hpfilter(x,options.hp_filter)';
end

m.sigmaY=std(x(varj.Y,:))*100;
m.sigmaC=std(x(varj.C,:))*100;
m.sigmaI=std(x(varj.I,:))*100;
m.sigmaV=std(x(varj.V,:))*100;
m.sigmamu=std(x(varj.mu,:))*100;
m.sigmapi=std(x(varj.pi,:))*100;
m.sigmai=std(x(varj.i,:))*100;

Loss=m.sigmaY^2+m.sigmapi^2;
return;

%% utility function

CSS=exp(SS.C);
LSS=exp(SS.L);
CeSS=SS.Ce;
if prefs==2  
    XSS=exp(SS.X_L);
else
    XSS=CSS;
end
    
    
if theta==1
    if prefs==1 
        U=@(C,C1,X,L) log(C/(1-b)-C1*b/(1-b)) - C0 - eta*L.^(1+phi)/(1+phi);
        Uinv=@(U,X,L) exp(U + C0 + eta*L.^(1+phi)/(1+phi));
    else
        U=@(C,C1,X,L) log(C/(1-b)-C1*b/(1-b) - eta*X.*L.^(1+phi)/(1+phi)); 
        Uinv=@(U,X,L)  exp(U) + eta*X.*L.^(1+phi)/(1+phi);
    end
else
    if prefs==1 
        U=@(C,C1,X,L) (C/CSS/(1-b)-b/(1-b)*C1/CSS).^(1-theta)/(1-theta) - eta*X.^(1-theta).*L.^(1+phi)/(1+phi);
        Uinv=@(U,X,L) ((1-theta)*(U + eta*X.^(1-theta).*L.^(1+phi)/(1+phi))).^(1/(1-theta))*CSS;
    else
        U=@(C,C1,X,L) (C/CSS/(1-b)-b/(1-b)*C1/CSS- eta*X.*L.^(1+phi)/phi).^(1-theta)/(1-theta); 
        Uinv=@(U,X,L) ( ((1-theta)*U).^(1/(1-theta)) + eta*L.^(1+phi)/(1+phi) )*CSS;
    end
end
USS=U(CSS,CSS,XSS,LSS);

    
%% Utility/welfare based on 2nd order approximation

if options.periods==0
    
    %average utility = average objective welfare
    m.EU=means.Utility; % one-period HH utility
    m.EUe=means.Ce; % one-period entrepreneur utility
    m.EUa=(1-weighte)*m.EU+weighte*m.EUe; % total utility

    %subjective welfare
    m.EWP=means.Welfare; % HH average welfare
    m.W0P=SS.Welfare+0.5*oo.dr.ghs2(jdx.Welfare); % HH conditional welfare
    %entrepreneur welfare under subjective beliefs is non-linear because they
    %will expect to hit the stock holding constraints - leave it out.

    %conditional objective welfare:
    if not(isfield(oo,'drPLM')) %easy when HHs are rational
        Ws2=oo.dr.ghs2(jdx.Welfare);
        W0=m.W0P; 
        Wes2=oo.dr.ghs2(jdx.Welfaree);
        We0=SS.Welfaree+0.5*Wes2;
    else %extra steps otherwise
        stateidx=oo.dr.inv_order_var(oo.dr.state_var);
        Ux=oo.dr.ghx(jdx.Utility,:);
        Uex=oo.dr.ghx(jdx.Ce,:);
        gx=oo.dr.ghx(stateidx,:);
        gu=oo.dr.ghu(stateidx,:);

        base=[(1-betta)*Ux; (1-1/R0)*Uex];
        mult=[betta; 1/R0]*ones(1,size(base,2));
        Wx=base;
        Wxold=zeros(size(Wx));
        iter=0;
        while norm(Wx-Wxold)>1e-10 %% tolerance level!
            iter=iter+1;
            Wxold=Wx;
            Wx=base+(mult.*Wx)*gx;
            if iter>maxiter
                %disp('Computation of Wx reached maxiter'); 
                m=[]; 
                return; 
            end
        end

        Uxx=oo.dr.ghxx(jdx.Utility,:);
        Uuu=oo.dr.ghuu(jdx.Utility,:);
        Uexx=oo.dr.ghxx(jdx.Ce,:);
        Ueuu=oo.dr.ghuu(jdx.Ce,:);
        gxx=oo.dr.ghxx(stateidx,:);
        guu=oo.dr.ghuu(stateidx,:);
        gs2=oo.dr.ghs2(stateidx);

        base=[(1-betta)*Uxx; (1-1/R0)*Uexx]+(mult.*Wx)*gxx;
        mult=[betta; 1/R0]*ones(1,size(base,2));
        Wxx=base;
        Wxxold=zeros(size(Wxx));
        krongx=kron(gx,gx);
        iter=0;
        while norm(Wxx-Wxxold)>1e-10 %% tolerance level!
            iter=iter+1;
            Wxxold=Wxx;
            Wxx=base+(mult.*Wxx)*krongx;
            if iter>maxiter 
                %disp('Computation of Wxx reached maxiter'); 
                m=[]; 
                return; 
            end
        end
        mult=[betta; 1/R0]*ones(1,size(Uuu,2));
        Wuu=(1-mult).*[Uuu; Ueuu]+mult.*(Wx*guu+Wxx*kron(gu,gu));

        Us2=oo.dr.ghs2(jdx.Utility);
        Ws2=Us2 + betta/(1-betta)*(Wx(1,:)*gs2 + Wuu(1,:)*vec(eye(M_.exo_nbr)));
        W0=SS.Welfare+0.5*Ws2;    
        Ues2=oo.dr.ghs2(jdx.Ce);
        Wes2=Ues2 + 1/(R0-1)*(Wx(2,:)*gs2 + Wuu(2,:)*vec(eye(M_.exo_nbr)));
        We0=SS.Welfaree+0.5*Wes2;

    end
    m.Ws2=Ws2;
    m.W0=W0;
    m.Wes2=Wes2;
    m.We0=We0;

    %loss measures

    m.Closs=100*(1-Uinv(m.W0,XSS,LSS)/CSS);
    m.Celoss=100*(1-m.We0/CeSS);

end

%% Utility/welfare based on simulated path
persistent Cemean0;

if options.periods>0
    T=options.periods;
	N=maxiter;
    s=rng;
    rng(0);
    v=randn(nexo,T-1,N);
    rng(s);
    minval=1e-9;
    
    Cpath=zeros(T,N);
    Lpath=zeros(T,N);
    Cepath=zeros(T,N);
    
    betta=betta; kappa=kappa; kappaW=kappaW; sigma=sigma; sigmaW=sigmaW; rigidwages=rigidwages; %#ok<ASGSL,NODEF> %otherwise parfor gets confused
    for n=1:N %better use parfor
        x=simult1(y0,oo.dr,v(:,:,n)',options.order,M_,options); %make use of Dynare routine  
        x=x(oo.dr.order_var,:);

        Delta=ones(1,T);
        DeltaW=ones(1,T);

        for t=2:T
            pi=exp(x(jdx.pi,t));
            pi=min(pi,((1-minval)/kappa)^(1/(sigma-1)));
            pstar=( (1-kappa*pi^(sigma-1))/(1-kappa) )^(1/(1-sigma));
            Delta(t)=(1-kappa)*pstar^-sigma + kappa*pi^sigma*Delta(t-1);
            if rigidwages
                piW=exp(x(jdx.piW,t));
                piW=min(piW,((1-1e-6)/kappaW)^(1/(sigmaW-1)));
                wstar=( (1-kappaW*piW^(sigmaW-1))/(1-kappaW) )^(1/(1-sigmaW));
                DeltaW(t)=(1-kappaW)*wstar^-sigmaW + kappaW*piW^sigmaW*DeltaW(t-1);
            end
        end

        Cpath(:,n)=max(CSS*(1+x(jdx.C,:)-log(CSS))./Delta,minval);
        %Cpath=exp(x(jdx.C,:))./Delta;
        Lpath(:,n)=max(LSS*(1+x(jdx.L,:)-log(LSS)).*DeltaW,minval);
        %Lpath=exp(x(jdx.L,:)).*DeltaW;
        Cepath(:,n)=x(jdx.Ce,:)./Delta;
               
    end
    
    if isempty(paramvector) || isempty(Cemean0); Cemean0=mean(Cepath,2); end    
    Cpath=max(Cpath+repmat(mean(Cepath,2)-Cemean0,1,N),minval);
    Cepath=Cepath-repmat(mean(Cepath,2)-Cemean0,1,N);
    
    C1path=[CSS*ones(1,N); Cpath(1:T-1,:)];
    Xpath=Cpath;    
    %WelfareVal=(1-betta)/(1-betta^T)*U(Cpath,C1path,Xpath,Lpath)'*[1; cumprod(ones(T-1,1)*betta)];
    %WelfareeVal=(1-betta)/(1-betta^T)*Cepath'*[1; cumprod(ones(T-1,1)*betta)]; 
    WelfareVal=mean(U(Cpath,C1path,Xpath,Lpath));
    WelfareeVal=mean(Cepath);
    
    %loss measures
    m.EU=mean(WelfareVal);
    m.Closs=100*(1-Uinv(m.EU,CSS,LSS)/CSS);
    m.Celoss=100*(1-mean(WelfareeVal)/CeSS);    
    
end

%% other statistics

m.Caloss=(CSS*m.Closs+CeSS*m.Celoss)/(CSS+CeSS);
%m.EC=means.C;
%m.ECe=means.Ce;
%m.N=means.N;

%% choose loss measure
Loss=m.Closs;
%Loss=m.sigmaY^2+m.sigmapi^2;

end
