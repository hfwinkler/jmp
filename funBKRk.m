function [ res ] = funBKRk( BKRk, gammaxi, M_ )

for i=1:M_.param_nbr
    eval([M_.param_names(i,:) '=M_.params(i);']);
end

BK=BKRk(1);
Rk0=BKRk(2);

gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));

V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);

res=[gama, xi]-gammaxi;

end

