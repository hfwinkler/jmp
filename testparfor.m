%% this script is used as a testing ground to diagnose subpar speed of parfor


%% this is the problematic code

clear;
T=1e6;
nexo=4;
K=10;

stream = RandStream.getGlobalStream;
reset(stream,0);
v=randn(nexo,T);
vk=zeros(nexo,T/K,K);
for k=1:K
    vk(:,:,k)=v(:,(k-1)/K*T+(1:T/K));
end

momentsk=zeros(2,K);

tic;
for k=1:K
    myv=vk(:,:,k);
    x=repmat(myv,2,2);
    x=x(:,2:T/K+1);
    x_hp=x(1:2,:)-hpfilter(x(1:2,:),1600)';
    momentsk(:,k)=var(x_hp,0,2);
end
disp(toc);
parfor k=1:K
    myv=vk(:,:,k);
    x=repmat(myv,2,2);
    x=x(:,2:T/K+1);
    x_hp=x(1:2,:)-hpfilter(x(1:2,:),1600)';
    momentsk(:,k)=var(x_hp,0,2);
end
disp(toc);

%% this is an alternate code

clear;
n = 50000;
x = randn(1,n) ;
y = zeros(1,n);
    tic;
    for i = 1 : n
        y(i) = std(x(1:i));
    end
    disp(toc);
    tic;
    parfor i = 1 : n
        y(i) = std(x(1:i));
    end
    disp(toc);
    