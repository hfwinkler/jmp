function [ moments ] = my_bgg2_evalsmm2(  paramnames, paramvector, v, indata, M, oo, options )
%MY_BGG2_EVALSMM2 wrapper for MY_BGG2_EVALSMM which only returns moments
    [~,moments]=my_bgg2_evalsmm(  paramnames, paramvector, v, indata, M, oo, options );
    fprintf('.');
end