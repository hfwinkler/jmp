%A variant of BGG with limited commitment
%RE frictionless solution


%load parameters from file:
@#include "my_bgg2_parameters.mod"

%redefine parameters
BK=0.0001;
Rk0=R0;
Y0=L0+A0+alphaa/(1-alphaa)*log(alphaa/(R0-1+delta));
K0=log(alphaa*G)+Y0-log(R0-1+delta);
I0=log((G-1+delta)/G)+K0;
C0=log(exp(Y0)-exp(I0));
V0=K0;
kappad=R0/(1-delta)-1;
eta=(1-alphaa)*exp(Y0-(phi+1)*L0);
@#if prefs==2
    deltaJR0=log(1-h/R0)-log(1-h/R0+(1-h)/(1+phi)*(1-alphaa)*exp(Y0-log(1-b)-C0));
@#endif

model;

@#include "my_bgg2_common.mod"

%firm side
exp(Q)=lambda*exp(Rk(+1)); %"R=Rk"
exp(Rk)=alphaa*exp(q+Y-K(-1)+log(G)+sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(w)=(1-alphaa)*exp(q+Y-L);
B=log(BK)+K; % set constant leverage
exp(K+Q)=exp(N)+exp(B)-exp(D); %flow of funds define dividends
exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%asset pricing
V=K+Q;
RV=Rk-Q(-1)-log(R0)+log(eqprem); %this is adjusted for eqprem only so we don't have to compute the SS value again

%learning equivalents
mu=V(+1)+sigeps1(+1)-V;

%no entrepreneurs here
Ce=0;

end;

steady_state_model;

Rk=log(R0);

@#include "my_bgg2_commonSS.mod"

Ce=0;
Welfaree=0;
C_all=log(exp(C)+Ce);

end;

@#include "my_bgg2_commonend.mod"
