% common steady state calculations for the my_bgg2 family

%productivity
A2=0;

%interest rate
R=log(R0);

%supply side
u=0;
K=log(alphaa*G)+Y0-log(exp(Rk)-1+delta);
I=log((G-1+delta)/G)+K;
Y=Y0;
C=C0;
L=L0;
w=log(1-alphaa)+Y-L;

%adjustment costs
Q=0;
Qd=log(1-delta);

%financials
B=log(BK)+K;
V=V0;
X=V;
N=log((Rk0-R0*BK)/G)+K;
E=log(alphaa*exp(Y)-delta*exp(K)/G-(R0-1)*BK*exp(K)/G);
D=log(gama*exp(N)+(1-gama)*c*exp(E));
ED=D;

%nominal rigidities
q=0;
pi=0;
i=R;
Delta=0;
Gamma1=-log(1-G/R0*kappa);
Gamma2=Gamma1;
DeltaW=0;
PCP=0;
@#if rigidwages
    piW=0;
    GammaW1=-log(1-1/R0*kappaW);
    GammaW2=GammaW1;
@#endif

%learning
mu=0;

%stock price variables
RV=log(eqprem);

@#if monpol==2
    Rn=R;
@#endif

%case of Gali-habit
@#if prefs==2
    X_L=C0;
@#endif

%utility and welfare
UL0 = -exp(L-DeltaW)^(1+phi)/(1+phi);
@#if prefs==1  
    Utility=eta*UL0;
    MU=1;
@#endif
@#if prefs==2
    Utility=eta*exp(X_L-C)*UL0;
    MU=1;
@#endif
@#if prefs==3  
    Utility=log(1+eta*UL0);
    MU=(1+eta*UL0)^-theta;
@#endif
Welfare=Utility;
