% evoke my_bgg2_plot for optimalpolicy calculated values

movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod; 
copyfile my_bgg2_parameters_optimalpolicy.mod my_bgg2_parameters.mod;
try
    my_bgg2_plot;
catch
    warning('my_bgg2_plotpol: something went wrong, aborting.');
end
delete my_bgg2_parameters.mod;
movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;
