%A variant of BGG with limited commitment
%solution with exogenous LOM for asset prices
%determined in turn from the ALM under learning

%load parameters from file:
@#include "my_bgg2_parameters.mod"

var lageps2;
parameters a1, a2, a3, a4, a5, b1, b2, b3, b4, b5;

load wouter_coefs;
if ARorder~=5 || MAorder~=5; error 'lag length does not match.'; end
a1=ARcoefs(1);
a2=ARcoefs(2);
a3=ARcoefs(3);
a4=ARcoefs(4);
a5=ARcoefs(5);
b1=MAcoefs(1);
b2=MAcoefs(2);
b3=MAcoefs(3);
b4=MAcoefs(4);
b5=MAcoefs(5);
sigmapred=sigmaAR;

model;

@#include "my_bgg2_common.mod"

%entrepreneurs
@#if entrepreneurs
    #lambdae=1/R0/eqprem;
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#else
    #lambdae=lambda;
    %#lambdae=1/R0;
    Ce=0;
@#endif

%firm side
exp(Rk)=(exp((1-markuptofirms)*q+Y)-exp(w+L))/exp(K(-1)-log(G)-sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(K+Q)=(1-gama+gama*omega)*(exp(N)-c*exp(E))+exp(B);
exp(w)=(1-alphaa)*exp(q+Y-L);
@#if cumdividend
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(lambdae*G*exp(V(+1)+sigeps1(+1))+exp(B)));
@#else    
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(exp(V)+exp(B)));
@#endif
exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));
exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%EXOGENOUS LOM for stock price
lageps2=eps2;
V-V0=a1*(V(-1)-V0)+a2*(V(-2)-V0)+a3*(V(-3)-V0)+a4*(V(-4)-V0)+a5*(V(-5)-V0)+b1*lageps2+b2*lageps2(-1)+b3*lageps2(-2)+b4*lageps2(-3)+b5*lageps2(-4);
@#if cumdividend
    exp(RV)=G*exp(V)/(exp(V(-1))-exp(D(-1)))*(1-gama)/(1-gama+gama*omega)/R0;
@#else
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;
@#endif

%learning equivalents
mu=V(+1)+sigeps1(+1)-V;


end;

steady_state_model;

Rk=log(Rk0);

@#include "my_bgg2_commonSS.mod"

@#if entrepreneurs
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);
@#else
    Ce=0;
@#endif
Welfaree=Ce;

end;

@#include "my_bgg2_commonend.mod"
