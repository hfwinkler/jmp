function [ stats, statnames ] = my_bgg2_evalpol2( paramnames, paramvector, noepsi)
%MY_BGG2_EVALPOL2 evaluate monetary policy rule
% with simple summary statistics for use e.g. in loss functions


%% preliminaries
global M_ oo_ options_


%load parameter file
my_bgg2_parameters;

%disp(paramvector);

%% update parameters
for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
    set_param_value(paramnames{i},paramvector(i));
end

if noepsi
    sigmai=0;
    set_param_value('sigmai',sigmai);
end

%recompute "endogenous" parameters
endoparamupdate;
for m=1:length(endoparamnames)
    set_param_value(endoparamnames{m},eval(endoparamnames{m}));
end

%% initialise return values - if anything goes wrong

stats=NaN;
statnames=cell(0,0);

%% run Dynare (if there are any parameters to update)  
options_.order=1;
options_.irf=0;
options_.nomoments=0;
options_.ar=0;
options_.noprint=1;
options_.hp_filter=1600;
options_.periods=2000;
    
warning off all;

oo_.dr.ghx=[];
try
    stoch_simul('');
end

if isempty(oo_.dr.ghx)
    %disp('Dynare error');
    return;
end

warning on all;

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr-2*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)


%% retrieve policy rules
C=zeros(length(oo_.dr.state_var),nendo);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end
A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B=oo_.dr.ghu(oo_.dr.inv_order_var,1:nexo);
SS=oo_.dr.ys;
VV=sqrt(diag(oo_.var));

if max(abs(eig(A)))>1
    %disp('Solution explosive');
    %keyboard;
    return;
end

%% calculations

varj=struct;
for j=1:nendo
varj.(deblank(M_.endo_names(j,:)))=j;
end
[~,iepsi]=ismember('epsi1',M_.exo_names,'rows');
[~,ieps2]=ismember('eps2',M_.exo_names,'rows');

n=0;
n=n+1; stats(n)=B(varj.Y,iepsi); statnames{n}='IRF of Y to epsi1';
n=n+1; stats(n)=B(varj.i,iepsi); statnames{n}='IRF of i to epsi1';
n=n+1; stats(n)=VV(varj.Y); statnames{n}='std(Y)';
n=n+1; stats(n)=VV(varj.L)/VV(varj.Y); statnames{n}='std(L)/std(Y)';
n=n+1; stats(n)=VV(varj.I)/VV(varj.Y); statnames{n}='std(I)/std(Y)';
n=n+1; stats(n)=VV(varj.i)/VV(varj.Y); statnames{n}='std(i)/std(Y)';
n=n+1; stats(n)=VV(varj.pi)/VV(varj.Y); statnames{n}='std(pi)/std(Y)';
n=n+1; stats(n)=VV(varj.V)/VV(varj.N); statnames{n}='std(V)/std(N)';

end

