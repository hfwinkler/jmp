%% common end lines

%shocks
shocks;
var eps1; stderr 1;
var eps2; stderr 1;
@#if monpol!=3
var epsi1; stderr 1;
var epsi2; stderr 1;
@#endif
end;

stoch_simul(order=2,pruning,irf=0,nomoments,nocorr,ar=0,noprint);
