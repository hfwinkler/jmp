function [ Loss, Closs, EU, USS, sigmaY, sigmapi, sigmaV ] = my_bgg2_evalpol( paramnames, paramvector, noepsi, T, N)
%MY_BGG2_EVALPOL evaluate HH welfare by evaluating the true utility at the
%perturbation solution

%% update parameters

%load parameter file
my_bgg2_parameters;

for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
    set_param_value(paramnames{i},paramvector(i));
end

if noepsi
    sigmai1=0;
    sigmai2=0;
    set_param_value('sigmai1',sigmai1);
    set_param_value('sigmai2',sigmai2);
end

%% initialise return values - if anything goes wrong

Loss=Inf;
Closs=Inf;
EU=-Inf;
USS=-Inf;
sigmaY=NaN;
sigmapi=NaN;
sigmaV=NaN;
    
%% run Dynare (if there are any parameters to update)  
options_.order=1; %which order?
options_.irf=0;
options_.nomoments=0;
options_.ar=0;
options_.noprint=1;
options_.hp_filter=0;

if not(isempty(paramnames))
    oo_.dr.ghx=[];
    try
        stoch_simul('');
    end

    if isempty(oo_.dr.ghx)
        %disp('Dynare error');
        %keyboard;
        return;
    end
end

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr-2*(1-veryrational)*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)

%% retrieve first order policy rules
C=zeros(length(oo_.dr.state_var),nendo);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end
A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B=oo_.dr.ghu(oo_.dr.inv_order_var,1:nexo);
SS=oo_.dr.ys;
VV=oo_.var;

if max(abs(eig(A)))>=1
    %disp('Solution explosive');
    return;
end

%% calculate variances

[~,jY]=ismember('Y',M_.endo_names,'rows');
[~,jpi]=ismember('pi',M_.endo_names,'rows');
[~,jV]=ismember('V',M_.endo_names,'rows');

sigmaY=sqrt(VV(jY,jY))*100;
sigmapi=sqrt(VV(jpi,jpi))*100;
sigmaV=sqrt(VV(jV,jV))*100;

%% utility function

[~,jC]=ismember('C',M_.endo_names,'rows');
[~,jL]=ismember('L',M_.endo_names,'rows');
[~,jpi]=ismember('pi',M_.endo_names,'rows');
if rigidwages; [~,jpiW]=ismember('piW',M_.endo_names,'rows'); end

CSS=exp(SS(jC));
LSS=exp(SS(jL));
if prefs==2 
    [~,jX_L]=ismember('X_L',M_.endo_names,'rows');    
    XSS=exp(SS(jX_L));
else
    XSS=exp(C0);
end

if theta==1
    if prefs==1 
        U=@(C,C1,X,L) log(C/(1-b)-b/(1-b)*C1) - C0 - eta*L^(1+phi)/(1+phi);
        Uinv=@(U,X,L) exp(U + eta*L^(1+phi)/(1+phi))*CSS;
    else
        U=@(C,C1,X,L) log(C/(1-b)-b/(1-b)*C1 - eta*X*L^(1+phi)/(1+phi)); 
        Uinv=@(U,X,L)  ( exp(U) + eta*X*L^(1+phi)/(1+phi) )*CSS;
    end
else
    if prefs==1 
        U=@(C,C1,X,L) (C/CSS/(1-b)-b/(1-b)*C1/CSS)^(1-theta)/(1-theta) - eta*X^(1-theta)*L^(1+phi)/(1+phi);
        Uinv=@(U,X,L) ((1-theta)*(U + eta*X^(1-theta)*L^(1+phi)/(1+phi)))^(1/(1-theta))*CSS;
    else
        U=@(C,C1,X,L) (C/CSS/(1-b)-b/(1-b)*C1/CSS- eta*X*L^(1+phi)/phi)^(1-theta)/(1-theta); 
        Uinv=@(U,X,L) ( ((1-theta)*U)^(1/(1-theta)) + eta*L^(1+phi)/(1+phi) )*CSS;
    end
end

%% calculate welfare



%generate simulations, including price dispersion term

stream = RandStream.getGlobalStream;
reset(stream,0);
v=randn(nexo,T,N);

WelfareVal=zeros(1,N);
parfor n=1:N
    x=simult1(oo_.dr.ys,oo_.dr,v(:,:,n)',options_.order,M_,options_); %make use of Dynare routine

    Delta=ones(1,T);
    DeltaW=ones(1,T);

    for t=2:T
        pi=exp(x(jpi,t));
        pstar=( (1-kappa*pi^(sigma-1))/(1-kappa) )^(1/(1-sigma));
        Delta(t)=(1-kappa)*pstar^-sigma + kappa*pi^sigma*Delta(t-1);
        if rigidwages
            piW=exp(x(jpiW,t));
            wstar=( (1-kappa*piW^(sigma-1))/(1-kappa) )^(1/(1-sigma));
            DeltaW(t)=(1-kappa)*wstar^-sigma + kappa*pi^sigma*Delta(t-1);
        end
    end

    Cpath=CSS*(1+x(jC,:)-log(CSS))./Delta;
    C1path=[CSS, Cpath(1:T-1)];
    Lpath=LSS*(1+x(jL,:)-log(LSS)).*DeltaW;

    if prefs==2  
        Xpath=XSS*(1+x(jX_L,:)-log(XSS));
    else
        Xpath=ones(1,T)*XSS;
    end
    WelfareVal(n)=(1-betta)/(1-betta^T)*arrayfun(U,Cpath,C1path,Xpath,Lpath)*[1; cumprod(ones(T-2,1)*betta)];
end


USS=U(CSS,CSS,XSS,LSS);
EU=mean(WelfareVal); %average HH utility = unconditional HH welfare under objective beliefs
Closs=100*(1-Uinv(EU,XSS,LSS)/CSS);

Loss=Closs;

end

