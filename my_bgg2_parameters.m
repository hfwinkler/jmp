%% ------------ switches ----------------

%type of preferences (1: separable, 2: Gali, 3: GHH risk-averse

prefs=1;

%set to 1 for variable capacity utilisation


%type of adjustment costs (0: in capital, 1: in investment)

adjcost=1;

%nominal wage rigidity?

rigidwages=1;

%indexed nominal rigidity (CEE type)?

indexcalvo=0;

%type of monetary policy (1: Taylor, 2: Taylor with natural rate, 3: strict
%inflation targeting (=flexible price allocation)

monpol=1;

%lagged belief updating?

laggedupdating=1;

%cum- or ex-dividend stock market value?

cumdividend=1;

%existence of separate entrepreneurs?

entrepreneurs=1;

%redistribute markup to firms?

markuptofirms=0;

%NOTE: need to adjust the dividend payout function in all model files as
%well!

%% -------------- variable and parameter definitions --------------

















%% ------------ free parameters ---------------

%technology
alphaa=0.33; %capital share
G=1^0.25; %deterministic trend growth
psi=7.51882397628; %COMPUTED BY SMM
nu=3; %elasticity of utilisation

%preferences
R0=1.02^0.25; %SS real rate
eqprem=1; %exp(.01016375); %equity premium
phi=0.33; %inverse Frisch
delta=0.025; %depreciation rate
theta=1;
L0=log(0.7); %steady state log hours
b=0; %consumption habit
h=0.99; %Gali-habit

%entrepreneur welfare weight
weighte=0;

%financials
BK=0.5; %fed funds data
Rk0=alphaa/0.18*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
%PD=154; %PD ratio in the economy
omega=0; %size of entering firms
xr=0.03; %probability of restructuring
c=0.449; %COMPUTED BY SMM
rhoX=0; %AR component on stock price in borrowing constraint

%nominal rigidities
phi_pi=1.5;
phi_Y=0;
phi_DY=0;
phi_DV=0;
rho_i=0.85;
kappa=0.7;
sigma=4;
kappaW=0.93;
sigmaW=4;
iota=0.99;

%learning
g=0.00477228816119; %COMPUTED BY SMM
rho_mu=0.999; %mean reversion in beliefs
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.01;

%shocks
sigmaA1=0;
sigmaA2=0.00649618447074; %COMPUTED BY SMM
sigmai1=0;
sigmai2=0;

%shock autocorrelations
rho=0.95; %of technology A2
rhoei=0.99; %of inflation target


%% ----------- computed parameters ------------

betta=G^theta/R0;

%gama=1-(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0*eqprem-G/PD);
%omega=(1-gama)/gama*(G/(R0*eqprem-G/PD)-1);
gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));

V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);

kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;

C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);


eta=(1-alphaa)*exp(Y0-(phi+1)*L0);












