function [ ret ] = exportpolicy( collectionname, policyname )
%EXPORTPOLICY( collectionname, policyname )
%write parameterfile from policy

load([collectionname '.mat']); 

paramnames=fieldnames(policies.(policyname));
paramvalues=cell2mat(struct2cell(policies.(policyname)));


fin = fopen('my_bgg2_parameters.mod');
fout = fopen(['my_bgg2_parameters_' policyname '.mod'], 'wt');

tline = 'dummy';
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) 
        foundparam=0;
        for i=1:length(paramvalues)
            foundparam=foundparam+i*strncmp(strtrim(tline),[paramnames{i} '='],length(paramnames{i})+1);
        end
        if foundparam==0
            fprintf(fout, '%s\n', tline);
        else
            fprintf(fout, '%s=%.8g; %%SET IN POLICY %s\n',paramnames{foundparam},paramvalues(foundparam),policyname);
        end
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);


end

