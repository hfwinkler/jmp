function [ res, moments, constraints, W, Shat, mhat, mnames ] = my_bgg2_evalsmm( paramnames, paramvector, v, indata, M_, oo, options)
% MY_BGG2_EVALSMM evaluate SMM
% invoked by smm.m

warning off all;

%% preliminaries

Tpre=indata.Tpre;
T=size(v,2)-Tpre;

%load parameter file
my_bgg2_parameters;
if strcmp(M_.fname,'my_bgg2_linearlearn')
    load linearREcoefs;
    fwrdnames={'C', 'pi', 'piW', 'D','Q'};
    statenames={'A2' 'I','B','K','R','w','i','mu'};
    for thisvarj=1:length(fwrdnames)
    thisvar=fwrdnames{thisvarj};
    for thisstatej=1:length(statenames)
        thisstate=statenames{thisstatej};
        pname=['bet_' thisvar '_' thisstate];    
        pval=ARE(varjRE.(thisvar),varjRE.(thisstate));
        eval([pname '=pval;']);
    end
end
end

%trending variables
trendingvars={'N','D','B','K','V','Y','C','I','w','Ce'};
if prefs==2
    trendingvars={trendingvars{:}, 'X_L'};
end
    
%% only keep the moments we need

for i=1:indata.I
    ii.(indata.mnames{i})=i;
end

%keepi=[ii.sY ii.rC ii.rI ii.rL ii.rDy ii.spi ii.sR];
%keepi=[ii.sY ii.rC ii.rI ii.rDy ii.sR];
keepi=[ii.sY ii.rI ii.sR];
%keepi=[ii.sY ii.rI];
%keepi=[ii.sPD];


mhat=indata.mhat(keepi);
mnames=indata.mnames(keepi);
Shat=indata.Shat(keepi,keepi);

N=length(keepi);

%% update parameters
for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
end
endoparamupdate;
for i=1:M_.param_nbr
    eval(['M_.params(i)=' M_.param_names(i,:) ';']);
end

%% run stoch_simul

dynerr=0;
try
    oo=rmfield(oo,'dr.ghx'); 
    oo=rmfield(oo,'drPLM');
end
if options.order==3; options.k_order_solver=1; end
try
    [oo.dr, info]=resol(0,M_,options,oo);
    %if info(1); print_info(info,0,options); end
catch err
    dynerr=1;
    disp(err.message);
end

%if Dynare fails, set the objective very high
if dynerr || isempty(oo.dr.ghx) || any(isnan(oo.dr.ghx(:))) || (options.order==3 && any(isnan(oo.dr.ghxxx(:))))
    %disp('Dynare error');
    res=1e9;
    moments=NaN;
    return;
end

nexo=M_.exo_nbr-strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)

%% moments in sims

%indices
get_dynare_indices;
jD=varj.D;
jV=varj.V;
jRV=varj.RV;
jVD=M_.endo_nbr+1; %compute V/D manually later
varj.VD=jVD;

%define what we use here:

idxcorrY=[]; %NB correlations are of each variable with the first variable

%filtering method: 0=none, 1=HP, 2=FD, 3=CF, 4=Hamilton

%jsd=[varj.Y varj.C varj.I varj.L varj.D varj.pi varj.RV];
%filtmethod=1*[1 1 1 1 1 1 1 0]; 
%idxrelY=[2 3 4 5]; %sd relative to first variable

%jsd=[varj.Y varj.C varj.I varj.D varj.RV];
%filtmethod=1*[1 1 1 1 0]; 
%idxrelY=[2 3 4]; %sd relative to first variable

jsd=[varj.Y varj.I varj.RV];
filtmethod=1*[1 1 0]; 
idxrelY=[2]; %sd relative to first variable

%jsd=[varj.Y varj.I];
%filtmethod=1*[1 1];
%idxrelY=[2]; %sd relative to first variable

%jsd=[varj.VD];
%filtmethod=[0];
%idxrelY=[]; %sd relative to first variable

%% retrieve first-order policy rules

%retrieve state variables
k2 = oo.dr.kstate(find(oo.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
oo.dr.state_var=oo.dr.order_var(k2);
    
C=zeros(length(oo.dr.state_var),M_.endo_nbr);
    for i=1:length(oo.dr.state_var)
        C(i,oo.dr.state_var(i))=1;
    end

A=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
B=oo.dr.ghu(oo.dr.inv_order_var,1:nexo);
          
if max(abs(eig(A)))>=1
    %disp('Solution explosive');
    res=1e9;
    moments=NaN;
    return;
end

%% generate simulations and calculate moments

ys=oo.dr.ys; dr=oo.dr; order=options.order;

sigmaA1=sigmaA1; %for use with parfor
eqprem=eqprem;
K=20; %number of parallel threads
stackedv=zeros(M_.exo_nbr,T/K+Tpre,K);
stackedv(:,1:Tpre,:)=repmat(v(:,1:Tpre),[1 1 K]);
stackedv(:,Tpre+(1:T/K),:)=reshape(v(:,Tpre+(1:T)),[M_.exo_nbr T/K K]);
moments=zeros(N,K);
simconstraints=zeros(1,K);
parfor k=1:K
    try; dates('initialize'); end;
    myv=stackedv(:,:,k);
    if strcmp(M_.fname,'my_bgg2_linearlearn')
        x=simult1_linearlearn(ys,oo,myv',1,M_,options,0.01,1,0);
    else
        x=simult1(ys,dr,myv',order,M_,options);
    end
    x=x(:,Tpre+1+(1:T/K))-repmat(dr.ys,1,T/K);

    trend=sigmaA1*[0 cumsum(myv(1,Tpre+(2:T/K)),2)];
    for i=1:length(trendingvars)
        [~,jidx]=ismember(trendingvars{i},M_.endo_names,'rows');
        x(jidx,:)=x(jidx,:)+trend;
    end

    %generate Dy
    x(jD,:)=filter(ones(1,4)/4,1,x(jD,:));
    %generate VD (append row to x)
    x(jVD,:)=x(jV,:)-x(jD,:);
    
    %filter
    x_filt=x(jsd,:)';
    %Hodrick-Prescott:
    x_filt(:,filtmethod==1)=x_filt(:,filtmethod==1)-hpfilter(x_filt(:,filtmethod==1),1600);
    %first difference:
    x_filt(:,filtmethod==2)=[0*x_filt(1,filtmethod==2); diff(x_filt(:,filtmethod==2))];
    %Christiano-Fitzgerald:
    x_filt(:,filtmethod==3)=bpass(x_filt(:,filtmethod==3),6,32);
    %Hamilton:
    Trange=12:T/K;
    Xham=[x_filt(Trange-8,filtmethod==4), x_filt(Trange-9,filtmethod==4), ...
          x_filt(Trange-10,filtmethod==4), x_filt(Trange-11,filtmethod==4), ...
          ones(T/K-11,1)];
    Yham=x_filt(Trange,filtmethod==4);
    x_filt(1:11,filtmethod==4)=0;
    x_filt(Trange,filtmethod==4)=Yham-Xham*((Xham'*Xham)\(Xham'*Yham));
    
    %calculate moments      
    corrmat=corr(x_filt);
    mymoments=[std(x_filt), corrmat(1,idxcorrY)];
    mymoments(idxrelY)=mymoments(idxrelY)/mymoments(1);
    moments(:,k)=mymoments;
    %calculate autocorrelation of return (two-period cycles check)
    simconstraints(k)=corr(x(jRV,2:T/K)',x(jRV,1:T/K-1)');
end
moments=mean(moments,2)';
simconstraints=mean(simconstraints,2)';

%% constraints

n=0;

%negativity of IRF of V after TFP shock
n=n+1;
minIRF=0;
maxIRF=0;
IRFA=B;
for t=1:1000
    minIRF=min(minIRF,IRFA(jV,1)+IRFA(jV,2));
    maxIRF=max(maxIRF,IRFA(jV,1)+IRFA(jV,2));
    IRFA=A*IRFA;
end
if maxIRF>0
    constraints(n)=-minIRF/maxIRF;
elseif minIRF>0
    constraints(n)=Inf;
else
    constraints(n)=0;
end
maxconstraint(n)=0.25;
weights(n)=0e4;

%negativity of IRF of i after monetary shock
n=n+1;
[~,ji]=ismember('i',M_.endo_names,'rows');
[~,ii]=ismember('epsi1',M_.exo_names,'rows');
if ii~=0
    posIRF=0;
    negIRF=0;
    IRFA=B;
    for t=1:20
        posIRF=posIRF+max(IRFA(ji,ii),0);
        negIRF=negIRF-min(IRFA(ji,ii),0);
        IRFA=A*IRFA;
    end
    constraints(n)=posIRF/(negIRF+posIRF);
    if isnan(constraints(n)); constraints(n)=0; end
    maxconstraint(n)=0;
    weights(n)=0e4;
end

%two-period cycles
n=n+1;
constraints(n)=-simconstraints(1);
maxconstraint(n)=0.1;
weights(n)=0e4;


%% apply weighting matrix and compute result

%choice of weighting matrix:

Shat1=(Shat/sqrt(norm(Shat)))^-1; %normalised
%W=eye(N)*1e4; %identity matrix
%W=diag(mhat)^-1; %scaled by size of moments
W=diag(diag(Shat1)); %scaled by inverse of variances
%W(7,7)=W(7,7)*10; %nudge towards return matching
%W=Shat1; %optimal SMM

res=(moments-mhat)*W*(moments-mhat)' + sum(weights.*max(constraints-maxconstraint,0));

end

