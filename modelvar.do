set more off
clear
insheet using "../my_bgg2_simlearn.csv", case
gen date=_n
tsset date
//drop if date>1000

gen expD=exp(D)
tssmooth ma Dy=expD, window(3 1)
replace Dy=100*log(Dy)
replace I=100*I
replace A2=100*A2
replace VD=100*VD
gen VDalt=(100*V-Dy)
keep date Dy I A2 VD VDalt

var A2 VDalt I Dy, lags(1/2)
irf set var_irf, replace
irf create IRF, set(var_irf) step(24) replace
irf graph oirf, noci response(I A2 Dy VDalt) byopts(yrescale)

