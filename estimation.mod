%% Dynare file to carry out estimation

% include relevant model file

@#include "my_bgg2_learn.mod"

varobs Yobs, Vobs, piobs;

estimated_params;

%structural parameters
%phi_pi, gamma_pdf, 1.5, 1;
%phi_DY, normal_pdf, 0, 0.1;
%rho_i, beta_pdf, 0.5, 0.1;
%psi, gamma_pdf, 9, 2;
%kappa, beta_pdf, 0.75, 0.1;
%kappaW, beta_pdf, 0.75, 0.1;
%c, beta_pdf, 0.66, 0.1;
%g,gamma_pdf, 0.004, 0.001;

%shock autocorrelations
rho, beta_pdf, 0.95, 0.01;
%rho_ei, beta_pdf, 0.9, 0.1;
%rhow, beta_pdf, 0.9, 0.1;
%rhop, beta_pdf, 0.9, 0.1;
%rhoxi, beta_pdf, 0.9, 0.1;
rhob, beta_pdf, 0.9, 0.01;

%shock standard deviations
%sigmaA1, inv_gamma_pdf, 0.005,0.005;
sigmaA2, inv_gamma_pdf, 0.005,0.005;
%sigmai1, inv_gamma_pdf, 0.005,0.005;
sigmai2, inv_gamma_pdf, 0.001,0.001;
%sigmaw, inv_gamma_pdf, 0.005,0.005;
%sigmaxi, inv_gamma_pdf, 0.005,0.005;
sigmab, inv_gamma_pdf, 0.01,0.01;
%stderr epsV, inv_gamma_pdf, .1,.1;
end;

estimated_params_init(use_calibration);
%sigmaA1,.005;
%sigmai1,.005;
sigmai2,.001;
%sigmaw,.005;
%sigmaxi,.005;
sigmab,.01;
%stderr epsV,.1;
%g,.003;
end;

%if learning, we need to copy the ALM file
if 1
    copyfile my_bgg2_learn_ALMfile.m estimation_ALMfile.m
else
    delete estimation_ALMfile.m
end

%identification;
%estimation(datafile=estimportdata,mh_replic=4000,mh_jscale=.9,mh_nblocks=1,mode_compute=0,mode_file=estimation_mode,conditional_variance_decomposition=[1 4 20], smoother,order=1,moments_varendo,ar=0,mode_check,plot_priors=0) Y C I L V pi i w R;
estimation(datafile=estimportdata,mh_replic=4000,mh_jscale=1,mh_nblocks=1,mode_compute=4,conditional_variance_decomposition=[1 4 20], smoother,order=1,moments_varendo,ar=0,mode_check,plot_priors=0) Y C I L V pi i w R;


%plot stock price
estimportdata;
if isfield(oo_.posterior_mean.parameters,'sigmaA1')
    trend=oo_.posterior_mean.parameters.sigmaA1*cumsum(oo_.SmoothedShocks.Mean.eps1);
else
    trend=zeros(size(oo_.SmoothedVariables.Mean.V));
end
Vmean=oo_.SmoothedVariables.Mean.V+trend;
figure; 
plot(date,Vmean-Vmean(1),date,cumsum(Vobs)-Vobs(1)); 
legend('estimated','observed'); title('V');

shock_decomposition Y N C I L V pi i w;

%write mode and clean up
writeparamfile(fieldnames(oo_.posterior_mode.parameters),cell2mat(struct2cell(oo_.posterior_mode.parameters)),'estim');
delete estimation_ALMfile.m;
cleanup_dyn estimation;