function [ stats, statnames ] = my_bgg2_evalshock( paramnames, paramvector)
%MY_BGG2_EVALPOL2 evaluate shocks


%% preliminaries
global M_ oo_ options_


%load parameter file
my_bgg2_parameters;

%disp(paramvector);

%% update parameters
for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
    set_param_value(paramnames{i},paramvector(i));
end

%% initialise return values - if anything goes wrong

stats=NaN;
statnames=cell(0,0);

%% run Dynare
options_.order=1;
options_.irf=0;
options_.nomoments=1;
options_.ar=0;
options_.noprint=1;
options_.hp_filter=0;

%if Dynare fails, set the objective very high
warning off;
oo_.dr.ghx=[];
try
    stoch_simul('');
end
warning on;

if isempty(oo_.dr.ghx)
    %disp('Dynare error');
    res=Inf;
    return;
end

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr-2*(1-veryrational)*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)

%% retrieve policy rules
C=zeros(length(oo_.dr.state_var),nendo);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end
A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B=oo_.dr.ghu(oo_.dr.inv_order_var,1:nexo);

%% generate simulation
T=1*10^4;
v=randn(nexo,T);
x=zeros(nendo,T);
for t=2:T
    x(:,t)=A*x(:,t-1)+B*v(:,t);
end

%trending variables
trendingvars={'N','B','K','V','Y','C','I','w','C_all'};
if prefs==2
    trendingvars={trendingvars, 'X_L0'};
end
    
trend=cumsum(sigmaA1*v(1,:),2);
for i=1:length(trendingvars)
    [~,jidx]=ismember(trendingvars{i},M_.endo_names,'rows');
    x(jidx,:)=x(jidx,:)+trend;
end

%do HP-filtering
x_hp=x-hpfilter(x,1600)';

%% calculations

varj=struct;
for j=1:nendo
varj.(deblank(M_.endo_names(j,:)))=j;
end

n=0;
n=n+1; stats(n)=std(x_hp(varj.Y,:)); statnames{n}='std(Y)';
n=n+1; stats(n)=std(x_hp(varj.I,:))/std(x_hp(varj.Y,:)); statnames{n}='std(I)/std(Y)';
n=n+1; stats(n)=std(x_hp(varj.C,:))/std(x_hp(varj.Y,:)); statnames{n}='std(C)/std(Y)';
n=n+1; stats(n)=std(x_hp(varj.L,:))/std(x_hp(varj.Y,:)); statnames{n}='std(L)/std(Y)';
n=n+1; stats(n)=std(x_hp(varj.i,:))/std(x_hp(varj.Y,:)); statnames{n}='std(i)/std(Y)';
n=n+1; stats(n)=std(x_hp(varj.pi,:))/std(x_hp(varj.Y,:)); statnames{n}='std(\pi)/std(Y)';
n=n+1; stats(n)=std(x_hp(varj.V,:))/std(x_hp(varj.Y,:)); statnames{n}='std(V)/std(Y)';
n=n+1; stats(n)=std(x(varj.Dy,:))/std(x_hp(varj.Y,:)); statnames{n}='std(Dy)/std(Y)';


end

