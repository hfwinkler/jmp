%toy model to get a better grasp of the amplification/dampening issue
clear;
%% preliminaries 

%simulation length
T=5*10^4;

%parameters
G=1;%1.001415;
sigma=.01;
R0=1.04^.25;

alpha=.33;
delta=.025;
k0tostar=.1; %alpha^(1/(1-alpha));
g=.006;
sigmag=g*1*10^-2;

%preliminary computations

d=@(k) alpha*(G*exp(-alpha*sigma^2/2))^(1-alpha)*k^alpha+(1-delta-R0)*k;


kstar=(alpha/(R0-1+delta))^(1/(1-alpha))*G*exp(-alpha*sigma^2/2);
k0=kstar*k0tostar;
Ed0=d(k0);
ERk0=d(k0)/k0+R0;
xi=(R0-G)/Ed0*k0;
xi=xi/(1+xi);

sigmaeta=sigmag*sqrt(1/g);
sigmanu=sigmag*sqrt((1-g)/g);
mu0=0.5*(-sigmanu^2/g+2*log(G)-sigma^2+sigmaeta^2+sigmanu^2);

%% pretty plots
krange=linspace(0,kstar,1e3);
vRErange=arrayfun(d, krange)/(R0-G);
figure;
plot(krange,[vRErange; krange; vRErange+krange; (1-xi)/xi*krange]);
axis([0 kstar 0 max(vRErange)+kstar]);
figure;
plot(max(R0-xi/(1-xi)*arrayfun(d,krange)./krange,0),log((1-xi)/xi*krange));
    

%% simulation

%draw shocks
eps=randn(T,1)*sigma-sigma^2/2;
%eps=zeros(T,1); eps(T/10+1)=2*sigma;

%RE solution
k.RE=ones(T,1)*k0;
Ed.RE=ones(T,1)*Ed0;
p.RE=ones(T,1)*Ed0/(R0-G);
mu.RE=ones(T,1)*mu0;

Rk.RE=ones(T,1)*(alpha*k0^(alpha-1)*(G*exp(-sigma^2/2))^(1-alpha)+1-delta);
d.RE=ones(T,1)*(Rk.RE(1)-R0)*k0/G/exp(-sigma^2/2);
R.RE=ones(T,1)*(p.RE(1)+d.RE(1))/p.RE(1)*G*exp(-sigma^2/2);

for t=2:T
    Rk.RE(t)=alpha*k0^(alpha-1)*(G*exp(eps(t)))^(1-alpha)+1-delta;
    d.RE(t)=(Rk.RE(t)-R0)*k0/G/exp(eps(t));
    R.RE(t)=(p.RE(t)+d.RE(t))/p.RE(t-1)*G*exp(eps(t));
end

%learning solution
k.learn=ones(T,1)*k.RE(1);
Ed.learn=ones(T,1)*Ed.RE(1);
p.learn=ones(T,1)*p.RE(1);
mu.learn=ones(T,1)*mu.RE(1);
Rk.learn=ones(T,1)*Rk.RE(1);
d.learn=ones(T,1)*d.RE(1);
R.learn=ones(T,1)*R.RE(1);

for t=2:T
    Rk.learn(t)=alpha*k.learn(t-1)^(alpha-1)*(G*exp(eps(t)))^(1-alpha)+1-delta;
    d.learn(t)=(Rk.learn(t)-R0)*k.learn(t-1)/G/exp(eps(t));
    k.learn(t)=( xi/(1-xi)*alpha*(G*exp(-alpha*sigma^2/2))^(1-alpha) ...
                / (xi/(1-xi)*(R0-1+delta)+R0-exp(mu.learn(t-1)+sigmag^2/2)) )^(1/(1-alpha));
    k.learn(t)=min(k.learn(t),kstar);
    Ed.learn(t)=alpha*k.learn(t)^alpha*(G*exp(-alpha*sigma^2/2))^(1-alpha)+(1-delta-R0)*k.learn(t);
    p.learn(t)=Ed.learn(t)/(R0-exp(mu.learn(t-1)+sigmag^2/2));
    R.learn(t)=(p.learn(t)+d.learn(t))/p.learn(t-1)*G*exp(eps(t));
    
    mu.learn(t)=mu.learn(t-1)-sigmanu^2/2+g*(log(p.learn(t)*G*exp(eps(t))/p.learn(t-1))+(sigmaeta^2+sigmanu^2)/2-mu.learn(t-1));
    if exp(mu.learn(t)+sigmag^2/2)>=R0*.999
        mu.learn(t)=mu.learn(t-1);
        disp(t);
    end
end

%transform back
A=ones(T,1);
for t=2:T
    A(t)=A(t-1)*exp(eps(t))*G;
end
D.RE=d.RE.*A;
P.RE=p.RE.*A;
K.RE=k.RE.*A;
D.learn=d.learn.*A;
P.learn=p.learn.*A;
K.learn=k.learn.*A;

DD.RE=[1; D.RE(2:T)./D.RE(1:T-1)];
DP.RE=[1; P.RE(2:T)./P.RE(1:T-1)];
pd.RE=P.RE./D.RE;
DD.learn=[1; D.learn(2:T)./D.learn(1:T-1)];
DP.learn=[1; P.learn(2:T)./P.learn(1:T-1)];
pd.learn=P.learn./D.learn;

%% plots

Trange=(T/10):(2*T/10);

figure;
plot(Trange,log(d.RE(Trange)),Trange,log(Ed.learn(Trange)),Trange,log(d.learn(Trange)));
legend('d_{RE}','Ed_{learn}','d_{learn}','Location','Best');

figure;
plot(Trange,eps(Trange)+log(G),Trange,log(DP.learn(Trange)),Trange,log(DD.learn(Trange)));
legend('Delta A','\Delta P_{learn}','\Delta D_{learn}','Location','Best');

figure;
plot(Trange,log(k.learn(Trange)),Trange,log(k.RE(Trange)));
legend('k_{learn}','k_{RE}','Location','Best');

figure;
plotyy(Trange,mu.learn(Trange),Trange,log(p.learn(Trange)));
legend('\mu_{learn}','p_{learn}','Location','Best');

%% moments

burnin=T/10;
disp('Moments:')
fprintf('%16s\t%8s\t%8s\t%8s\t%8s\n','Variable','Mean','Stddev','Skewness','Kurtosis');
varnames={'log(pd.RE)','log(pd.learn)','log(DP.RE)', 'log(DP.learn)','log(DD.RE)', 'log(DD.learn)', '4*log(R.RE)', '4*log(R.learn)'};
for i=1:length(varnames)
    eval(['v=' varnames{i} ';']);
    v=v(burnin:T);
    m1=mean(v);
    m2=sqrt(mean((v-m1).^2));
    m3=mean((v-m1).^3)/m2^3;
    m4=mean((v-m1).^4)/m2^4-3;
    fprintf('%16s\t%8.4g\t%8.4g\t%8.4g\t%8.4g\n',varnames{i},m1,m2,m3,m4);
end
fprintf \n;
