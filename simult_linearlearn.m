% simulate my_bgg2_linearlearn

%this script will eventually need to become a function that my_bgg2_plot
%can use. for now it's just a construction site.

clear; %for debugging

%% USER: parameters

%learning gain
linearg=0.01;
R0scale=linearg/(1-linearg);

%simulation length
T=500;
Tpre=1e4; %burn-in

%learning variables: regressors and regressands
fwrdnames={'C', 'pi', 'piW', 'D','Q'};
statenames={'A2' 'I','B','K','R','w','i','mu'};

%% run Dynare

dynare my_bgg2_linearlearn;
options_.qz_criterium=1;

%retrieve state variables, C matrix
k2 = oo_.dr.kstate(find(oo_.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
oo_.dr.state_var=oo_.dr.order_var(k2);
C=zeros(length(oo_.dr.state_var),M_.endo_nbr);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end

%stuff
nfwrd=length(fwrdnames);
nstate=length(statenames);

%indices
get_dynare_indices;
fwrdj=zeros(1,nfwrd);
for i=1:nfwrd
    fwrdj(i)=varj.(fwrdnames{i});
end
statej=zeros(1,nstate);
for i=1:nstate
    statej(i)=varj.(statenames{i});
end


%% USER: shock sequence

v=randn(M_.exo_nbr,T+Tpre);

%% initialize


%pre-allocate parameter vector and precision matrix
learncoefs0=zeros(nstate,nfwrd);
R0=zeros(nstate);

%load learning parameter values from RE file
load linearREcoefs;

%calculate variance matrix of regressors
nendoRE=size(ARE,1);
regjRE=zeros(1,nstate);
for i=1:nstate
    regjRE(i)=varjRE.(statenames{i});
end
Vvec=(eye(nendoRE^2)-kron(ARE,ARE))\vec(BRE*BRE');
VV=zeros(nendoRE);
VV(:)=Vvec;
R0=R0scale*diag(diag(VV(regjRE,regjRE)));

%set each parameter to RE value
paramj=zeros(nstate,nfwrd);
for thisvarj=1:nfwrd
    thisvar=fwrdnames{thisvarj};
    for thisstatej=1:nstate
        thisstate=statenames{thisstatej};
        pname=['bet_' thisvar '_' thisstate];
        pval=ARE(varjRE.(thisvar),varjRE.(thisstate));
        [~,paramj(thisstatej,thisvarj)]=ismember(pname,M_.param_names,'rows');       
        assignin('base',pname,pval);
        M_.params(paramj(thisstatej,thisvarj))=pval;
        learncoefs0(thisstatej,thisvarj)=pval;
    end
end
stoch_simul('');


%% run simulation

%initialize
y=zeros(M_.endo_nbr,T+Tpre);
yRE=y;
simulflag=zeros(1,T+Tpre);


learncoefs=zeros(nstate,nfwrd,T+Tpre);
learncoefs(:,:,1)=learncoefs0;
R=R0;
A0=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B0=oo_.dr.ghu(oo_.dr.inv_order_var,:);

for t=2:T+Tpre
    %update parameters
    M_.params(paramj)=learncoefs(:,:,t-1);
    %run resol, solve equilibrium at t
    oo_.dr=resol(0,M_,options_,oo_);
    A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
    B=oo_.dr.ghu(oo_.dr.inv_order_var,:);
    if max(abs(eig(A)))>1 %revert beliefs to last period's in this case
        simulflag(t)=1;
        learncoefs(:,:,t-1)=learncoefs(:,:,t-2);
        M_.params(paramj)=learncoefs(:,:,t-1);
        oo_.dr=resol(0,M_,options_,oo_);
        A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
        B=oo_.dr.ghu(oo_.dr.inv_order_var,:);
    end
    y(:,t)=A*y(:,t-1)+B*v(:,t);
    yRE(:,t)=A0*yRE(:,t-1)+B0*v(:,t);
    
    %belief updating
    xreg=y(statej,t-1);
    yreg=y(fwrdj,t);
    learncoefs(:,:,t)=learncoefs(:,:,t-1)+linearg*inv(R)*xreg*(yreg-learncoefs(:,:,t-1)'*xreg)';
    R=R+linearg*(xreg*xreg'-R);
    if rcond(R)<1e-12; R=0.99*R+0.01*R0; simulflag(t)=2; end %quick fix for rank-deficient R
end

%% save and display results

plotvars={'Y','I','C','L','V','D','pi','i'};
figure;
for i=1:8
    subplot(2,4,i);
    plot(1:T,y(varj.(plotvars{i}),Tpre+(1:T)),1:T,yRE(varj.(plotvars{i}),Tpre+(1:T)));
    title(plotvars{i});
    if i==8; legend('LSQ learning','RE coefs'); end
end
figure;
plot(1:T,simulflag(Tpre+(1:T)));