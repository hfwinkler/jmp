% evoke my_bgg2_plot at the estimated posterior mode

movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod; 
copyfile my_bgg2_parameters_estim.mod my_bgg2_parameters.mod;
try
    my_bgg2_plot;
end
delete my_bgg2_parameters.mod;
movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;
