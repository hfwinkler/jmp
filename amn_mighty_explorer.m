%simulate AMN with more general beliefs

%simulation length
T=20;

%free parameters
beta=0.99;
rho=0.1;
rho_d=0.9;
gamma=0.9;
sigmaeta=1;
sigmanu=0.00;
sigmad=1;
sigmaR=20;


%calculate Kalman filter weights
H=[1 -1 1];
A=[rho 0 0; 1 0 0; 0 0 gamma];
Q=diag([sigmaeta 0 sigmanu]);
R=sigmaR;
Pminus=eye(3);
for i=1:1000
    P=Pminus+Pminus*H'*(H*Pminus*H'+R)^-1*H*Pminus;
    Pminus=A*P*A'+Q;
end
if max(max(abs(Pminus)))>10^3;
    disp(Pminus);
    error 'Pminus doesnt converge';
end
Pw=Pminus*H'/(H*Pminus*H'+R);

%initialise
d=zeros(1,T);
p=zeros(1,T);
x=zeros(1,T);
y=zeros(1,T);
mu=zeros(1,T);
pred=zeros(1,T);

%simulate
for t=2:T
    d(t)=rho_d*d(t-1)+sigmad*(t==2);
    
    p(t)=d(t)+beta/(1-beta)*(gamma^2*mu(t-1)+(rho-1)*rho*x(t-1));
    
    pred(t)=p(t-1)+gamma*mu(t-1)+(rho-1)*x(t-1);
    x(t)=rho*x(t-1)+Pw(1)*(p(t)-pred(t));
    y(t)=x(t-1)+Pw(2)*(p(t)-pred(t));
    mu(t)=gamma*mu(t-1)+Pw(3)*(p(t)-pred(t));
    
end;

%plot
plot(1:T,p,1:T,pred);