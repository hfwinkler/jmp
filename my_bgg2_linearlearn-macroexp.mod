@#line "my_bgg2_linearlearn.mod" 1
%A variant of BGG with limited commitment
%traditional adaptive learning solution

%% load parameters from file:

@#line "my_bgg2_parameters.mod" 1
%% ------------ switches ----------------

%type of preferences (1: separable, 2: Gali, 3: GHH risk-averse

prefs=1;

%set to 1 for variable capacity utilisation

varutil=0;

%type of adjustment costs (0: in capital, 1: in investment)

adjcost=1;

%nominal wage rigidity?

rigidwages=1;

%indexed nominal rigidity (CEE type)?

indexcalvo=0;

%type of monetary policy (1: Taylor, 2: Taylor with natural rate, 3: strict
%inflation targeting (=flexible price allocation)

monpol=1;

%lagged belief updating?
parameters laggedupdating;

laggedupdating=0;

%cum- or ex-dividend stock market value?
parameters cumdividend;

cumdividend=0;

%existence of separate entrepreneurs?
parameters entrepreneurs;

entrepreneurs=1;

%redistribute markup to firms?
parameters markuptofirms;

markuptofirms=0;

%NOTE: need to adjust the dividend payout function in all model files as
%well!

%% -------------- variable and parameter definitions --------------

var A2, Y, C, Ce, I, L, N, V, B, K, R, Rk, w, D, X;
var E, ED, RV;
var q, pi, i, v_i;
var mu;
var u, Q, Qd;
var Delta, Gamma1, Gamma2, DeltaW;
var MU, Utility, Welfare, Welfaree, C_all;

parameters alphaa, betta, b, eta, theta, h;
parameters gama, delta, G, phi, xi, rhoX, BK, A0, PD, Rk0, R0, omega, xr, c;
parameters eqprem;
parameters weighte;
parameters C0, V0, Y0, L0, K0;
parameters kappad, nu, psi;
parameters phi_pi, phi_Y, phi_DY, phi_DV, kappa, sigma, rho_i;
parameters g, rho_mu, a0, sigmav;


@#line "my_bgg2_parameters.mod" 74


@#line "my_bgg2_parameters.mod" 78


@#line "my_bgg2_parameters.mod" 80
var piW, GammaW1, GammaW2; 
parameters kappaW, sigmaW;

@#line "my_bgg2_parameters.mod" 83


@#line "my_bgg2_parameters.mod" 88

varexo eps1, eps2;
var sigeps1;

@#line "my_bgg2_parameters.mod" 92
varexo epsi1, epsi2;

@#line "my_bgg2_parameters.mod" 94

parameters sigmaA1, sigmaA2, sigmai1, sigmai2;
parameters rho, rhoei;


%% ------------ free parameters ---------------

%technology
alphaa=0.33; %capital share
G=1^0.25; %deterministic trend growth
psi=25.2828435707; %COMPUTED BY SMM
nu=3; %elasticity of utilisation

%preferences
R0=1.02^0.25; %SS real rate
eqprem=1; %exp(.01016375); %equity premium
phi=0.33; %inverse Frisch
delta=0.025; %depreciation rate
theta=1; %IES
L0=log(0.7); %steady state log hours
b=0; %consumption habit
h=0.99; %Gali-habit

%entrepreneur welfare weight
weighte=0;

%financials
BK=0.5; %fed funds data
Rk0=alphaa/0.18*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
%PD=154; %PD ratio in the economy
omega=0; %size of entering firms
xr=0.093; %probability of restructuring
c=0.561727852545; %COMPUTED BY SMM
rhoX=0; %AR component on stock price in borrowing constraint

%nominal rigidities
phi_pi=1.5; 
phi_Y=0; 
phi_DY=0;
phi_DV=0;
rho_i=0.85;
kappa=0.408187142509; %COMPUTED BY SMM
sigma=4;
kappaW=0.960810104485; %COMPUTED BY SMM
sigmaW=4;
iota=0.99;

%learning 
g=0.00497926609167; %COMPUTED BY SMM
rho_mu=0.999; %mean reversion in beliefs 
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.01;

%shocks
sigmaA1=0;
sigmaA2=0.00659606518722; %COMPUTED BY SMM
sigmai1=0; %0.00001;
sigmai2=0;

%shock autocorrelations
rho=0.95; %of technology A2
rhoei=0.99; %of inflation target


%% ----------- computed parameters ------------

betta=G^theta/R0;

%gama=1-(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0*eqprem-G/PD);
%omega=(1-gama)/gama*(G/(R0*eqprem-G/PD)-1);
gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));

V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);

kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;

@#line "my_bgg2_parameters.mod" 176
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);

@#line "my_bgg2_parameters.mod" 180

eta=(1-alphaa)*exp(Y0-(phi+1)*L0);


@#line "my_bgg2_parameters.mod" 190










@#line "my_bgg2_linearlearn.mod" 6

%% parameters for adaptive learning




@#line "my_bgg2_linearlearn.mod" 12

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_A2;
        bet_C_A2=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_I;
        bet_C_I=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_B;
        bet_C_B=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_K;
        bet_C_K=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_R;
        bet_C_R=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_w;
        bet_C_w=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_i;
        bet_C_i=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_C_mu;
        bet_C_mu=0;

@#line "my_bgg2_linearlearn.mod" 16

@#line "my_bgg2_linearlearn.mod" 12

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_A2;
        bet_pi_A2=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_I;
        bet_pi_I=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_B;
        bet_pi_B=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_K;
        bet_pi_K=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_R;
        bet_pi_R=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_w;
        bet_pi_w=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_i;
        bet_pi_i=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_pi_mu;
        bet_pi_mu=0;

@#line "my_bgg2_linearlearn.mod" 16

@#line "my_bgg2_linearlearn.mod" 12

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_A2;
        bet_piW_A2=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_I;
        bet_piW_I=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_B;
        bet_piW_B=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_K;
        bet_piW_K=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_R;
        bet_piW_R=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_w;
        bet_piW_w=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_i;
        bet_piW_i=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_piW_mu;
        bet_piW_mu=0;

@#line "my_bgg2_linearlearn.mod" 16

@#line "my_bgg2_linearlearn.mod" 12

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_A2;
        bet_D_A2=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_I;
        bet_D_I=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_B;
        bet_D_B=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_K;
        bet_D_K=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_R;
        bet_D_R=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_w;
        bet_D_w=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_i;
        bet_D_i=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_D_mu;
        bet_D_mu=0;

@#line "my_bgg2_linearlearn.mod" 16

@#line "my_bgg2_linearlearn.mod" 12

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_A2;
        bet_Q_A2=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_I;
        bet_Q_I=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_B;
        bet_Q_B=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_K;
        bet_Q_K=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_R;
        bet_Q_R=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_w;
        bet_Q_w=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_i;
        bet_Q_i=0;

@#line "my_bgg2_linearlearn.mod" 13
        parameters bet_Q_mu;
        bet_Q_mu=0;

@#line "my_bgg2_linearlearn.mod" 16

@#line "my_bgg2_linearlearn.mod" 17

%% model equations
model(linear);

%define Dyare macro-shortcuts to expectations

@#line "my_bgg2_linearlearn.mod" 23
    #EXP_C=

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_A2*A2 +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_I*I +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_B*B +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_K*K +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_R*R +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_w*w +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_i*i +

@#line "my_bgg2_linearlearn.mod" 25
        bet_C_mu*mu +

@#line "my_bgg2_linearlearn.mod" 27
    0;

@#line "my_bgg2_linearlearn.mod" 23
    #EXP_pi=

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_A2*A2 +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_I*I +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_B*B +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_K*K +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_R*R +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_w*w +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_i*i +

@#line "my_bgg2_linearlearn.mod" 25
        bet_pi_mu*mu +

@#line "my_bgg2_linearlearn.mod" 27
    0;

@#line "my_bgg2_linearlearn.mod" 23
    #EXP_piW=

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_A2*A2 +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_I*I +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_B*B +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_K*K +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_R*R +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_w*w +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_i*i +

@#line "my_bgg2_linearlearn.mod" 25
        bet_piW_mu*mu +

@#line "my_bgg2_linearlearn.mod" 27
    0;

@#line "my_bgg2_linearlearn.mod" 23
    #EXP_D=

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_A2*A2 +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_I*I +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_B*B +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_K*K +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_R*R +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_w*w +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_i*i +

@#line "my_bgg2_linearlearn.mod" 25
        bet_D_mu*mu +

@#line "my_bgg2_linearlearn.mod" 27
    0;

@#line "my_bgg2_linearlearn.mod" 23
    #EXP_Q=

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_A2*A2 +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_I*I +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_B*B +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_K*K +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_R*R +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_w*w +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_i*i +

@#line "my_bgg2_linearlearn.mod" 25
        bet_Q_mu*mu +

@#line "my_bgg2_linearlearn.mod" 27
    0;

@#line "my_bgg2_linearlearn.mod" 29
%asset price expectations

@#line "my_bgg2_linearlearn.mod" 33
    #EXP_V=V+rho_mu*mu;

@#line "my_bgg2_linearlearn.mod" 35

%productivity
A2=rho*A2(-1)+sigmaA2*eps2;
sigeps1=sigmaA1*eps1;


%adjustment costs

@#line "my_bgg2_linearlearn.mod" 43
    %in investment
    Q = psi*(I-I(-1)+sigeps1);
    Qd= Q;

@#line "my_bgg2_linearlearn.mod" 51


%household side (only prefs==1, b=0 implemented here!!!)

#lambda=-theta*(EXP_C-C);
# MRS    = phi*L-MU;
MU = -theta*C;

%Welfare (not implemented)
Utility=0;
Welfare=0;
Welfaree=0;

%short-term debt pricing
0=lambda+R;
0=lambda+i-EXP_pi;

%nominal rigidity (backward-looking not implemented)
Gamma1=0;
Gamma2=0;

@#line "my_bgg2_linearlearn.mod" 75
    pi=betta*EXP_pi+(1-betta*kappa)*(1-kappa)/kappa*q;
    Delta=0;

@#line "my_bgg2_linearlearn.mod" 78

w=w(-1)-sigeps1+piW-pi;

@#line "my_bgg2_linearlearn.mod" 81
    piW=betta*EXP_piW+(1-betta*kappaW)*(1-kappaW)/kappaW*(MRS-w);
    DeltaW=0;
    GammaW1=0;
    GammaW2=0;

@#line "my_bgg2_linearlearn.mod" 89

%monetary policy

@#line "my_bgg2_linearlearn.mod" 92
    %option 1: Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(pi+(phi_pi-1)*(pi-v_i)+phi_Y*Y)-sigmai1*epsi1;
    v_i=0+sigmai2*epsi2;

@#line "my_bgg2_linearlearn.mod" 96

@#line "my_bgg2_linearlearn.mod" 101

@#line "my_bgg2_linearlearn.mod" 106

%market clearing and auxiliaries
Y=alphaa*(K(-1)-sigeps1)+(1-alphaa)*(A2+L);
I=1/(1-(1-delta)/G)*(K-(1-delta)/G*(K(-1)-sigeps1));
Y=Ce+exp(C0-Y0)*(C-Ce)+exp(-Y0)*(1-(1-delta)/G)*(I-Ce); 


%financials
E=G/(Rk0-1-(R0-1)*BK)*( exp(Y0)*((1-0)*q+Y)-(1-alphaa)*exp(Y0)*(w+L)-delta/G*(Q+K(-1)-sigeps1)-(R0-1)*BK/G*(B(-1)-sigeps1+R0/(R0-1)*R(-1)) );
X=0;
ED=EXP_D;

%entrepreneurs

@#line "my_bgg2_linearlearn.mod" 120
    #lambdae=0;
    Ce=E+gama*(1-omega)*(Rk0-R0*BK)/(gama*(1-omega)*(Rk0-R0*BK)+(1-gama+gama*omega)*c*(Rk0-1-(R0-1)*BK))*(N-E);
    C_all=exp(C0)/(exp(Y0)-(1-(1-delta)/G))*(C-Ce)+Ce;

@#line "my_bgg2_linearlearn.mod" 128

%firm side
Rk=G*exp(Y0)/Rk0*((1-0)*q+Y-K(-1)+sigeps1-(1-alphaa)*(w+L-K(-1)+sigeps1))+(1-delta)/Rk0*Q;
K+Q=(1-gama+gama*omega)*((Rk0-R0*BK)*N-c*(Rk0-1-(R0-1)*BK)*E)/G+BK*B;
w=q+Y-L;


@#line "my_bgg2_linearlearn.mod" 137
    B=xi*(1-xr)*betta/BK*(lambda+EXP_Q+K)+xi*xr*V0/BK*V+xi*xr*B;

@#line "my_bgg2_linearlearn.mod" 139
N=Rk0/(Rk0-R0*BK)*(Rk+K(-1)-sigeps1)-R0*BK/(Rk0-R0*BK)*(R(-1)+B(-1)-sigeps1);
D=E+gama*(Rk0-R0*BK)/(gama*(Rk0-R0*BK)+(1-gama)*c*(Rk0-1-(R0-1)*BK))*(N-E);

%capacity choice (not implemented)
u=0;

%asset pricing

@#line "my_bgg2_linearlearn.mod" 150
    V=lambdae+1/(1+PD)*EXP_D+PD/(1+PD)*EXP_V;
    RV=(PD*V+D)/(1+PD) - sigeps1-V(-1);

@#line "my_bgg2_linearlearn.mod" 153

%learning equivalents
mu=rho_mu*mu(-1)-a0*V(-1)+g*(V-V(-1)-mu(-1));

end;

steady_state_model;
V=0;
end;


@#line "my_bgg2_commonend.mod" 1
%% common end lines

%shocks
shocks;
var eps1; stderr 1;
var eps2; stderr 1;

@#line "my_bgg2_commonend.mod" 8
var epsi1; stderr 1;
var epsi2; stderr 1;

@#line "my_bgg2_commonend.mod" 11
end;

stoch_simul(order=2,pruning,irf=0,nomoments,nocorr,ar=0,noprint);

@#line "my_bgg2_linearlearn.mod" 164



