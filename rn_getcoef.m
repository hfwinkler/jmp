% this script finds the rule for the natural rate of interest and writes it
% to a file


%% write new parameter file

movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod; 

delete('my_bgg2_parameters.m');
fin = fopen('my_bgg2_parameters_old.mod');
fout = fopen('my_bgg2_parameters.mod', 'wt');

tline = 'dummy';
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) 
        if strncmp(strtrim(tline),'@#define monpol',15);
            fprintf(fout, '@#define monpol=3\n');
        else
            fprintf(fout, '%s\n', tline);
        end
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);

%% run Dynare with flexible prices

dynare my_bgg2_RE noclearall;
options_.noprint=1;
options_.order=1;
options_.irf=0;
options_.nomoments=1;
options_.hp_filter=0;
stoch_simul('');
if isempty(oo_.dr.ghx);
    error Could not solve learning model.;
end
dr=oo_.dr;
nexo=M_.exo_nbr;
nendo=M_.endo_nbr;
nstate=length(dr.state_var);

%% get coefficients for rn

[~,ji]=ismember('i',M_.endo_names(dr.order_var,:),'rows');

rncoefs=struct;
for i=1:nstate
    if dr.ghx(ji,i)~=0
        rncoefs.(deblank(M_.endo_names(dr.state_var(i),:)))=dr.ghx(ji,i);
    end
end
for i=1:nexo
    if dr.ghu(ji,i)~=0
        rncoefs.(deblank(M_.exo_names(i,:)))=dr.ghu(ji,i);
    end
end

%store as array,too,  because Dynare cannot handle structs
rncoefmat(1)=rncoefs.K;
rncoefmat(2)=rncoefs.B;
rncoefmat(3)=rncoefs.R;
rncoefmat(4)=rncoefs.A2;

save rn_coefs rncoefs rncoefmat;

%% clean up

delete my_bgg2_parameters.mod;
movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;

cleanup_dyn my_bgg2_RE;