% display stats and plots for policy collection and across different models

%% preliminaries

mlist={'learn' 'RE'};
dashstyles={'-','--'};
collectionname='RandRcoll';
varnames={'pi' 'V' 'i' 'Y'};
order=2;

load([collectionname '.mat']);
policynames=fieldnames(policies);
N=length(policynames);

%% parse parameter file to use in functions

dynare my_bgg2_parameters onlymacro savemacro;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', strtrim(tline));
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% run Dynare once for every model

welfields=cell(0);

for mi=1:length(mlist)
    model=mlist{mi};
    dynare(['my_bgg2_' model],'nostrict noclearall');
    nendo=M_.endo_nbr; %number of endogenous variables
    nexo=M_.exo_nbr-2*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)
    nendolag=sum(strncmp('AUX_ENDO_LAG',cellstr(M_.endo_names),12)); %number of endogenous lagged variables appended by Dynare
    M.(model)=M_;
    oo.(model)=oo_;

    SS.(model)=cell(N,1);
    A.(model)=cell(N,1);
    B.(model)=cell(N,1);
    VV.(model)=cell(N,1);
    welf.(model)=cell(N,1);
    params=cell(N,1);
    
    %run evalpol3 once   
            
    options_.noprint=1;
    options_.order=order;
    options_.irf=0;
    options_.ar=0;
    options_.nomoments=0;
    options_.hp_filter=0;
    options_.periods=2e4;
    options_.PLM=0;
    maxiter=1;
     
    my_bgg2_evalpol3([],[],0,maxiter,M_,oo_,options_);
    
    %then loop over rules
    fprintf('Looping');
    for n=1:N
        policy=policies.(policynames{n});
        paramnames=fieldnames(policy);
        paramvalues=struct2array(policy);

        oo_.dr.ghx=[];
        m=[];
        try %if we produce an error at any stage we need to catch it
            fprintf('.');
            [~,m]=my_bgg2_evalpol3(paramnames,paramvalues,0,maxiter,M_,oo_,options_);
            if not(isempty(m))
                welfields=fieldnames(m);
                if size(welf.(model),2)==1; welf.(model)=cell(N,length(welfields)); end
                welf.(model)(n,:)=struct2cell(m)';
            end
        catch err
            disp(err.message);
        end
        
    end
    fprintf('done.\n');
    cleanup_dyn(model);
    
end

%% output

for i=1:length(mlist)
    model=mlist{i};
    welf.(model)(cellfun(@isempty,welf.(model)))={NaN};
end

nrows=3;
ncols=ceil(length(welfields)/nrows);
plotdata=zeros(length(mlist),N);

figure; set(gcf,'name','Welfare measures');
disp(policynames');

for j=1:length(welfields)
    fprintf('\tWelfare measure %8s:\n', welfields{j});
    for i=1:length(mlist)
        fprintf('model %8s: ', mlist{i});
        model=mlist{i};
        plotdata(i,:)=[welf.(model){:,j}];
        disp(plotdata(i,:));
    end
    subplot(nrows,ncols,j); h=bar(plotdata'); title(welfields{j});
    set(h(1),'BaseValue',min(0,min(plotdata(:))));
    set(gca,'XTickLabel',policynames);
    rotateXLabels(gca, 45);
end


    