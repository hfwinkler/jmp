function [y_,yf_,simulflag]=simult1_linearlearn(y0,oo,ex_,iorder,M_,options_,linearg,R0scale,maxfh)
% Simulates the modelwith learning about everything
%
% INPUTS
%    y0       [double]   n*1 vector, initial value relative to SS!
%    oo.dr       [struct]   matlab's structure where the reduced form solution of the model is stored.
%    ex_      [double]   T*q matrix of innovations.
%    iorder   [integer]  order of the taylor approximation.
%    M_       [struct]   M structure.
%    options_ [struct]   options structure.
%    linearg  [double]   learning gain for LSQ learning.
%
% OUTPUTS
%    y_       [double]   n*(T+1) time series for the endogenous variables.
%


%% sim parameters

%learning gain
%linearg=0.01; from function argument
%R0scale=linearg/(1-linearg);
if isempty(options_.qz_criterium); options_.qz_criterium=1; end

%simulation length
T=size(ex_,1)+1;

%learning variables: regressors and regressands
fwrdnames={'C', 'pi', 'piW', 'D','Q'};
statenames={'A2' 'I','B','K','R','w','i','mu'};

%% various indices

%retrieve state variables, C matrix
k2 = oo.dr.kstate(find(oo.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
oo.dr.state_var=oo.dr.order_var(k2);
C=zeros(length(oo.dr.state_var),M_.endo_nbr);
for i=1:length(oo.dr.state_var)
    C(i,oo.dr.state_var(i))=1;
end

%stuff
nendo=M_.endo_nbr;
nfwrd=length(fwrdnames);
nstate=length(statenames);

%indices
get_dynare_indices;
fwrdj=zeros(1,nfwrd);
for i=1:nfwrd
    fwrdj(i)=varj.(fwrdnames{i});
end
statej=zeros(1,nstate);
for i=1:nstate
    statej(i)=varj.(statenames{i});
end
C1=zeros(nendo,nstate);
for i=1:nstate
    C1(statej(i),i)=1;
end

%% initialize


%pre-allocate parameter vector and precision matrix
learncoefs0=zeros(nstate,nendo);
R0=zeros(nstate);

%load learning parameter values from RE file
load linearREcoefs;

%calculate variance matrix of regressors
nendoRE=size(ARE,1);
regjRE=zeros(1,nstate);
for i=1:nstate
    regjRE(i)=varjRE.(statenames{i});
end
Vvec=(eye(nendoRE^2)-kron(ARE,ARE))\vec(BRE*BRE');
VV=zeros(nendoRE);
VV(:)=Vvec;
R0=R0scale*diag(diag(VV(regjRE,regjRE)));

%initialize learning coefficients
for thisvarj=1:nendo
    thisvar=deblank(M_.endo_names(thisvarj,:));
    for thisstatej=1:nstate
        thisstate=statenames{thisstatej};
        learncoefs0(thisstatej,thisvarj)=ARE(varjRE.(thisvar),varjRE.(thisstate));
    end
end

%set each learning parameter in mod file to RE value
paramj=zeros(nstate,nfwrd);
for thisvarj=1:nfwrd
    thisvar=fwrdnames{thisvarj};
    for thisstatej=1:nstate
        thisstate=statenames{thisstatej};
        pname=['bet_' thisvar '_' thisstate];
        pval=ARE(varjRE.(thisvar),varjRE.(thisstate));
        [~,paramj(thisstatej,thisvarj)]=ismember(pname,M_.param_names,'rows');       
        assignin('base',pname,pval);
        M_.params(paramj(thisstatej,thisvarj))=pval;
    end
end
oo.dr=resol(0,M_,options_,oo);


%% run simulation

%initialize
y_=zeros(nendo,T);
y_(:,1)=y0;
yf_=zeros(nendo,T,maxfh+1);

simulflag=zeros(1,T);


learncoefs=zeros(nstate,nendo,T);
learncoefs(:,:,1)=learncoefs0;
R=R0;
A0=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
B0=oo.dr.ghu(oo.dr.inv_order_var,:);

for t=2:T
    %update parameters
    M_.params(paramj)=learncoefs(:,fwrdj,t-1);
    %run resol, solve equilibrium at t
    try
        oo.dr=resol(0,M_,options_,oo);
    catch
        simulflag(t)=1;
    end
    A=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
    B=oo.dr.ghu(oo.dr.inv_order_var,:);
    if max(abs(eig(A)))>=1; simulflag(t)=1; end
    if simulflag(t) %revert beliefs to last period's in this case
        learncoefs(:,:,t-1)=learncoefs0; %learncoefs(:,:,t-2);
        M_.params(paramj)=learncoefs(:,fwrdj,t-1);
        oo.dr=resol(0,M_,options_,oo);
        A=oo.dr.ghx(oo.dr.inv_order_var,:)*C;
        B=oo.dr.ghu(oo.dr.inv_order_var,:);
    end
    y_(:,t)=A*y_(:,t-1)+B*ex_(t-1,:)';
    
    %forecasts
    A1=learncoefs(:,:,t-1)'*C1';
    yf_(:,t,1)=y_(:,t);
    for h=1:maxfh
        yf_(:,t,h+1)=A1*yf_(:,t,h);
    end
    
    %belief updating
    xreg=y_(statej,t-1);
    yreg=y_(:,t);
    learncoefs(:,:,t)=learncoefs(:,:,t-1)+linearg*inv(R)*xreg*(yreg-learncoefs(:,:,t-1)'*xreg)';
    R=R+linearg*(xreg*xreg'-R);
    if rcond(R)<1e-12
        R=0.99*R+0.01*R0;  %quick fix for rank-deficient R
        simulflag(t)=2; 
    end 
end

end