% check whether the conditions for existence and uniqueness of our
% equilibrium are satisfied.

% If this scripts does not print anything, we're in the clear.


% Check that return on capital is sufficiently high for firms to
% invest

bettatil=1/R0/eqprem;

if Rk0*bettatil<=1
    disp('Return on capital too low!');
end

% Compute firm value function
cB=@(B) bettatil*(Rk0-R0)*(1+(1-gama)*(B-1)*(1-c));
cK=@(B) bettatil*(Rk0+(1-gama)*(B-1)*((1-c)*Rk0+c));
Bs=fzero(@(B) (cK(B)-B)*(1-xi*(xr+(1-xr)/R0+xr*cB(B))) + cB(B)*xi*((1-xr)/R0+xr*cK(B)) , 1);


% Check that return on capital is sufficiently high for firms to invest
if xi*((1-xr)/R0+xr+xr*cB(Bs))>=1
    disp('Borrowing constraint slack at equilibrium!');
end

% Check for absence of Miao-Wang-style bubble
if bettatil*(1-gama)*( 1 + xi*xr*cB(Bs)/(1-xi*(xr+(1-xr)/R0+xr*cB(Bs))) ) >=1
    disp('Bubble equilibria cannot be ruled out at As=0!');
end

cB1=(1-bettatil*(1-gama))/(xi*xr)*(1-xi*xr-xi*(1-xr)*betta);
Bs1=(1-bettatil*(1-gama))/(xi*bettatil*xr)*(1-xi*xr)/(1-gama);
Rk1=R0+cB1/bettatil/(1+(1-gama)*(1-c)*(Bs1-1));
if Rk1 < Rk0
    disp('Bubble equilibria cannot be ruled out at As>0!');
end

