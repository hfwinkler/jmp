%A variant of BGG with limited commitment
%traditional adaptive learning solution
%NOTE run my_bgg2_linearRE.mod first!!!

%% load parameters from file:
@#include "my_bgg2_parameters.mod"

%% parameters for adaptive learning
@#define statevars=["A2", "I","B","K","R","w","i","mu"]
@#define fwrdvars=["C", "pi", "piW", "D", "Q"]

@#for thisfwrdvar in fwrdvars
    @#for thisstatevar in statevars
        parameters bet_@{thisfwrdvar}_@{thisstatevar};
        bet_@{thisfwrdvar}_@{thisstatevar}=0;
    @#endfor
@#endfor

%manually put in an smm gain for comparability
g=0.0052;

%% model equations
model(linear);

%define Dyare macro-shortcuts to expectations
@#for thisfwrdvar in fwrdvars
    #EXP_@{thisfwrdvar}=
    @#for thisstatevar in statevars
        bet_@{thisfwrdvar}_@{thisstatevar}*@{thisstatevar} +
    @#endfor
    0;
@#endfor
%asset price expectations
@#if laggedupdating
    #EXP_V=V+rho_mu*mu(-1);
@#else
    #EXP_V=V+rho_mu*mu;
@#endif

%productivity
A2=rho*A2(-1)+sigmaA2*eps2;
sigeps1=sigmaA1*eps1;


%adjustment costs
@#if adjcost
    %in investment
    Q = psi*(I-I(-1)+sigeps1);
    Qd= Q;
@#else
    %in capital
    Q = psi*(K-K(-1)+sigeps1);
    Qd= Q;
@#endif


%household side (only prefs==1, b=0 implemented here!!!)

#lambda=-theta*(EXP_C-C);
# MRS    = phi*L-MU;
MU = -theta*C;

%Welfare (not implemented)
Utility=0;
Welfare=0;
Welfaree=0;

%short-term debt pricing
0=lambda+R;
0=lambda+i-EXP_pi;

%nominal rigidity (backward-looking not implemented)
Gamma1=0;
Gamma2=0;
@#if monpol==3
    pi=0;
    Delta=0;
@#else
    pi=betta*EXP_pi+(1-betta*kappa)*(1-kappa)/kappa*q;
    Delta=0;
@#endif

w=w(-1)-sigeps1+piW-pi;
@#if rigidwages    
    piW=betta*EXP_piW+(1-betta*kappaW)*(1-kappaW)/kappaW*(MRS-w);
    DeltaW=0;
    GammaW1=0;
    GammaW2=0;
@#else
    DeltaW=0;
    w = MRS;
@#endif

%monetary policy
@#if monpol==1 
    %option 1: Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(pi+(phi_pi-1)*(pi-v_i)+phi_Y*Y)-sigmai1*epsi1;
    v_i=0+sigmai2*epsi2;
@#endif
@#if monpol==2 
    %option 2: extended Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(pi+(phi_pi-1)*(pi-v_i)+phi_Y*Y+phi_DY*(Y-Y(-1)+sigeps1)+phi_DV*(V-V(-1)+sigeps1)+phi_LDY*(Y(-1)-Y(-2)+sigeps1(-1))+phi_LDV*(V(-1)-V(-2)+sigeps1(-1))+phi_BK*(B-K))-sigmai1*epsi1;
    v_i=rhoei*v_i(-1)+sigmai2*epsi2;
@#endif
@#if monpol==3 
    %option 3: inflation targeting
    q=0;
    v_i=0;
@#endif

%market clearing and auxiliaries
Y=alphaa*(K(-1)-sigeps1)+(1-alphaa)*(A2+L);
I=1/(1-(1-delta)/G)*(K-(1-delta)/G*(K(-1)-sigeps1));
Y=Ce+exp(C0-Y0)*(C-Ce)+exp(-Y0)*(1-(1-delta)/G)*(I-Ce); 


%financials
E=G/(Rk0-1-(R0-1)*BK)*( exp(Y0)*((1-@{markuptofirms})*q+Y)-(1-alphaa)*exp(Y0)*(w+L)-delta/G*(Q+K(-1)-sigeps1)-(R0-1)*BK/G*(B(-1)-sigeps1+R0/(R0-1)*R) );
X=0;
ED=EXP_D;

%entrepreneurs
@#if entrepreneurs
    #lambdae=0;
    Ce=E+gama*(1-omega)*(Rk0-R0*BK)/(gama*(1-omega)*(Rk0-R0*BK)+(1-gama+gama*omega)*c*(Rk0-1-(R0-1)*BK))*(N-E);
    C_all=exp(C0)/(exp(Y0)-(1-(1-delta)/G))*(C-Ce)+Ce;
@#else
    #lambdae=lambda;
    Ce=0;
    C_all=C;
@#endif

%firm side
Rk=G*exp(Y0)/Rk0*((1-@{markuptofirms})*q+Y-K(-1)+sigeps1-(1-alphaa)*(w+L-K(-1)+sigeps1))+(1-delta)/Rk0*Q;
K+Q=(1-gama+gama*omega)*((Rk0-R0*BK)*N-c*(Rk0-1-(R0-1)*BK)*E)/G+BK*B;
w=q+Y-L;

@#if cumdividend
    B=xi*(1-xr)*betta/BK*(lambda+EXP_Q+K)+xi*xr*(betta*G*V0/BK)*(lambdae+EXP_V)+xi*xr*B;
@#else    
    B=xi*(1-xr)*betta/BK*(lambda+EXP_Q+K)+xi*xr*V0/BK*V+xi*xr*B;
@#endif
N=Rk0/(Rk0-R0*BK)*(Rk+K(-1)-sigeps1)-R0*BK/(Rk0-R0*BK)*(R+B(-1)-sigeps1);
D=E+gama*(Rk0-R0*BK)/(gama*(Rk0-R0*BK)+(1-gama)*c*(Rk0-1-(R0-1)*BK))*(N-E);

%capacity choice (not implemented)
u=0;

%asset pricing
@#if cumdividend
    V=G/R0/eqprem/PD*D+(1-G/R0/eqprem/PD)*(lambdae+EXP_V);
    RV=V-sigeps1-(PD*V(-1)+D(-1))/(PD+1);
@#else
    V=lambdae+1/(1+PD)*EXP_D+PD/(1+PD)*EXP_V;
    RV=(PD*V+D)/(1+PD) - sigeps1-V(-1);
@#endif

%learning equivalents
mu=rho_mu*mu(-1)-a0*V(-1)+g*(V-V(-1)-mu(-1));

end;

steady_state_model;
V=0;
end;

@#include "my_bgg2_commonend.mod"


