% Dynare moment matcher

global M_ oo_ options_

loadlast=0;
T=1e4; %length of simulation

%file name
fname='MSW_M1';

%parameters and ranges to search over
paramnames={'sigmaA2','kappa','psi','c'};
J=length(paramnames);

lbounds=[0; 0; 0; 0];
ubounds=[.1; .99; 20; .99];

%% run Dynare once
dynare(fname,'noclearall');
options_.noprint=1;
options_.order=1;
options_.irf=0;
options_.ar=0;
options_.hp_filter=0;

%% match moments

param0=zeros(J,1);
for i=1:J
    eval(['param0(' num2str(i) ')=' paramnames{i} ';']);
end
%can also start from previously found optimum
if loadlast; load momentmatch_result; param0=paramstar(1:J); end;
fprintf('\nStarting values:\n');
for i=1:length(paramnames)
    fprintf('%8s = %3.4g\n',paramnames{i},param0(i));
end

[res,moments0]=momentloss(paramnames,param0,T);
fprintf('\nInitial loss value:  %3.4g\n',res);

%fmincon:
options=optimset('Algorithm','sqp','Display','iter','MaxFunEvals',10^4);
paramstar=fmincon(@(x) momentloss(paramnames,x,T) ,param0,[],[],[],[],lbounds,ubounds,[],options);
save momentmatch_result paramstar;

[res,moments,targets,weights]=momentloss(paramnames,paramstar,T);

%check for uniqueness:
if uniquecheck
    a=.8; %how far away to go to the boundary

    bestparams=zeros(J,K+1);
    bestres=zeros(1,K+1);
    bestparams(:,K+1)=paramstar;
    bestres(:,K+1)=res;
    fprintf('Looking for multiplicity, %i iterations. Initial optimum has loss: %3.4g\n',K,res);
    options=optimset('Algorithm','sqp','Display','off','MaxFunEvals',10^4);
    for k=1:uniquecheck
        rng shuffle;
        randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        try;
            bestparams(:,k)=fmincon(@(x) momentloss(paramnames,x,T) ,(1-a)*paramstar+a*randompoint,[],[],[],[],lbounds,ubounds,[],options);
        end
        distance=norm(bestparams(:,k)-paramstar);
        bestres(k)=momentloss(paramnames,bestparams(:,k),T);
        fprintf('Perturbation #%i at distance: %3.4g, improvement: %3.4g\n',k,distance,res-bestres(k));
        if bestres(k)<res
            paramstar=bestparams(:,k);
            res=bestres(k);
            save momentmatch_result paramstar;
        end
    end

    disp('All optima:');
    disp(bestparams);
end
[res,moments,targets,weights]=momentloss(paramnames,paramstar,T);

%% print and save results

fprintf('Optimal values:\n');
for i=1:length(paramnames)
    fprintf('%8s = %3.4g\n',paramnames{i},paramstar(i));
end
fprintf('\nmoments0=\n'); disp(moments0);
fprintf('\nmoments*=\n'); disp(moments);
fprintf('targets=\n'); disp(targets);
fprintf('weights=\n'); disp(weights);


