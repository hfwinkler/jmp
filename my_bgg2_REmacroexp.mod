@#line "my_bgg2_RE.mod" 1
%A variant of BGG with limited commitment
%RE solution

%load parameters from file:

@#line "my_bgg2_parameters.mod" 1
%% ------------ switches ----------------

%type of preferences (1: separable, 2: Gali, 3: GHH risk-averse

prefs=1;

%set to 1 for variable capacity utilisation

varutil=0;

%type of adjustment costs (0: in capital, 1: in investment)

adjcost=1;

%nominal wage rigidity?

rigidwages=1;

%indexed nominal rigidity (CEE type)?

indexcalvo=0;

%type of monetary policy (1: Taylor, 2: Taylor with natural rate, 3: strict
%inflation targeting (=flexible price allocation)

monpol=1;

%lagged belief updating?
parameters laggedupdating;

laggedupdating=0;

%cum- or ex-dividend stock market value?
parameters cumdividend;

cumdividend=0;

%existence of separate entrepreneurs?
parameters entrepreneurs;

entrepreneurs=1;

%redistribute markup to firms?
parameters markuptofirms;
markuptofirms=0;


%% -------------- variable and parameter definitions --------------

var A2, Y, C, Ce, I, L, N, V, B, K, R, Rk, w, D, X, C_all;
var E, ED, RV;
var q, pi, i, v_i;
var mu;
var u, Q, Qd;
var Delta, Gamma1, Gamma2, DeltaW;
var MU, Utility, Welfare, Welfaree;

parameters alphaa, betta, b, eta, theta, h;
parameters gama, delta, G, phi, xi, rhoX, BK, A0, PD, Rk0, R0, omega, xr, c;
parameters eqprem;
parameters weighte;
parameters C0, V0, Y0, L0, K0;
parameters kappad, nu, psi;
parameters phi_pi, phi_Y, phi_DY, phi_DV, kappa, sigma, rho_i;
parameters g, rho_mu, a0, sigmav;


@#line "my_bgg2_parameters.mod" 71


@#line "my_bgg2_parameters.mod" 75


@#line "my_bgg2_parameters.mod" 77
var piW, GammaW1, GammaW2; 
parameters kappaW, sigmaW;

@#line "my_bgg2_parameters.mod" 80


@#line "my_bgg2_parameters.mod" 85

varexo eps1, eps2;
var sigeps1;

@#line "my_bgg2_parameters.mod" 89
varexo epsi1, epsi2;

@#line "my_bgg2_parameters.mod" 91

parameters sigmaA1, sigmaA2, sigmai1, sigmai2;
parameters rho, rhoei;


%% ------------ free parameters ---------------

%technology
alphaa=0.33; %capital share
G=1^0.25; %deterministic trend growth
psi=18.8227871787; %COMPUTED BY SMM
nu=3; %elasticity of utilisation

%preferences
R0=1.02^0.25; %SS real rate
eqprem=1; %exp(.01016375); %equity premium
phi=0.33; %inverse Frisch
delta=0.025; %depreciation rate
theta=1; %IES
L0=log(0.7); %steady state log hours
b=0; %consumption habit
h=0.99; %Gali-habit

%entrepreneur welfare weight
weighte=0;

%financials
BK=0.4; %fed funds data
Rk0=alphaa/0.18*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
%PD=154; %PD ratio in the economy
omega=0; %size of entering firms
xr=0.093; %probability of restructuring
c=0.498244068029; %COMPUTED BY SMM
rhoX=0; %AR component on stock price in borrowing constraint

%nominal rigidities
phi_pi=1.5; 
phi_Y=0; 
phi_DY=0;
phi_DV=0;
rho_i=0.85;
kappa=0.373020252398; %COMPUTED BY SMM
sigma=4;
kappaW=0.964651198636; %COMPUTED BY SMM
sigmaW=4;
iota=0.99;

%learning 
g=0.00509244752504; %COMPUTED BY SMM
rho_mu=0.999; %mean reversion in beliefs 
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.001;

%shocks
sigmaA1=0;
sigmaA2=0.0109658620663; %COMPUTED BY SMM
sigmai1=0.00001;
sigmai2=0;

%shock autocorrelations
rho=0.95; %of technology A2
rhoei=0.99; %of inflation target


%% ----------- computed parameters ------------

betta=G^theta/R0;

%gama=1-(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0*eqprem-G/PD);
%omega=(1-gama)/gama*(G/(R0*eqprem-G/PD)-1);
gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));


V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);

kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;

@#line "my_bgg2_parameters.mod" 173
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);

@#line "my_bgg2_parameters.mod" 177

eta=(1-alphaa)*exp(Y0-(phi+1)*L0);


@#line "my_bgg2_parameters.mod" 187










@#line "my_bgg2_RE.mod" 6

model;


@#line "my_bgg2_common.mod" 1
%common model equations for all bgg2 variants

%productivity
A2=rho*A2(-1)+sigmaA2*eps2;
sigeps1=sigmaA1*eps1;


%adjustment costs

@#line "my_bgg2_common.mod" 10
    %in investment
    exp(Q) = 1+psi*(exp(I-I(-1)+sigeps1)-1);
    exp(Qd)= exp(Q)*(1-delta);

@#line "my_bgg2_common.mod" 18


%household side - WARNING ONLY theta=1 AND b=0 IS IMPLEMENTED FOR WELFARE!

# UL = -exp(L+DeltaW)^(1+phi)/(1+phi);
# dUL = exp(L+DeltaW)^phi;
#lambda=betta*MU(+1)/MU*(G*exp(sigeps1(+1)))^-theta;


@#line "my_bgg2_common.mod" 27
    # MRS    = eta*dUL*(exp(C-C0)/(1-b)-b/(1-b)*exp(C(-1)-C0-sigeps1))^theta;
    MU = (exp(C-C0)/(1-b)-b/(1-b)*exp(C(-1)-C0-sigeps1))^-theta;
    Utility=C-C0+eta*UL;

@#line "my_bgg2_common.mod" 31

@#line "my_bgg2_common.mod" 37

@#line "my_bgg2_common.mod" 42

%Welfare
Welfare=(1-betta)*Utility+betta*Welfare(+1);
Welfaree=(1-1/R0)*Ce+1/R0*Welfaree(+1);

%short-term debt pricing
1=lambda*exp(R);
1=lambda*exp(i-pi(+1));

%nominal rigidity
Gamma1=0;
Gamma2=0;
pi=betta*pi(+1)+(1-betta*kappa)*(1-kappa)/kappa*q;
Delta=0;


w=w(-1)-sigeps1+piW-pi;

piW=betta*piW(+1)+(1-betta*kappaW)*(1-kappaW)/kappaW*(log(MRS)-w);
DeltaW=0;
GammaW1=0;
GammaW2=0;


%monetary policy

@#line "my_bgg2_common.mod" 85
    %option 1: Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(log(R0)+pi+(phi_pi-1)*(pi-v_i)+phi_Y*(Y-Y0)+phi_DY*(Y+sigeps1-Y(-1))+phi_DV*(V-V(-1)+sigeps1))-sigmai1*epsi1;
    v_i=rhoei*v_i(-1)+sigmai2*epsi2;

@#line "my_bgg2_common.mod" 89

@#line "my_bgg2_common.mod" 94

@#line "my_bgg2_common.mod" 99

%market clearing and auxiliaries
exp(Y)=exp(alphaa*(u+K(-1)-log(G)-sigeps1)+(1-alphaa)*(A0+A2+L));
exp(I)=exp(K)-(1-delta)*exp(K(-1)-log(G)-sigeps1)*(1-kappad/nu*(exp(nu*u)-1));

@#line "my_bgg2_common.mod" 104
    exp(Y-Delta)=exp(C)+Ce+exp(I)+psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;

@#line "my_bgg2_common.mod" 108
C_all=log(exp(C)+Ce);

%financials
exp(E)=exp((1-markuptofirms)*q+Y)-exp(w+L)+(exp(Qd)-exp(Q))*exp(K(-1)-sigeps1)/G-(exp(R(-1))-1)*exp(B(-1)-sigeps1)/G;
X=rhoX*(X(-1)-sigeps1)+(1-rhoX)*V;
ED=D(+1);



@#line "my_bgg2_RE.mod" 10

%entrepreneurs

@#line "my_bgg2_RE.mod" 13
    #lambdae=1/R0/eqprem;
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;

@#line "my_bgg2_RE.mod" 20

%firm side
exp(Rk)=(exp((1-markuptofirms)*q+Y)-exp(w+L))/exp(K(-1)-log(G)-sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(K+Q)=(1-gama+gama*omega)*(exp(N)-c*exp(E))+exp(B);
exp(w)=(1-alphaa)*exp(q+Y-L);

@#line "my_bgg2_RE.mod" 28
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(exp(V)+exp(B)));

@#line "my_bgg2_RE.mod" 30
exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));
exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs

@#line "my_bgg2_RE.mod" 37
    u=0;

@#line "my_bgg2_RE.mod" 39

%asset pricing

@#line "my_bgg2_RE.mod" 45
    exp(V)=G*lambdae*(exp(D(+1)+sigeps1(+1))+(1-gama)/(1-gama+gama*omega)*exp(V(+1)+sigeps1(+1)));
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;

@#line "my_bgg2_RE.mod" 48

%learning equivalents
mu=V(+1)+sigeps1(+1)-V;

end;

steady_state_model;

Rk=log(Rk0);


@#line "my_bgg2_commonSS.mod" 1
% common steady state calculations for the my_bgg2 family

%productivity
A2=0;

%interest rate
R=log(R0);

%supply side
u=0;
K=log(alphaa*G)+Y0-log(exp(Rk)-1+delta);
I=log((G-1+delta)/G)+K;
Y=Y0;
C=C0;
L=L0;
w=log(1-alphaa)+Y-L;

%adjustment costs
Q=0;
Qd=log(1-delta);

%financials
B=log(BK)+K;
V=V0;
X=V;
N=log((Rk0-R0*BK)/G)+K;
E=log(alphaa*exp(Y)-delta*exp(K)/G-(R0-1)*BK*exp(K)/G);
D=log(gama*exp(N)+(1-gama)*c*exp(E));
ED=D;

%nominal rigidities
q=0;
pi=0;
i=R;
Delta=0;
Gamma1=0;
Gamma2=0;
DeltaW=0;
PCP=0;

@#line "my_bgg2_commonSS.mod" 41
    piW=0;
    GammaW1=0;
    GammaW2=0;

@#line "my_bgg2_commonSS.mod" 45

%learning
mu=0;

%stock price variables
RV=log(eqprem);


@#line "my_bgg2_commonSS.mod" 55

%case of Gali-habit

@#line "my_bgg2_commonSS.mod" 60

%utility and welfare
UL0 = -exp(L-DeltaW)^(1+phi)/(1+phi);

@#line "my_bgg2_commonSS.mod" 64
    Utility=eta*UL0;
    MU=1;

@#line "my_bgg2_commonSS.mod" 67

@#line "my_bgg2_commonSS.mod" 71

@#line "my_bgg2_commonSS.mod" 75
Welfare=Utility;


@#line "my_bgg2_RE.mod" 59


@#line "my_bgg2_RE.mod" 61
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);

@#line "my_bgg2_RE.mod" 65
Welfaree=Ce;
C_all=log(exp(C)+Ce);

end;


@#line "my_bgg2_commonend.mod" 1
%% common end lines

%shocks
shocks;
var eps1; stderr 1;
var eps2; stderr 1;

@#line "my_bgg2_commonend.mod" 8
var epsi1; stderr 1;
var epsi2; stderr 1;

@#line "my_bgg2_commonend.mod" 11
end;

stoch_simul(order=2,pruning,irf=0,nomoments,nocorr,ar=0);

@#line "my_bgg2_RE.mod" 71



