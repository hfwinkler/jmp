%% switches:
%type of preferences (1: separable, 2: Jaimovich-Rebelo, 3: GHH risk-averse
@#define prefs=1
prefs=@{prefs};

%set to 1 for variable capacity utilisation
@#define varutil=1
varutil=@{varutil};

%rational expectations households?
parameters veryrational; %must be the first declared parameter!
@#define veryrational=0
veryrational=@{veryrational};

%type of monetary policy (1: simple Taylor, 2: rate smoothing, 3: strict
%inflation targeting (=flexible price allocation)
@#define monpol=1
monpol=@{monpol};

%bond market structure (1: real one-period, 2: nominal one-period, 
%3: real stochastic, 4: nominal stochastic)
@#define bondmarket=1
bondmarket=@{bondmarket};

%redistribute dividends to households?
@#define redistN=0
redistN=@{redistN};

%redistribute markup to firms?
parameters markuptofirms;
markuptofirms=0;

%preferences over labour (0) or leisure (1)?
@#define leisure=0
leisure=@{leisure};

%% variable definitions

var Rk, N, B, K, V, D, A2, R, Y, C, I, w, L, C_all;
var RB, PB;
var u, Q, Qd;
var q, pi, i, v_i, Delta, Gamma1, Gamma2;
var mu, X;
var VN, RV;
var C_level, L_level, pi_level;
@#if prefs==2
    %define additional variables for Jaimovich-Rebelo
    var X_L, deltaJR, X_L_level;
    parameters deltaJR0;
@#endif
%define additional variables for stochastic maturity
@#if bondmarket==3
    var mubond, RL, RLn;
@#endif
@#if bondmarket==4
    var mubond, iL, iLn;
@#endif

@#if monpol==2
    var Rn; 
    parameters rnK, rnB, rnR, rnA;
@#endif

varexo eps1, eps2, epsi, epsN;
var epsN1;

parameters alphaa, betta, b, eta, theta, h, gama, delta, G, phi, rho, xi, BK, A0, PD, Rk0, R0, omega, xr;
parameters C0, V0, Y0, L0, K0;
parameters kappad, nu, psi, Mat;
parameters phi_pi, phi_Y, phi_DY, phi_DV, kappa, sigma, rho_i, rho_ei;
parameters g1, g2, rho_mu, a0, sigmav;
parameters sigmaA1, sigmaA2, sigmai, sigmaN;

%% -- free parameters

%technology
alphaa=0.33; %capital share
G=1.018^0.25; %deterministic trend growth
rho=0.95; %persistence of technology A2
psi=0; %investment adjustment costs
nu=2.0292644; %COMPUTED BY GMM

%preferences
R0=1.02^0.25; %SS real rate
phi=1.0151078; %COMPUTED BY GMM
delta=0.025; %depreciation rate
theta=1; %IES
L0=log(0.7); %steady state log hours
b=0.9; %consumption habit
h=0.99; %JR-habit

@#if prefs==4
    theta=0;
@#endif

%financials
BK=0.5; %fed funds data
Rk0=alphaa/0.2*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
PD=139; %G/(R0-(1-0.05)*(Rk0-R0*BK)/(1-BK)); %PD ratio in the economy
xr=1; %probability of restructuring

%learning
g1=0.99979133; %COMPUTED BY GMM
g2=0.0050476078; %COMPUTED BY GMM
rho_mu=1; %mean reversion in beliefs 
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.01;

%shocks
sigmaA1=0.00; %stddev of technology shock eps1
sigmaA2=0.010696046; %COMPUTED BY GMM
sigmai=0.0001; %COMPUTED BY GMM
sigmaN=0.00; %stddev of net worth shock epsN

%nominal rigidities
phi_pi=1.5; 
phi_Y=0; 
phi_DY=0;
phi_DV=0;
rho_i=0.5; 
rho_ei=0;
kappa=0.75;
sigma=4;

%bond market (structures 3 or 4)
Mat=0.25/4; %maturity probability (must be larger than gamma*(1-omega))

%% -- computed parameters

betta=G^theta/R0;

gama=1-(R0-G/PD)*(1-BK)/(Rk0-R0*BK);
omega=G*(1-BK)/(Rk0-R0*BK)/gama-(1-gama)/gama;
xi=BK/(xr*BK+(1-xr)/R0+xr*PD*gama*(Rk0-R0*BK)/G);
V0=log(PD*gama*(Rk0-R0*BK)/G);

kappad=Rk0-1+delta;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;
@#if redistN
    C0=log(exp(Y0)-(G-1+delta)/G);
@#else
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);
@#endif

@#if leisure
    eta=(1-alphaa)*exp(Y0-log(1-b)-C0-L0+phi*log(1-exp(L0)));
@#else
    eta=(1-alphaa)*exp(Y0-log(1-b)-C0-(phi+1)*L0);
@#endif

@#if prefs==2
    deltaJR0=log(1-h/R0)-log(1-h/R0+(1-h)/(1+phi)*(1-alphaa)*exp(Y0-log(1-b)-C0));
@#endif

@#if monpol==2
    load rn_coefs;
    rnK=rncoefmat(1);
    rnB=rncoefmat(2);
    rnR=rncoefmat(3);
    rnA=rncoefmat(4);
@#endif



