%  ----------- my_bgg2 central parameter file -------------
%



%% ------------ switches ----------------

%type of preferences (1: separable, 2: Gali, 3: GHH risk-averse
@#define prefs=1
prefs=@{prefs};

%set to 1 for variable capacity utilisation
@#define varutil=0
varutil=@{varutil};

%type of adjustment costs (0: in capital, 1: in investment)
@#define adjcost=1
adjcost=@{adjcost};

%nominal wage rigidity?
@#define rigidwages=1
rigidwages=@{rigidwages};

%indexed nominal rigidity (CEE type)?
@#define indexcalvo=0
indexcalvo=@{indexcalvo};

%type of monetary policy (1: Taylor, 2: Taylor with natural rate, 3: strict
%inflation targeting (=flexible price allocation)
@#define monpol=1
monpol=@{monpol};

%bond market structure (1: real one-period, 2: nominal one-period, 
%3: real stochastic, 4: nominal stochastic)
@#define bondmarket=1
bondmarket=@{bondmarket};

%household rationality?
parameters veryrational;
@#define veryrational=0
veryrational=@{veryrational};

%lagged belief updating?
parameters laggedupdating;
@#define laggedupdating=1
laggedupdating=@{laggedupdating};

%cum- or ex-dividend stock market value?
parameters cumdividend;
@#define cumdividend=0
cumdividend=@{cumdividend};

%redistribute dividends to households?
@#define redistN=1
redistN=@{redistN};

%redistribute markup to firms?
parameters markuptofirms;
markuptofirms=0;

%preferences over labour (0) or leisure (1)?
@#define leisure=0
leisure=@{leisure};


%% -------------- variable and parameter definitions --------------

var A2, Y, C, I, L, N, V, B, K, R, Rk, w, D, X;
var Dy, E, VD, RV;
var q, pi, i;
var mu;
var u, Q, Qd;
var RB, PB;
var C_all;
var v_i, Delta, Gamma1, Gamma2, DeltaW;
var C_level, L_level, pi_level;
var Utility, Utilitye, Welfare, Welfaree;

parameters alphaa, betta, b, eta, theta, h, gama, delta, G, phi, xi, rhoX, BK, A0, PD, Rk0, R0, omega, xr, c;
parameters weighte;
parameters C0, V0, Y0, L0, K0;
parameters kappad, nu, psi, Mat;
parameters phi_pi, phi_Y, phi_DY, phi_DV, kappa, sigma, rho_i;
parameters g, rho_mu, a0, sigmav;

@#if prefs==2
    %define additional variables for Jaimovich-Rebelo
    var X_L, X_L_level;
@#endif
%define additional variables for stochastic maturity
@#if bondmarket==3
    var mubond, RL, RLn;
@#endif
@#if bondmarket==4
    var mubond, iL, iLn;
@#endif

@#if monpol==2
    parameters phi_LDY phi_LDV phi_BK;
@#endif

@#if rigidwages
    var piW, piW_level, GammaW1, GammaW2; 
    parameters kappaW, sigmaW;
@#endif

@#if indexcalvo
    var PCP;
    parameters iota;
@#endif

varexo eps1, eps2, epsi1, epsi2, epsw, epsxi, epsb, epsp, epsD, epsV;
var sigeps1, v_w, v_xi, v_b, v_p, v_D;
parameters sigmaA1, sigmaA2, sigmai1, sigmai2, sigmaw, sigmaxi, sigmab, sigmap, sigmaD;
parameters rho, rho_ei, rhow, rhoxi, rhob, rhop, rhoD;
parameters Vscale;

%pertaining to observer equations
var Yobs, Iobs, Cobs, Lobs, iobs, piobs, Vobs;

%% ------------ free parameters ---------------

%technology
alphaa=0.33; %capital share
G=1.0164^0.25; %deterministic trend growth
rho=0.95; %persistence of technology A2
psi=10.282155; %COMPUTED BY SMM
nu=3; %elasticity of utilisation

%preferences
R0=1.02^0.25; %SS real rate
phi=0.33; %inverse Frisch elasticity
delta=0.025; %depreciation rate
theta=1; %IES
L0=log(0.7); %steady state log hours
b=0; %consumption habit
h=0.99; %Gali-habit

%entrepreneur welfare weight
weighte=0;

@#if prefs==4
    theta=0;
@#endif

%financials
BK=0.5; %fed funds data
Rk0=alphaa/0.18*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
PD=154; %PD ratio in the economy
xr=0.1; %probability of restructuring
c=0.81629174; %COMPUTED BY SMM
rhoX=0; %AR component on stock price in borrowing constraint

%nominal rigidities
phi_pi=1.5; 
phi_Y=0; 
phi_DY=0;
phi_DV=0;
rho_i=0.5;
kappa=0.75;
sigma=4;
kappaW=0.94354091; %COMPUTED BY SMM
sigmaW=4;
iota=0.99;

%for gap estimation
rhogap=0.5;

%bond market (structures 3 or 4)
Mat=0.9; %maturity probability (must be larger than gamma*(1-omega))

%learning 
g=0.0045967481; %COMPUTED BY SMM
rho_mu=0.999; %mean reversion in beliefs 
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.001;

%shocks
sigmaA1=0; %stddev of technology shock eps1
sigmaA2=0.0062795277; %COMPUTED BY SMM
sigmai1=1e-6;
sigmai2=0.001; %COMPUTED BY SMM
sigmap=0; 
sigmaw=0;
sigmaxi=0;
sigmab=0; 
sigmaD=0; 

%shock autocorrelations
rho=0.95; %of technology A2
rho_ei=.99; %of interest rate
rhop=.5; %of price markup shock
rhow=0.8; %of wage markup shock
rhoxi=0.8; %of financial shock
rhob=.95; %of impatience shock
rhoD=.95; %of dividend shock

Vscale=1;

%% ----------- computed parameters ------------

betta=G^theta/R0;

gama=1-G*(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0/G-1/PD);
omega=(1-gama)/gama*(1/(R0/G-1/PD)-1);
V0=log(gama*(Rk0-R0*BK)+(1-gama)*c*(Rk0-1-BK*(R0-1)))-log(G)+log(PD)+cumdividend*log(R0/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0/G)))+(1-xr)/R0);

kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;
@#if redistN
    C0=log(exp(Y0)-(G-1+delta)/G);
@#else
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);
@#endif

@#if leisure
    eta=(1-alphaa)*exp(Y0-log(1-b)-C0-L0+phi*log(1-exp(L0)));
@#else
    eta=(1-alphaa)*exp(Y0-log(1-b)-C0-(phi+1)*L0);
@#endif

@#if monpol==2
    load rn_coefs;
    rnK=rncoefmat(1);
    rnB=rncoefmat(2);
    rnR=rncoefmat(3);
    rnA=rncoefmat(4);
@#endif




