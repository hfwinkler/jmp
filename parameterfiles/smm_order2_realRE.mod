%% ------------ switches ----------------

%type of preferences (1: separable, 2: Gali, 3: GHH risk-averse
@#define prefs=1
prefs=@{prefs};

%set to 1 for variable capacity utilisation
@#define varutil=0
varutil=@{varutil};

%type of adjustment costs (0: in capital, 1: in investment)
@#define adjcost=1
adjcost=@{adjcost};

%nominal wage rigidity?
@#define rigidwages=0
rigidwages=@{rigidwages};

%indexed nominal rigidity (CEE type)?
@#define indexcalvo=0
indexcalvo=@{indexcalvo};

%type of monetary policy (1: Taylor, 2: Taylor with natural rate, 3: strict
%inflation targeting (=flexible price allocation)
@#define monpol=3
monpol=@{monpol};

%lagged belief updating?
parameters laggedupdating;
@#define laggedupdating=0
laggedupdating=@{laggedupdating};

%cum- or ex-dividend stock market value?
parameters cumdividend;
@#define cumdividend=0
cumdividend=@{cumdividend};

%existence of separate entrepreneurs?
parameters entrepreneurs;
@#define entrepreneurs=1
entrepreneurs=@{entrepreneurs};

%redistribute markup to firms?
parameters markuptofirms;
markuptofirms=0;


%% -------------- variable and parameter definitions --------------

var A2, Y, C, Ce, I, L, N, V, B, K, R, Rk, w, D, X;
var E, ED, RV;
var q, pi, i, v_i;
var mu;
var u, Q, Qd;
var Delta, Gamma1, Gamma2, DeltaW;
var MU, Utility, Welfare, Welfaree;

parameters alphaa, betta, b, eta, theta, h;
parameters gama, delta, G, phi, xi, rhoX, BK, A0, PD, Rk0, R0, omega, xr, c;
parameters eqprem;
parameters weighte;
parameters C0, V0, Y0, L0, K0;
parameters kappad, nu, psi;
parameters phi_pi, phi_Y, phi_DY, phi_DV, kappa, sigma, rho_i;
parameters g, rho_mu, a0, sigmav;

@#if prefs==2
%define additional variables for Jaimovich-Rebelo
var X_L;
@#endif

@#if monpol==2
parameters phi_LDY phi_LDV phi_BK;
@#endif

@#if rigidwages
var piW, GammaW1, GammaW2; 
parameters kappaW, sigmaW;
@#endif

@#if indexcalvo
var PCP;
parameters iota;
@#endif

varexo eps1, eps2;
var sigeps1;
@#if monpol!=3
varexo epsi1, epsi2;
@#endif

parameters sigmaA1, sigmaA2, sigmai1, sigmai2;
parameters rho, rhoei;


%% ------------ free parameters ---------------

%technology
alphaa=0.33; %capital share
G=1^0.25; %deterministic trend growth
psi=0; %COMPUTED BY SMM
nu=3; %elasticity of utilisation

%preferences
R0=1.02^0.25; %SS real rate
eqprem=1; %exp(.01016375); %equity premium
phi=0.001; %COMPUTED BY SMM
delta=0.025; %depreciation rate
theta=1; %IES
L0=log(0.7); %steady state log hours
b=0; %consumption habit
h=0.99; %Gali-habit

%entrepreneur welfare weight
weighte=0;

%financials
BK=0.4; %fed funds data
Rk0=alphaa/0.18*(G-1+delta)+1-delta; %calibrated to get investment share I/Y; needs to be more than R!
%PD=154; %PD ratio in the economy
omega=0; %size of entering firms
xr=0.093; %probability of restructuring
c=0; %COMPUTED BY SMM
rhoX=0; %AR component on stock price in borrowing constraint

%nominal rigidities
phi_pi=1.5; 
phi_Y=0; 
phi_DY=0;
phi_DV=0;
rho_i=0.85;
kappa=0;
sigma=4;
kappaW=0;
sigmaW=4;
iota=0.99;

%learning 
g=0.00521383544579; %COMPUTED BY SMM
rho_mu=.999; %mean reversion in beliefs 
a0=0; %0.5*(1-rho_mu)^2/4; %mean reversion in beliefs
sigmav=0.001;

%shocks
sigmaA1=0;
sigmaA2=0.0134401641155; %COMPUTED BY SMM
sigmai1=0;
sigmai2=0;

%shock autocorrelations
rho=0.95; %of technology A2
rhoei=0.99; %of inflation target


%% ----------- computed parameters ------------

betta=G^theta/R0;

%gama=1-(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0*eqprem-G/PD);
%omega=(1-gama)/gama*(G/(R0*eqprem-G/PD)-1);
gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));

V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);

kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;
@#if entrepreneurs
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);
@#else
    C0=log(exp(Y0)-(G-1+delta)/G);
@#endif

eta=(1-alphaa)*exp(Y0-(phi+1)*L0);

@#if monpol==2
    load rn_coefs;
    rnK=rncoefmat(1);
    rnB=rncoefmat(2);
    rnR=rncoefmat(3);
    rnA=rncoefmat(4);
@#endif









