%% common model equations for all bgg2 variants

%productivity
A2=rho*A2(-1)+sigmaA2*eps2;
sigeps1=sigmaA1*eps1;


%adjustment costs
@#if adjcost
    %in investment
    exp(Q) = 1+psi*(exp(I-I(-1)+sigeps1)-1);
    exp(Qd)= exp(Q)*(1-delta);
@#else
    %in capital
    exp(Q) = 1+psi*(exp(K-K(-1)+sigeps1)-1);
    exp(Qd)= exp(Q)-delta+psi/2*(exp(K-K(-1)+sigeps1)-1)^2;
@#endif


%household side - WARNING ONLY theta=1 AND b=0 IS IMPLEMENTED FOR WELFARE!

# UL = -exp(L+DeltaW)^(1+phi)/(1+phi);
# dUL = exp(L+DeltaW)^phi;
#lambda=betta*MU(+1)/MU*(G*exp(sigeps1(+1)))^-theta;

@#if prefs==1  
    # MRS    = eta*dUL/MU;
    MU = (exp(C-C0)/(1-b)-b/(1-b)*exp(C(-1)-C0-sigeps1))^-theta;
    Utility=C-C0+eta*UL;
@#endif
@#if prefs==2
    X_L=(1-h)*C+h*(X_L(-1)-sigeps1);
    # MRS    = eta*dUL*exp(X_L-C0);
    MU = (exp(C-C0)-b*exp(C(-1)-C0-sigeps1))^-theta;
    Utility=C-C0+eta*exp(X_L-C)*UL;
@#endif
@#if prefs==3  
    # MRS    = eta*dUL;
    MU = (exp(C-C0)-b*exp(C(-1)-C0-sigeps1)+eta*UL)^-theta;
    Utility=log(exp(C-C0)+eta*UL);
@#endif

%Welfare
Welfare=(1-betta)*Utility+betta*Welfare(+1);
Welfaree=(1-1/R0)*Ce+1/R0*Welfaree(+1);

%short-term debt pricing
1=lambda*exp(R);
1=lambda*exp(i-pi(+1));

%nominal rigidity
@#if indexcalvo
    exp(Gamma1)=exp(q)+lambda*exp(Y(+1)+log(G)-Y)*kappa*exp(sigma*pi(+1)+Gamma1(+1));
    exp(Gamma2)=1     +lambda*exp(Y(+1)+log(G)-Y)*kappa*exp((sigma-1)*pi(+1)+Gamma2(+1));
    exp(PCP)^(1-sigma)=(1-iota)*exp(Gamma1-Gamma2)^(1-sigma)+iota*exp(pi(-1)-pi+PCP(-1))^(1-sigma);
    1=kappa*exp(-pi)^(1-sigma)+(1-kappa)*exp(PCP)^(1-sigma);
    exp(Delta)=kappa*exp(pi)^sigma*exp(Delta(-1))+(1-kappa)*( (1-iota)*exp(Gamma1-Gamma2)^-sigma + iota*exp(pi(-1)-pi+PCP(-1))^-sigma );
@#else
    @#if monpol==3
        Gamma1=-log(1-G/R0*kappa);
        Gamma2=Gamma1;
        pi=0;
        Delta=0;
    @#else
        exp(Gamma1)=exp(q)+lambda*exp(Y(+1)+log(G)-Y)*kappa*exp(sigma*pi(+1)+Gamma1(+1));
        exp(Gamma2)=1     +lambda*exp(Y(+1)+log(G)-Y)*kappa*exp((sigma-1)*pi(+1)+Gamma2(+1));
        exp(Gamma1-Gamma2)=((1-kappa*exp(pi)^(sigma-1))/(1-kappa))^(1/(1-sigma));
        exp(Delta)=kappa*exp(pi)^sigma*exp(Delta(-1))+(1-kappa)*exp(Gamma1-Gamma2)^-sigma;
    @#endif
@#endif

@#if rigidwages
    w=w(-1)-sigeps1+piW-pi;
    exp(GammaW1)=MRS/exp(w)+lambda*exp(L(+1)-L)*kappaW*exp(sigmaW*piW(+1)+GammaW1(+1));
    exp(GammaW2)=1     +lambda*exp(L(+1)-L)*kappaW*exp((sigmaW-1)*piW(+1)+GammaW2(+1));
    exp(GammaW1-GammaW2)=((1-kappaW*exp(piW)^(sigmaW-1))/(1-kappaW))^(1/(1-sigmaW));
    exp(DeltaW)=(1-kappaW)*exp(GammaW1-GammaW2)^-sigmaW+kappaW*exp(piW)^sigma*exp(DeltaW(-1));
@#else
    DeltaW=0;
    exp(w) = MRS;
@#endif

%monetary policy
@#if monpol==1 
    %option 1: Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(log(R0)+pi+(phi_pi-1)*(pi-v_i)+phi_Y*(Y-Y0)+phi_DY*(Y+sigeps1-Y(-1))+phi_DV*(V-V(-1)+sigeps1))-sigmai1*epsi1;
    v_i=rhoei*v_i(-1)+sigmai2*epsi2;
@#endif
@#if monpol==2 
    %option 2: extended Taylor rule
    i=rho_i*i(-1)+(1-rho_i)*(log(R0)+pi+(phi_pi-1)*(pi-v_i)+phi_Y*(Y-Y0)+phi_DY*(Y-Y(-1)+sigeps1)+phi_DV*(V-V(-1)+sigeps1)+phi_LDY*(Y(-1)-Y(-2)+sigeps1(-1))+phi_LDV*(V(-1)-V(-2)+sigeps1(-1))+phi_BK*(B-K-log(BK)))-sigmai1*epsi1;
    v_i=rhoei*v_i(-1)+sigmai2*epsi2;
@#endif
@#if monpol==3 
    %option 3: inflation targeting
    q=0;
    v_i=0;
@#endif

%market clearing and auxiliaries
Y=alphaa*(u+K(-1)-log(G)-sigeps1)+(1-alphaa)*(A0+A2+L);
exp(I)=exp(K)-(1-delta)*exp(K(-1)-log(G)-sigeps1)*(1-kappad/nu*(exp(nu*u)-1));
@#if adjcost
    exp(Y-Delta)=exp(C)+Ce+exp(I)+1*psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#else
    exp(Y-Delta)=exp(C)+Ce+exp(I)+1*psi/2*(exp(K-K(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#endif
C_all=log(exp(C)+Ce);

%financials
exp(E)=exp((1-markuptofirms)*q+Y)-exp(w+L)+(exp(Qd)-exp(Q))*exp(K(-1)-sigeps1)/G-(exp(R)-1)*exp(B(-1)-sigeps1)/G;
X=rhoX*(X(-1)-sigeps1)+(1-rhoX)*V;
ED=D(+1);


