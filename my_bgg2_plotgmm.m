% evoke my_bgg2_plot for GMM calculated values

movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod; 
copyfile my_bgg2_parameters_gmm.mod my_bgg2_parameters.mod;
try
    my_bgg2_plot;
end
delete my_bgg2_parameters.mod;
movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;
