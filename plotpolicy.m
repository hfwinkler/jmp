% plot graphs visualising the impact of monetary policy

global M_ oo_ options_

%% parameters and ranges to search over
paramnames={'phi_DY','phi_DV'};
instrunames={'\phi_{\Delta Y}', '\phi_{\Delta V}'};
J=length(paramnames);

lbounds=[0;   0];
ubounds=[1;   1];

K=[6 6]; %number of gridpoints per parameters

mnames={'learn'};

resultnames={'\sigma_{\pi}', '\sigma_C' '\sigma_V'};

T=1e3; %simulation length (if needed)
maxiter=1e3;
order=2;

%% parse parameter file to use in functions

dynare my_bgg2_parameters onlymacro savemacro noclearall;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% make inflation targeting parameterfile

fin = fopen('my_bgg2_parameters.mod');
fout = fopen('my_bgg2_parameters_pit.mod', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && strncmp(tline,'@#define monpol',15);
        fprintf(fout, '%s\n', '@#define monpol=3');
    else
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);

%% make grid

%make grid
X=zeros(J,max(K));
for i=1:J
    X(i,1:K(i))=lbounds(i)+(0:K(i)-1)/(K(i)-1)*(ubounds(i)-lbounds(i));
end
    
%% now loop over models

for mi=1:length(mnames)
    model=mnames{mi};
    
% run Dynare once
    clear functions;
    clear M_ oo_ options_;
    dynare(['my_bgg2_' model], 'noclearall nostrict');
    
    options_.hp_filter=0;
    options_.order=order;
    options_.periods=T;
    options_.PLM=0;
    
    param0=zeros(J,1);
    for i=1:J
        eval(['param0(' num2str(i) ')=' paramnames{i} ';']);
    end

% evaluate on grid

    results.(model)=nan([K length(resultnames)]);

    %loop
    h=waitbar(0,['policy evaluation for ' model ', ' num2str(prod(K)) ' points']);
    k=cell(1,J);
    for node=1:prod(K)
        waitbar(node/prod(K));
        [k{:}]=ind2sub(K,node);
        idx=sub2ind([J max(K)],1:J,[k{:}]);
        [Loss, m]=my_bgg2_evalpol3(paramnames,X(idx),1,maxiter,M_,oo_,options_);
        if ~isempty(m);
            results.(model)(k{:}, :)=[m.sigmapi, m.sigmaI, Loss];
        end
    end
    delete(h); drawnow;
   
% also compute inflation targeting value
    results_pit.(model)=nan(length(resultnames),1);
    movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod;
    movefile my_bgg2_parameters_pit.mod my_bgg2_parameters.mod;
    clear M_ oo_ m;
    clear functions;
    try
        dynare(['my_bgg2_' model], noclearall);
        [Loss, m]=my_bgg2_evalpol3(paramnames,X(idx),1,maxiter,M_,oo_,options_);
        results_pit.(model)=[m.sigmapi, m.sigmaY, Loss]';
    end
    movefile my_bgg2_parameters.mod my_bgg2_parameters_pit.mod; 
    movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;
   
end

%% save

save plotpolicy_data;

%% plot


for mi=1:length(mnames)
    model=mnames{mi};
    figure; set(gcf,'name',['Policy metrics for ' model ' (inflation targeting in bold)']);
    i=0;
    for ri=1:length(resultnames)
        i=i+1;
        Zdata=squeeze(results.(model)(:,:,ri));
        subtightplot(1,3,i,0.05,0.12,0.04);
        [~,h]=contour(X(1,:),X(2,:),Zdata');
        set(h,'ShowText','on','LabelSpacing',10^6,'LevelStep',get(h,'LevelStep')/4,'TextStep',get(h,'LevelStep'));
        xlabel(instrunames{1}); ylabel(instrunames{2}); 
        title(resultnames{ri});
        hold on;
        if isfinite(results_pit.(model)(ri))
            contour(X(1,:),X(2,:),Zdata',results_pit.(model)(ri)*[1 1],'k:','LineWidth',2);
        end
        if strcmp(resultnames{ri},'loss')
            [istar,jstar]=ind2sub(size(Zdata), find(Zdata==min(min(Zdata))));
            plot(X(1,istar),X(2,jstar),'ko','MarkerSize',5,'MarkerFaceColor','k');
        end
    end
end
    
