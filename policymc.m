% my_bgg2 policy evaluator
% Monte Carlo version

global M_ oo_ options_

%Dynare file to use
mname='my_bgg2_learn';

%parameters and ranges to search over
paramnames={'phi_pi' 'phi_DY' 'phi_DV'};
J=length(paramnames);

lbounds=[1; 0; 0];
ubounds=[5; 1; 1];

%number of draws
K=500;

%% read parameter file

dynare my_bgg2_parameters onlymacro savemacro;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% run Dynare once
dynare(mname,'noclearall');
options_.noprint=1;
options_.order=1;
options_.irf=0;
options_.ar=0;
options_.hp_filter=0;

%% Monte Carlo

%determine result size and names

[stat0,statnames]=my_bgg2_evalpol2(cell(0,0),[],0);
N=length(stat0);

if N<=1; error 'initial point not feasible'; end
if J~=length(lbounds) || J~=length(lbounds); error 'bounds incorrect'; end

stats=zeros(N,K);
X=zeros(J,K);

hw=waitbar(0,['grid search, ' num2str(K) ' points']);
for k=1:K
    waitbar(k/K);
    X(:,k)=lbounds+(ubounds-lbounds).*rand(J,1);
    stats(:,k)=my_bgg2_evalpol2(paramnames,X(:,k),0);
end
delete(hw);

%% produce output

%stats
for n=1:N
    figure; set(gcf,'name',statnames{n});
    for j=1:J
        subplot(2,ceil(J/2),j);
        scatter(X(j,:),stats(n,:),'x');
        axlims=axis;
        axlims(1)=lbounds(j);
        axlims(2)=ubounds(j);
        axlims(3)=prctile(stats(n,:),1);
        axlims(4)=prctile(stats(n,:),99);
        axis(axlims); 
        title(paramnames{j});
    end
end

figure; set(gcf,'name','inexistence of stable, unique solution');
for j=1:J
    subplot(2,ceil(J/2),j);
    K=20;
    parambins=linspace(lbounds(j),ubounds(j),K+1);
    nanprob=zeros(1,K);
    for k=1:K
        idx=find((X(j,:)>=parambins(k)) .* (X(j,:)<parambins(k+1)));
        nanprob(k)=sum(isnan(stats(1,idx)))/length(idx);
    end
    bar(parambins(1:K),nanprob);
    axlims=axis;
    axlims(1)=lbounds(j);
    axlims(2)=ubounds(j);
    axis(axlims); 
    title(paramnames{j});
end
    
cleanup_dyn(mname);