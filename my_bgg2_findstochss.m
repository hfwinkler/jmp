function ystochSS = my_bgg2_findstochss( paramnames, paramvector, noepsi, M_, oo, options, T) %#ok<INUSL>
%MY_BGG2_FINDSTOCHSS finds stochastic steady state with changed parameters

warning off all;
sigma=0;

%initialise values to return if anything goes wrong
Loss=Inf;
m=[];
    
%% update parameters

%load parameters
my_bgg2_parameters;
for i=1:M_.param_nbr
    eval([M_.param_names(i,:) '=M_.params(i);']);
end

for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
end

if noepsi
    sigmai1=0; %#ok<NASGU>
    sigmai2=0; %#ok<NASGU>
end

endoparamupdate;
for i=1:M_.param_nbr
    eval(['M_.params(i)=' M_.param_names(i,:) ';']);
end


%% run Dynare (if there are any parameters to update)  

options.pruning=1;
options.irf=0;
options.nomoments=0;
options.ar=0;
options.noprint=1;
options.hp_filter=0;
options.qz_criterium=1+1e-8;

dynerr=0;
try
    oo=rmfield(oo,'dr.ghx'); 
    oo=rmfield(oo,'drPLM');
end
try
    [oo.dr, info]=resol(0,M_,options,oo);
    %if info(1); print_info(info,0,options); end
 catch err
     dynerr=1;
     %disp(err.message);
 end

%if Dynare fails, set the objective very high
if dynerr || isempty(oo.dr.ghx) || any(isnan(oo.dr.ghx(:))) || (options.order==3 && any(isnan(oo.dr.ghxxx(:))))
    %disp('Dynare error');
    return;
end

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr; %number of exogenous shocks


%% simulate

x=simult1(oo.dr.ys,oo.dr,zeros(T-1,nexo),options.order,M_,options);

ystochSS=x(:,T);

end
