% evoke my_bgg2_plot for SMM calculated values

movefile my_bgg2_parameters.mod my_bgg2_parameters_old.mod; 
copyfile my_bgg2_parameters_smm.mod my_bgg2_parameters.mod;
try
    my_bgg2_plot;
catch err
    disp(err.message);
end
delete my_bgg2_parameters.mod;
movefile my_bgg2_parameters_old.mod my_bgg2_parameters.mod;
