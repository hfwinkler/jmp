% my_bgg2 shock evaluator

global M_ oo_ options_

%Dynare file to use
mlist={'nofric' 'RE' 'learn'};

%shock stddevs search over
sigmanames={'sigmaA1' 'sigmaA2' 'sigmai1' 'sigmai2' 'sigmap' 'sigmaw' 'sigmaxi' 'sigmab' 'sigmaD'};
J=length(sigmanames);

%stddev of any single shock
sigmasize=1e-2; 


%% read parameter file

dynare my_bgg2_parameters onlymacro savemacro;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% loop over models
for mi=1:length(mlist)
    model=mlist{mi};

    dynare(['my_bgg2_' model],'noclearall');
    options_.noprint=1;
    options_.order=1;
    options_.irf=0;
    options_.ar=0;
    options_.hp_filter=0;


    %determine result size and names

    [stat0,statnames]=my_bgg2_evalshock(cell(0,0),[]);
    N=length(stat0);

    if N<=1; error 'initial point not feasible'; end

    stats.(model)=zeros(J,N);

    for k=1:J
        X=zeros(J,1);
        X(k)=sigmasize;
        stats.(model)(k,:)=my_bgg2_evalshock(sigmanames,X);
        stats.(model)(k,abs(stats.(model)(k,:))>10)=10;
    end

end
%% collect stats

stats.all=zeros(length(mlist),J,N);
for mi=1:length(mlist)
    stats.all(mi,:,:)=stats.(mlist{mi});
end
    
%% produce output

figure;
for n=1:N
    subplot(2,ceil(N/2),n);
    bar(stats.all(:,:,n)');
    set(gca,'XTickLabel',sigmanames);
    rotateXLabels( gca, 45 )
    title(statnames{n});
    if n==1; legend(mlist,'Location','Best'); end
end

for mi=1:length(mlist)
    cleanup_dyn(mlist{mi});
end