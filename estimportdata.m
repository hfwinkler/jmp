% import data for estimation from projectdata.xls

[numdata, txtdata] = xlsread('data/projectdata.xls');

obsdata=struct;
for i = 1:length(txtdata)
      obsdata.(txtdata{i})=numdata(:,i);
end

T=length(obsdata.date);
date=obsdata.datenum(2:T);

Yobs=obsdata.Ymodel(2:T)-obsdata.Ymodel(1:T-1);
Yobs=Yobs-mean(Yobs);
Cobs=obsdata.Cmodel(2:T)-obsdata.Cmodel(1:T-1);
Cobs=Cobs-mean(Cobs);
Iobs=obsdata.Imodel(2:T)-obsdata.Imodel(1:T-1);
Iobs=Iobs-mean(Iobs);
Lobs=obsdata.hours(2:T);
Lobs=Lobs-mean(Lobs);
iobs=obsdata.FFR(2:T);
iobs=iobs-mean(iobs);
piobs=obsdata.piCC(2:T);
piobs=piobs-mean(piobs);
Vobs=obsdata.Ppop(2:T)-obsdata.Ppop(1:T-1);
Vobs=Vobs-mean(Vobs);
