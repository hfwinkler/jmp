%A variant of BGG with limited commitment
%RE frictionless solution


%load parameters from file:
@#include "my_bgg2_parameters.mod"

var lag_pred_err aux_pred_err;
varexo pred_err pred_err1;

%redefine parameters
g=0.00465;
BK=0.0001;
Rk0=R0;


V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
kappad=Rk0/(1-delta)-1;
Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
eta=(1-alphaa)*exp(Y0-(phi+1)*L0);

I0=log((G-1+delta)/G)+K0;
C0=log(exp(Y0)-exp(I0)-exp(V0)/PD);


model;

@#include "my_bgg2_common.mod"

%entrepreneurs hard-coded in this mod file
#lambdae=1/R0/eqprem;
Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;

%firm side
exp(Rk)=alphaa*exp(q+Y-K(-1)+log(G)+sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(w)=(1-alphaa)*exp(q+Y-L);

%instead of borrowing constraint:
exp(Q)=R0/Rk0*lambda*exp(Rk(+1)); % constant return differential
B=Q+K+log(BK); %constant leverage

%the following only defines the profit claims to the "investors", who don't
%control the firm now. Residual cash flow after debt flow and investor
%dividends goes to households.

exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));
%exp(D)=gama*exp(N)+(1-gama)*c*exp(E)-(exp(q)-1)*(gama+0*(1-gama)*c)*exp(Y);
exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%asset pricing with learning
@#if laggedupdating
    V=V(-1)+mu-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*(V(-1)-V0)+g*(sigmav*pred_err1-0.5*(sigmav*aux_pred_err(+1))^2);
@#else
    V=V(-1)+mu(-1)-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*(V(-1)-V0)+g*(sigmav*pred_err -0.5*(sigmav*aux_pred_err(+1))^2);
@#endif    
aux_pred_err=pred_err;
lag_pred_err=aux_pred_err(-1);

@#if cumdividend
    exp(RV)=G*exp(V-sigeps1)/(exp(V(-1))-exp(D(-1)))*(1-gama)/(1-gama+gama*omega)/R0;
@#else
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;
@#endif

end;

steady_state_model;

Rk=log(Rk0);

@#include "my_bgg2_commonSS.mod"

Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);
Welfaree=Ce;
C_all=log(exp(C)+Ce);

end;

@#include "my_bgg2_commonend.mod"
