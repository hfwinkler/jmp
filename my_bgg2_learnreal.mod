%A variant of BGG with limited commitment
%learning solution, no nominal rigidities

%load parameters from file:
@#include "my_bgg2_parameters.mod"

%redefine parameters
%kappa=0;
%kappaW=0;
psi=0;

var lag_pred_err aux_pred_err;
varexo pred_err pred_err1;

model;

@#include "my_bgg2_common.mod"

%entrepreneurs
@#if entrepreneurs
    #lambdae=1/R0/eqprem;
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#else
    #lambdae=lambda;
    %lambdae=1/R0;
    Ce=0;
@#endif

%firm side
exp(Rk)=(exp((1-markuptofirms)*q+Y)-exp(w+L))/exp(K(-1)-log(G)-sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(K+Q)=(1-gama+gama*omega)*(exp(N)-c*exp(E))+exp(B);
exp(w)=(1-alphaa)*exp(q+Y-L);
@#if cumdividend
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(lambdae*G*exp(V(+1)+sigeps1(+1))+exp(B)));
@#else    
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(exp(V)+exp(B)));
@#endif
exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));
exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%asset pricing with learning
@#if laggedupdating
    V=V(-1)+mu-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*V(-1)+g*(sigmav*pred_err1-0.5*(sigmav*aux_pred_err(+1))^2);
@#else
    V=V(-1)+mu(-1)-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*V(-1)+g*(sigmav*pred_err -0.5*(sigmav*aux_pred_err(+1))^2);
@#endif    
aux_pred_err=pred_err;
lag_pred_err=aux_pred_err(-1);

@#if cumdividend
    exp(RV)=G*exp(V)/(exp(V(-1))-exp(D(-1)))*(1-gama)/(1-gama+gama*omega)/R0;
@#else
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;
@#endif

end;

steady_state_model;

Rk=log(Rk0);

@#include "my_bgg2_commonSS.mod"

@#if entrepreneurs
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);
@#else
    Ce=0;
@#endif
Welfaree=Ce;

end;


@#include "my_bgg2_commonend.mod"
