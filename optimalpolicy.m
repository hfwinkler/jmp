%% find optimal monetary policy

%parameters and ranges to search over
paramnames={'phi_DY' 'phi_DV'};
J=length(paramnames);
K=0; %how many random starting points to consider apart from initial point

lbounds=[1.5 0]';
ubounds=[10 10]';

mname='my_bgg2_learn';

T=5e2; %simulation length 
maxiter=3e3; %max iterations / number of simulations

%% parse parameter file

evalc('dynare my_bgg2_parameters onlymacro savemacro');
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', tline);
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% run Dynare once
clear functions;
dynare(mname,'noclearall');

options_.hp_filter=0;
options_.periods=T;
options_.order=2;
options_.PLM=0;

optionsPLM=options_;
optionsPLM.PLM=1;

%% find optimal policy

[loss0, m0]=my_bgg2_evalpol3([],[],0,maxiter,M_,oo_,options_); %evaluate this WITH shocks!
%loss0PLM=my_bgg2_evalpol3([],[],0,maxiter,M_,oo_,optionsPLM);

param0=zeros(J,1);
for i=1:J
    eval(['param0(' num2str(i) ')=' paramnames{i} ';']);
end
%or start with guess
%param0=test;

%fun=@(x) my_bgg2_evalpol3(paramnames,x,1,maxiter,M_,oo_,options_) + 1e3*max(0,my_bgg2_evalpol3(paramnames,x,1,maxiter,M_,oo_,optionsPLM)-loss0PLM);
fun=@(x) my_bgg2_evalpol3(paramnames,x,1,maxiter,M_,oo_,options_);

%try once
options=optimset('Algorithm','sqp','Display','iter','MaxFunEvals',10^4,'TolX',1e-7);
[paramstar, res]=fmincon(fun,param0,[],[],[],[],lbounds,ubounds,[],options);

%options=optimset('Display','iter','TolX',1e-9);
%[paramstar, res]=simulannealbnd(fun,param0,lbounds,ubounds,options);

%% repeat if necessary

if K
    options=optimset('Algorithm','sqp','Display','off','MaxFunEvals',1e4,'TolX',1e-8);
    bestparams=zeros(J,K+1);
    bestparams(:,K+1)=paramstar;
    bestres=ones(1,K+1)*1e9;
    bestres(K+1)=res;
    p=ProgressBar(K);
    for k=1:K
        randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        while my_bgg2_evalpol3(paramnames,randompoint,1,maxiter,M_,oo_,options_)>res+abs(res);
            randompoint=lbounds+rand(J,1).*(ubounds-lbounds);
        end
        try
            [bestparams(:,k),bestres(k)]=fmincon(fun,randompoint,[],[],[],[],lbounds,ubounds,[],options);
        end
        %disp(bestparams(:,k)');
        p.progress();
    end
    save multistart_result bestparams bestres;
    p.stop;
    sortres=sort(bestres');
    disp(sortres);
    [res, kstar]=min(bestres);
    paramstar=bestparams(:,kstar);
end
    

%% print results

lossstar=res;
[~, mstar]=my_bgg2_evalpol3(paramnames,paramstar,1,maxiter,M_,oo_,options_);


for i=1:length(paramnames)
    fprintf('%8s = %3.4g\n',paramnames{i},paramstar(i));
end

fprintf('\nField:  \tparam0  \tparam*   \tDelta\n');fis=fieldnames(m0);
for fi=1:length(fis)
    fieldname=fis{fi};
    fprintf('%7s:\t%8.4g  \t%8.4g\t%8.4g\n',fieldname,m0.(fieldname),mstar.(fieldname),mstar.(fieldname)-m0.(fieldname));
end

save optpoldata;

%% write new parameter file

fin = fopen('my_bgg2_parameters.mod');
fout = fopen('my_bgg2_parameters_optimalpolicy.mod', 'wt');

tline = 'dummy';
count = 0;
allparamnames={paramnames{:}, 'sigmai1', 'sigmai2'};
paramstarext=[paramstar; 0; 0];
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) 
        foundparam=0;
        for i=1:(J+2)
            foundparam=foundparam+i*strncmp(strtrim(tline),[allparamnames{i} '='],length(allparamnames{i})+1);
        end
        if foundparam==0
            fprintf(fout, '%s\n', tline);
        else
            fprintf(fout, '%s=%.8g; %%COMPUTED BY OPTIMALPOLICY\n',allparamnames{foundparam},paramstarext(foundparam));
        end
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);

%cleanup_dyn(M_.fname);

disp('Now write the following to save the results:');
disp(' writetocollection(pcoll,pname,{paramnames{:} ''sigmai1'' ''sigmai2''},[paramstar; 0; 0]);');
    