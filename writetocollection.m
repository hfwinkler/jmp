function [ ] = writetocollection( collectionname, policyname, paramnames, paramvalues )
%WRITETOCOLLECTION( collectionname, policyname, paramnames, paramvalues )
%writes parameters to collection

J=length(paramnames);
if length(paramvalues)~=J; error('names and values of different length.'); end

try 
    load([collectionname '.mat']); 
catch
    policies=struct;
end

policies.(policyname)=struct;
for j=1:J
    policies.(policyname).(paramnames{j})=paramvalues(j);
end

save([collectionname '.mat'],'policies');

fprintf('\nThe file ''%s.mat'' now contains the following policies:\n',collectionname);
disp(fieldnames(policies));

end

