function [ res, moments, targets, weights ] = my_bgg2_evalgmm( paramnames, paramvector, T )
%MY_BGG2_EVALGMM evaluate GMM loss function
% invoked by my_bgg2_momentmatch.m

%% preliminaries
global M_ oo_ options_

%load parameter file
my_bgg2_parameters;

%disp(paramvector);

%% update parameters
for i=1:length(paramnames)
    eval([paramnames{i} '=paramvector(' num2str(i) ');']);
    set_param_value(paramnames{i},paramvector(i));
end
endoparamupdate;
for m=1:length(endoparamnames)
    set_param_value(endoparamnames{m},eval(endoparamnames{m}));
end

%% run Dynare
options_.order=1;
options_.irf=0;
options_.nomoments=1;
options_.ar=0;
options_.noprint=1;
options_.hp_filter=0;

%if Dynare fails, set the objective very high
warning off;
oo_.dr.ghx=[];
try
    stoch_simul('');
end
warning on;

if isempty(oo_.dr.ghx)
    %disp('Dynare error');
    res=Inf;
    return;
end

nendo=M_.endo_nbr; %number of endogenous variables
nexo=M_.exo_nbr-2*(1-veryrational)*strncmp(M_.fname,'my_bgg2_learn',20); %number of exogenous shocks (not counting AU)

%% retrieve policy rules
C=zeros(length(oo_.dr.state_var),nendo);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end
A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B=oo_.dr.ghu(oo_.dr.inv_order_var,1:nexo);

if max(abs(eig(A)))>=1
    %disp('Learning explosive');
    res=Inf;
    return;
end

%% generate simulation

stream = RandStream.getGlobalStream;
reset(stream,0);
v=randn(nexo,T);


x=zeros(nendo,T);
for t=2:T
    x(:,t)=A*x(:,t-1)+B*v(:,t);
end

%trending variables
trendingvars={'N','D','Dy','B','K','V','Y','C','I','w','C_all'};
if prefs==2
    trendingvars={trendingvars, 'X_L0'};
end
    
trend=cumsum(sigmaA1*v(1,:),2);
for i=1:length(trendingvars)
    [~,jidx]=ismember(trendingvars{i},M_.endo_names,'rows');
    x(jidx,:)=x(jidx,:)+trend;
end

%do HP-filtering
x_hp=x-hpfilter(x,1600)';

%% calculate individual moments

n=0;

%volatility of output
n=n+1;
[~,jY]=ismember('Y',M_.endo_names,'rows');
moments(n)=std(x_hp(jY,:));
targets(n)=0.0143;
weights(n)=100/abs(targets(n));

%relative volatility of hours
n=n+1;
[~,jL]=ismember('L',M_.endo_names,'rows');
moments(n)=std(x_hp(jL,:))/std(x_hp(jY,:));
targets(n)=1.13;
weights(n)=1/abs(targets(n));

%relative volatility of consumption
n=n+1;
[~,jC]=ismember('C',M_.endo_names,'rows');
moments(n)=std(x_hp(jC,:))/std(x_hp(jY,:));
targets(n)=0.6;
weights(n)=0/abs(targets(n));

%relative volatility of investment
n=n+1;
[~,jI]=ismember('I',M_.endo_names,'rows');
moments(n)=std(x_hp(jI,:))/std(x_hp(jY,:));
targets(n)=2.9;
weights(n)=1/abs(targets(n));

%relative volatility of nominal interest rate
n=n+1;
[~,ji]=ismember('i',M_.endo_names,'rows');
moments(n)=std(x_hp(ji,:))/std(x_hp(jY,:));
targets(n)=0.23;
weights(n)=0/abs(targets(n));

%relative volatility of price
n=n+1;
[~,jV]=ismember('V',M_.endo_names,'rows');
moments(n)=std(x_hp(jV,:))/std(x_hp(jY,:));
targets(n)=2.99*2.63;
weights(n)=0/abs(targets(n));

%relative volatility of price versus dividend
n=n+1;
[~,jV]=ismember('V',M_.endo_names,'rows');
[~,jDy]=ismember('Dy',M_.endo_names,'rows');
moments(n)=std(x_hp(jV,:))/std(x_hp(jDy,:));
targets(n)=2.63;
weights(n)=0/abs(targets(n));


%negativity of IRF of V after TFP shock
n=n+1;
minIRF=0;
maxIRF=0;
IRFA=B;
for t=1:100
    minIRF=min(minIRF,IRFA(jV,2));
    maxIRF=max(maxIRF,IRFA(jV,2));
    IRFA=A*IRFA;
end
if maxIRF>0
    moments(n)=max(-minIRF/maxIRF-.2,0);
elseif strcmp(M_.fname,'my_bgg2_nofric')
    moments(n)=0;
else
    moments(n)=Inf;
end
targets(n)=0;
weights(n)=1e3;

%negativity of IRF of i after monetary shock
n=n+1;
[~,ji]=ismember('i',M_.endo_names,'rows');
[~,ii]=ismember('epsi1',M_.exo_names,'rows');
posIRF=0;
negIRF=0;
IRFA=B;
for t=1:20
    posIRF=posIRF+max(IRFA(ji,ii),0);
    negIRF=negIRF-min(IRFA(ji,ii),0);
    IRFA=A*IRFA;
end
moments(n)=(negIRF-posIRF)/(negIRF+posIRF);
if isnan(moments(n)); moments(n)=0; end
targets(n)=1;
weights(n)=0;



%% apply weighting matrix and compute result
res=(moments-targets)*diag(weights)*(moments-targets)';
%disp(moments);

end

