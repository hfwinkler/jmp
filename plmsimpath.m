% calculate path of learning economy under PLM, plugging in TFP, interest
% rate shock and observed asset prices

%% get parameters
% NB: need to run my_bgg2_plot first!

my_bgg2_parameters;

%% get the PLM

dynare my_bgg2_learn noclearall
options_.noprint=1;
options_.order=1;
options_.irf=0;
options_.ar=0;
options_.hp_filter=0;
options_.PLM=1;
stoch_simul('');

nexo=M_.exo_nbr; %number of exogenous shocks
nendo=M_.endo_nbr; %number of endogenous variables

C=zeros(length(oo_.dr.state_var),nendo);
for i=1:length(oo_.dr.state_var)
    C(i,oo_.dr.state_var(i))=1;
end
A=oo_.dr.ghx(oo_.dr.inv_order_var,:)*C;
B=oo_.dr.ghu(oo_.dr.inv_order_var,:);

%correction: pred_err and pred_err1 shocks need to be identical
nexo=nexo-1;
B(:,nexo)=B(:,nexo)+B(:,nexo+1); B=B(:,1:nexo);


%% get time series and detrend

data = dataset('File','data\dataexport.csv','Delimiter',',');

T=length(data);

allobs=double(data,{'GDP','tfp','treasury_12m','p'});
allobs=detrend(allobs,1);
Yobs=allobs(:,1);
Aobs=allobs(:,2);
iobs=allobs(:,3)/4;
Vobs=allobs(:,4);


%iobs=iobs*(0.23*std(Yobs)/std(iobs));
%Aobs=Aobs*(0.85*std(Yobs)/std(Aobs));
Vobs=Vobs*(0.89*std(Yobs)/std(Vobs));

datelabel=double(data,'dateint')';

dataobs=[Vobs,Aobs,iobs]';
%dataobs=dataobs(2:3,:);

%% back out shocks

%vector of relevant shocks
shockj=[2 3 nexo];

[~,jV]=ismember('V',M_.endo_names,'rows');
[~,jA2]=ismember('A2',M_.endo_names,'rows');
[~,ji]=ismember('i',M_.endo_names,'rows');

[~,jX]=ismember('X',M_.endo_names,'rows');
[~,jmu]=ismember('mu',M_.endo_names,'rows');

D=zeros(3,nendo); D(1,jV)=1; D(2,jA2)=1; D(3,ji)=1;
%D=zeros(2,nendo); D(1,jA2)=1; D(2,ji)=1;

x=zeros(nendo,T);
v=zeros(nexo,T);
B1=B(:,shockj);

%burn-in
for s=1:300
    v(shockj,1)=(D*B1)^-1*(dataobs(:,1)-D*A*x(:,1));
    x(:,1)=A*x(:,1)+B*v(:,1);  
    %x(jX,1)=x(jX,1)+g1*v(4,1);
    %x(jmu,1)=x(jmu,1)+g2*v(4,1);
end

%actual data
for t=2:T
    v(shockj,t)=(D*B1)^-1*(dataobs(:,t)-D*A*x(:,t-1));
    x(:,t)=A*x(:,t-1)+B*v(:,t);
    %x(jX,t)=x(jX,t)+g1*v(4,t);
    x(jmu,t)=x(jmu,t)+g2*v(4,t);
end

%decomposition by shock and IRF
xdecomp=zeros(nendo,T,nexo);
xirf=zeros(nendo,T,nexo);
for n=1:nexo
    for t=2:T
        Cf=zeros(nexo); Cf(n,n)=1;
        xdecomp(:,t,n)=A*xdecomp(:,t-1,n)+B*Cf*v(:,t);
        if n==4; xdecomp(jmu,t,n)=xdecomp(jmu,t,n)+g2*v(n,t); end
        xirf(:,t,n)=A*xirf(:,t-1,n)+B*Cf*ones(nexo,1)*(t==2);
    end
end
%% plot

varnames={'Y' 'C' 'pi' 'mu', 'V' 'A2' 'i'};
Trange=1:T;

nrows=2;
ncols=ceil(length(varnames)/nrows);
for i=1:nexo
    figure; set(gcf,'name',['IRF to shock #' num2str(i)]);
    for j=1:length(varnames)
        subplot(nrows,ncols,j);
        [yn,jidx]=ismember(varnames{j},M_.endo_names,'rows');
        plot(1:20,xirf(jidx,1:20,i),'LineWidth',1.5); axis tight;
        title(varnames{j});
    end
end

figure; 
for j=1:nexo
    subplot(1,nexo,j);
    plot(datelabel(Trange),v(j,Trange),'LineWidth',1.5); axis tight;
    title(M_.exo_names(j,:));
end

figure;
nrows=2;
ncols=ceil(length(varnames)/nrows);
for j=1:length(varnames)
    subplot(nrows,ncols,j);
    [yn,jidx]=ismember(varnames{j},M_.endo_names,'rows');
    plot(datelabel(Trange),x(jidx,Trange),'LineWidth',1.5); axis tight;
    title(varnames{j});
    if strcmp(varnames{j},'Y')
        hold on
        plot(datelabel(Trange),Yobs(Trange),'r','LineWidth',1.5);
        disp test
    end
end

figure;
nrows=2;
ncols=ceil(length(varnames)/nrows);
for j=1:length(varnames)
    subplot(nrows,ncols,j);
    [yn,jidx]=ismember(varnames{j},M_.endo_names,'rows');
    plot(datelabel(Trange),squeeze(xdecomp(jidx,Trange,:)),'LineWidth',1.5); axis tight;
    title(varnames{j});
    if j==1
        legend(num2str((1:nexo)'),'Location','Best');
    end
end

% figure;
% nrows=2;
% ncols=ceil(nexo/nrows);
% for j=1:nexo
%     subplot(nrows,ncols,j);
%     plot(1:8,sacf(v(j,:),8,0,0));
%     title(['autocorr v(' num2str(j) ')']);
% end
