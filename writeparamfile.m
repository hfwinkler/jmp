function [ ret ] = writeparamfile( paramnames, paramvals, suffix )
%WRITEPARAMFILE writes new my_bgg2 parameter file

if length(paramnames)~=length(paramvals)
    ret=0;
    error 'number of names and values are different.';
end
if isempty(suffix)
    error 'suffix cannot be empty.';
end

fin = fopen('my_bgg2_parameters.mod');
fout = fopen(['my_bgg2_parameters_' suffix '.mod'], 'wt');

tline = 'dummy';
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) 
        foundparam=0;
        for i=1:length(paramvals)
            foundparam=foundparam+i*strncmp(strtrim(tline),[paramnames{i} '='],length(paramnames{i})+1);
        end
        if foundparam==0
            fprintf(fout, '%s\n', tline);
        else
            fprintf(fout, '%s=%.8g; %%ESTIMATED\n',paramnames{foundparam},paramvals(foundparam));
        end
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);

ret=1;

end

