%A variant of BGG with limited commitment
%learning frictionless solution


%load parameters from file:
@#include "my_bgg2_parameters.mod"

var lag_pred_err aux_pred_err;
varexo pred_err pred_err1;

model;

@#include "my_bgg2_common.mod"

%entrepreneurs
@#if entrepreneurs
    #lambdae=1/R0/eqprem;
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#else
    #lambdae=lambda;
    %#lambdae=1/R0;
    Ce=0;
@#endif

%firm side
exp(Rk)=alphaa*exp(q+Y-K(-1)+log(G)+sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(K+Q)=(1-gama+gama*omega)*(exp(N)-c*exp(E))+exp(B); %respect flow of funds
exp(w)=(1-alphaa)*exp(q+Y-L);

%instead of borrowing constraint:
exp(Q)=R0/Rk0*lambda*exp(Rk(+1)); % constant return differential

exp(N)=( exp(Rk+K(-1))-exp(R(-1)+B(-1)) ) / (G*exp(sigeps1));
%exp(D)=gama*exp(N)+(1-gama)*c*exp(E)-(exp(q)-1)*(0*gama+(1-gama)*c)*exp(Y);
exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%asset pricing with learning
@#if laggedupdating
    V=V(-1)+mu-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*(V(-1)-V0)+0.5*g*(sigmav*pred_err1-0.5*(sigmav*aux_pred_err(+1))^2);
@#else
    V=V(-1)+mu(-1)-sigeps1+sigmav*pred_err-0.5*(sigmav*aux_pred_err(+1))^2; 
    mu=rho_mu*mu(-1)-a0*(V(-1)-V0)+0.5*g*(sigmav*pred_err -0.5*(sigmav*aux_pred_err(+1))^2);
@#endif    
aux_pred_err=pred_err;
lag_pred_err=aux_pred_err(-1);

@#if cumdividend
    exp(RV)=G*exp(V-sigeps1)/(exp(V(-1))-exp(D(-1)))*(1-gama)/(1-gama+gama*omega)/R0;
@#else
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;
@#endif

end;

steady_state_model;

Rk=log(Rk0);

@#include "my_bgg2_commonSS.mod"

@#if entrepreneurs
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);
@#else
    Ce=0;
@#endif
Welfaree=Ce;
C_all=log(exp(C)+Ce);

end;

@#include "my_bgg2_commonend.mod"
