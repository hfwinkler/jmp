% update endogenous parameters

betta=G^theta/R0;

%gama=1-(1-BK)/(c*(1-BK)+(1-c)*(Rk0-R0*BK))*(R0*eqprem-G/PD);
%omega=(1-gama)/gama*(G/(R0*eqprem-G/PD)-1);
gama=(1 - G*(1-BK)/((Rk0-R0*BK)*(1-c)+c*(1-BK)))/(1-omega);
PD=1/(R0*eqprem/G-(1-gama)/(1-gama+gama*omega));

V0=log( (gama+(1-gama)*c)*(Rk0-R0*BK) - (1-gama)*c*(1-BK) ) -log(G)+log(PD)+cumdividend*log(R0*eqprem/G);
xi=BK/(xr*(BK+exp(V0-cumdividend*log(R0*eqprem/G)))+(1-xr)/R0);


kappad=Rk0/(1-delta)-1;

Y0=log(1/alphaa*(Rk0-1+delta)/G);
A0=-L0+1/(1-alphaa)*(Y0+alphaa*log(G));
K0=0;

if entrepreneurs
    C0=log((1-alphaa)*exp(Y0) + (R0-G)/G*BK);
else
    C0=log(exp(Y0)-(G-1+delta)/G);
end

eta=(1-alphaa)*exp(Y0-(phi+1)*L0);

if strncmp(M_.fname,'my_bgg2_nofric',14)
    Rk0=R0;
    Y0=L0+A0+alphaa/(1-alphaa)*log(alphaa/(R0-1+delta));
    K0=log(alphaa*G)+Y0-log(R0-1+delta);
    I0=log((G-1+delta)/G)+K0;
    C0=log(exp(Y0)-exp(I0));
    V0=K0;
    kappad=R0/(1-delta)-1;
    eta=(1-alphaa)*exp(Y0-(phi+1)*L0);
    if prefs==2
        deltaJR0=log(1-h/R0)-log(1-h/R0+(1-h)/(1+phi)*(1-alphaa)*exp(Y0-log(1-b)-C0));
    end
end
