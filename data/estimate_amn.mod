% estimate AMN

var pd_est, pd, d_d, mu;

varexo eps, nu;

parameters delta, g, a, s, c;

delta=0.99;
g=0.003;
a=log(1.02)/4;
s=0.1366;
c=0;

model;

pd_est=log(delta)+a+s^2/2-log(1-delta*exp(mu(-1)))+c;
mu=(1-g)*mu(-1)+g*(pd_est-pd_est(-1)+d_d);
d_d=a+s*eps;
pd=pd_est+nu;

end;

steady_state_model;

d_d=a;
mu=d_d;
pd_est=log(delta)+a+s^2/2-log(1-delta*exp(mu))+c;
pd=pd_est;

end;

steady;
check;

shocks;
var eps; stderr s;
var nu; stderr 0.2949;
end;

%stoch_simul(order=1,irf=100);

varobs d_d, pd;

estimated_params;
c, normal_pdf, 0, 0.1;
delta, beta_pdf, 0.99, 0.001, 0.98, 0.995;
g, beta_pdf, 0.0023, 0.003, 0, 0.1;
s, inv_gamma_pdf, 0.2129, inf; 

stderr nu, inv_gamma_pdf, 0.2949, inf;

end;

estimation(datafile=amn_data,order=1,first_obs=300,mh_nblocks=3,mh_replic=10000,mh_jscale=0.7,mh_init_scale=12) pd pd_est;
