use "projectdata.dta", replace
keep date P* treasury_12m D* E* piD Y

gen pd=exp(PD)

gen r=4*log((exp(P)+exp(D))/exp(l.P))
gen rf=4*(l.treasury_12m - piD)
gen dd=d.D
gen ddy=d.Dy
gen dp=d.P
gen dY=d.Y
gen re=r-rf
foreach var of varlist PD pd ddy dp re r rf {
	bootstrap r(mean) r(sd), reps(1000) seed(1) nodots: summarize `var'
}

corr PD l.PD

gmm (Ppop_hp^2-({coeff=1}*{sD=.01})^2) ( Dy_hp^2-{sD=.01}^2), winitial(unadjusted, independent) wmatrix(hac nwest opt)

foreach h of numlist 4 8 20 {
	tssmooth ma re`h'=f.re, window(0 0 `h')
	tssmooth ma r`h'=f.r, window(0 0 `h')
	}


save "temp.dta", replace
foreach h of numlist 4 8 20 {
	drop if re`h'==. | r`h'==.
	bootstrap r(rho), reps(1000) seed(1) nodots: corr PD re`h'
	bootstrap r(rho), reps(1000) seed(1) nodots: corr PD r`h'
	use "temp.dta", clear
}

corr re f(1/4).re
corr r f(1/4).r

corr r4 l4.r4
corr r8 l8.r8
corr r20 l20.r20

