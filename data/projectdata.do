
set more off

// macro data

import excel "fred-aggregates.xls", sheet("data") cellrange(A10) firstrow clear
gen date=qofd(DATE)
recast int date
format %tq date
drop if date==.
order date
drop DATE
tsset date

replace FFR=(1+FFR/100)^.25

foreach var of varlist *DEFL {
	replace `var'=`var'/100
}

gen Y=y*YDEFL
gen C=c*CDEFL
drop y c

foreach var of varlist Y C CD CND CS I IR IN {
	// gen `var'1=`var'
	replace `var'=`var'/`var'DEFL
}
replace B=B/YDEFL


gen piCA=CPI_ALL/L.CPI_ALL
gen piCC=CPI_CORE/L.CPI_CORE
gen piD=YDEFL/L.YDEFL
gen relPI=IDEFL/YDEFL


gen Imodel=IN+CD
gen Cmodel=CND+CS
gen Ymodel=Imodel+Cmodel
gen Yres=Y-Ymodel

gen w2=W*hours/emp/YDEFL
replace u=u/100

save "projectdata.dta", replace

// TFP data
merge 1:1 date using "tfp_util.dta", keep(3) nogenerate
foreach var of varlist tfp util tfp_util {
	replace `var'=exp(`var')
	drop d`var'
}
sort date
save "projectdata.dta", replace


// population data
use "uspopulation.dta", clear
gen over20=all-under20
gen date=qofd(dofy(year))+3
format %tq date
keep date over20 all
merge 1:1 date using "projectdata.dta", nogenerate
sort date
ipolate all date, generate(pop) epolate
ipolate over20 date, generate(popadult) epolate
drop all over20
order date
save "projectdata.dta", replace

// financial data

use "frb-rates.dta", replace

gen spread=(1+corporate_baa/100)/(1+corporate_aaa/100)

keep date treasury_1m treasury_12m spread corporate_aaa

rename date mdate
gen int date=qofd(dofm(mdate))
format %tq date
replace spread=log(spread)
replace treasury_1m=log(1+treasury_1m/100)
replace treasury_12m=log(1+treasury_12m/100)
replace corporate_aaa=log(1+corporate_aaa/100)
collapse (mean) spread treasury_1m treasury_12m corporate_aaa, by(date)
tsset date
replace spread=exp(spread/4)
replace treasury_1m=exp(treasury_1m/4)
replace treasury_12m=exp(treasury_12m/4)
replace corporate_aaa=exp(corporate_aaa/4)
merge 1:1 date using "projectdata.dta", keep(3) nogenerate
save "projectdata.dta", replace

use "equities_quarterly.dta", replace
//choose which data to keep
rename P_COMP P
rename D_COMP D
rename Dy_COMP Dy
rename Ey_COMP Ey
keep date P D Dy Ey
gen PD=P/Dy
gen R=(P+D)/L.P
merge 1:1 date using "projectdata.dta", keep(3) nogenerate
foreach var of varlist P D Dy Ey {
	replace `var'=`var'/YDEFL
	gen `var'pop=`var'
	order `var'pop, after(`var')
}
save "projectdata.dta", replace

// take logs, normalise by population, and filter

drop *DEFL CPI*

order date

foreach var of varlist P-popadult {
	replace `var'=log(`var')
}
//undo for unemployment
replace u=exp(u)

egen popmean=mean(pop)
replace pop=pop-popmean
replace popadult=popadult-popmean
drop popmean
foreach var of varlist *pop I* C* Y* hours emp {
	replace `var'=`var'-popadult
}


foreach var of varlist P-tfp_util {
	tsfilter hp `var'_hp=`var' //for HP filter
	//tsfilter cf `var'_hp=`var' //for Christiano-Fitzgerald filter
	//gen `var'_hp=d.`var' // for first difference
	//quietly reg `var' date //for linear trend
	//predict `var'_hp, residual //for linear trend
	//quietly reg `var' l(8/11).`var' //for Hamilton filter
	//predict `var'_hp, residual //for Hamilton filter
}
	
save "projectdata_fullsample.dta", replace

// select sample and save

keep if year(dofq(date))>=1962 & year(dofq(date))<=2012

gen datenum=year(dofq(date))+(quarter(dofq(date))-1)/4
order date datenum
export excel "projectdata.xls", firstrow(variables) replace
drop datenum
format date %tq
save "projectdata.dta", replace

// summary stats

tabstat *_hp, statistics(sd count) columns(statistics)
corr Ymodel_hp *_hp

// asset price statistics
gen L4PD=l4.PD
tssmooth ma R4=R, window(0 0 4)
tssmooth ma R20=R, window (0 0 20)
gen Rf=treasury_12m-piD
 
//NB returns in the below estimations are real returns
matrix define initval = J(1,18,.001) // .001 for HP filtered, .01 for FD or linear
 gmm ( Ymodel_hp^2-{sY}^2)  ( Dypop_hp^2-{sD}^2) ( Ppop_hp^2-({rV}*{sY})^2) ( Ppop_hp^2-({rV2}*{sD})^2) ( (PD-{ePD})^2-{sPD}^2) ( (R-{eR})^2-{sR}^2) /* standard deviations 
  */  ( R-treasury_12m-{excessR}) ( PD-{ePD}) ( R-{eR}) ( (R-{eR})^3 -{Rskew}*{sR}^3 )  ( (R-{eR})^4 -{Rkurt}*{sR}^4 ) /* equity premium, means of PD and R, skew, kurtosis
  */  ( Rf-{eRf}) ( (Rf-{eRf})^2-{sRf}^2) /* risk free rate
  */ ( (R4-{eR})^2 - {sR4}^2 )  ( (R20-{eR})^2 - {sR20}^2 )  ( (PD-{ePD})*( R4-{eR}) - {sPD}*{sR4}*{c4} ) ( (PD-{ePD})*( R20-{eR}) - {sPD}*{sR20}*{c20} ) /* return predictability
  */ ( (PD-{ePD})*(L4PD-{ePD}) - {PDac}*{sPD}^2 ) /* PD autocorrelation
  */ , winitial(unadjusted, independent) wmatrix(hac nwest opt) vce(hac nwest opt) from(initval)


// calculate covariance matrix of stddevs and correlations for SMM

matrix define initval = J(1,18,.5)

 gmm ( Ymodel_hp^2-{sY}^2) ( emp_hp^2-({rL}*{sY})^2) ( Imodel_hp^2-({rI}*{sY})^2) ( Cmodel_hp^2-({rC}*{sY})^2)  ( piD_hp^2-{spi}^2) ( FFR_hp^2-{si}^2) ( Dypop_hp^2-({rDy}*{sY})^2) ( Ppop_hp^2-({rV}*{sY})^2) ( (PD-{ePD})^2-{sPD}^2) ( (R-{eR})^2-{sR}^2) /* standard deviations 
 */  ( (Ymodel_hp*emp_hp)-{corrL}*{sY}^2*{rL}) ( (Ymodel_hp*Cmodel_hp)-{corrC}*{sY}^2*{rC}) ( (Ymodel_hp*Imodel_hp)-{corrI}*{sY}^2*{rI}) ( (Ymodel_hp*Dypop_hp)-{corrDy}*{sY}^2*{rDy})  /* correlations with output
  */  ( R-treasury_12m-{excessR}) ( PD-{ePD}) ( R-{eR}) ( (R-{eR})*(l.R-{eR})-{acR}*{sR}^2 ) /* equity premium, means of PD and R, autocorr of R
 */ , winitial(unadjusted, independent) wmatrix(hac nwest opt) vce(hac nwest opt) from(initval)

matrix list e(V)
matrix define S=e(V)
matrix list e(b)
matrix define m=e(b)
matrix define T=_N
matrix define N=colsof(S)
matrix colnames m=`e(params)'
clear
svmat N
svmat T
svmat S
svmat m, names(eqcol) 
format * %20.19e
outsheet using "smmexport.csv", replace

