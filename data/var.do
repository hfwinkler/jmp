set more off

// get the data

use "projectdata.dta", replace

// var analysis

rename spread s
rename tfp_util z

keep date P D PD Dy s I z FFR B

varsoc FFR I z Dy B PD, exog(date)
var I z FFR Dy B PD, lags(1/2) exog(date)
irf set var_irf, replace
irf create IRF, set(var_irf) step(24) bs replace
irf graph oirf, impulse(PD) byopts(yrescale) level(95) name(I2_p, replace)
irf table fevd, impulse(PD) noci
