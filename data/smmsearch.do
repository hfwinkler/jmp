// produce SMM for search model, 3rd paper
// to run after projectdata.do

use "projectdata.dta", replace

 gmm ( Y_hp^2-{vY}) ( w2_hp^2-{vw}) ( u_hp^2-{vN}) ( PD_hp^2-{vPD}) /*
 */ , winitial(unadjusted, independent) wmatrix(hac nwest opt)

matrix list e(V)
matrix define S=e(V)
matrix list e(b)
matrix define m=e(b)
matrix define T=_N
matrix define N=colsof(S)
matrix colnames m=`e(params)'
clear
svmat N
svmat T
svmat S
svmat m, names(col) 
format * %20.19e
outsheet using "smmsearchexport.csv", replace
