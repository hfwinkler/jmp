import excel "fred-moreaggregates.xls", sheet("Sheet1") cellrange(A20:O285) firstrow clear

gen date=qofd(DATE)
recast int date
format %tq date
order date
drop DATE
tsset date

drop if i==.
drop if pi==.
drop if Y==.

keep date i pi Y

replace i=0.25*log(1+i/100)
replace pi=log(1+pi/100)
replace Y=log(Y)

tsfilter hp Y_hp=Y
gen di=d.i
gen dpi=d.pi
gen dY=d.Y
gen li=l.i
gen ldi=ld.i
gen pi4=0.25*(pi+l.pi+2l.pi+3l.pi)

keep if year(dofq(date))>1980 & year(dofq(date))<2013

nl ( i = {rho}*li + (1-{rho})*({alpha} + {phipi}*pi+{phiY}*dY) ) 
nl ( i = {rho}*li + (1-{rho})*({alpha} + {phipi}*pi) ) 
nl ( i = {rho}*li + (1-{rho})*({alpha} + {phipi}*pi4+{phiY}*dY) ) 
arima i pi dY, ar(1)



