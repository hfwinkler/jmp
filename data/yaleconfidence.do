// make Yale investor confidence dta file

import excel "yaleconfidence.xls", sheet("data") firstrow clear
drop *stderr
drop if Date==.
rename Date date
replace date=mofd(date)
format %tmCCYY-NN date

tsset date
save "yaleconfidence.dta", replace

// merge other stuff
merge 1:1 date using "equities_quarterly.dta", keepusing(P_Shiller Dy_Shiller Ey_Shiller CPI)
rename P_shiller P
rename Dy_Shiller D
rename Ey_Shiller E
gen p=P/CPI
gen d=D/CPI
gen e=E/CPI
merge 1:1 date using "frb-rates.dta", nogenerate keep(3) keepusing(treasury_12m)
drop if treasury_12m==.
sort date

foreach var of varlist P D E p d e CPI {
	replace `var'=log(`var')
}
replace treasury_12m=log(1+treasury_12m/100)

gen PD=P-D
gen R=12*(log(exp(P)+exp(D))-L.P)
gen D_P=12*D.P

tssmooth ma R1y=R, window(0 0 12)
gen Re1y=R1y-treasury_12m
tssmooth ma D_P1y=D_P, window(0 0 12)

drop if _merge~=3

pwcorr *1y PD conf* val*

twoway (scatter conf_inst Re1y) (lfit conf_inst Re1y), legend(off) xtitle("Confidence") ytitle("Returns 1 year from now") name("inst1", replace)
twoway (scatter conf_ind Re1y) (lfit conf_ind Re1y), legend(off) xtitle("Confidence") ytitle("Returns 1 year from now") name("ind1", replace)
