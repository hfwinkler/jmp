// imports a series of variables from the SPF into a single Stata file

set more off

//format 1: levels
clear
gen date=0
save "spf.dta", replace
foreach var in PGDP RGDP RCONSUM RNRESIN EMP { //logged levels
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=log(`var'1)
	gen `var'0=.
	order date `var'9 `var'0
	foreach q of numlist 0/4 {
		local q2=`q'+2
		replace `var'`q'=log(`var'`q2')
	}
	keep date `var'9-`var'4
	merge 1:1 date using "spf.dta", nogenerate
	save "spf.dta", replace
}
foreach var in UNEMP { //not logged
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=log(`var'1)
	gen `var'0=.
	order date `var'9 `var'0
	foreach q of numlist 0/4 {
		local q2=`q'+2
		replace `var'`q'=`var'`q2'/100
	}
	keep date `var'9-`var'4
	merge 1:1 date using "spf.dta", nogenerate
	save "spf.dta", replace
}
foreach var in CPI CORECPI TBILL TBOND { //logged rates
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=log(1+`var'1/100)/4
	gen `var'0=.
	order date `var'9 `var'0
	foreach q of numlist 0/4 {
		local q2=`q'+2
		replace `var'`q'=log(1+`var'`q2'/100)/4
	}
	keep date `var'9-`var'4
	format %tq date
	merge 1:1 date using "spf.dta", nogenerate
	save "spf.dta", replace
}
tsset date
save "spf.dta", replace


//format 2: cumulative growth rates
clear
gen date=0
save "spfcum.dta", replace
foreach var in PGDP RGDP RCONSUM RNRESIN EMP { //from levels, logged
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=log(`var'1)
	gen `var'0=log(`var'2)
	order date `var'9 `var'0
	foreach q of numlist 1/4 {
		local q2=`q'+2
		replace `var'`q'=log(`var'`q2')-`var'0
	}
	replace `var'0=`var'0-`var'9
	keep date `var'9-`var'4
	
	merge 1:1 date using "spfcum.dta", nogenerate
	save "spfcum.dta", replace
}
foreach var in UNEMP { //from levels, not logged
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=`var'1/100
	gen `var'0=`var'2/100
	order date `var'9 `var'0
	foreach q of numlist 1/4 {
		local q2=`q'+2
		replace `var'`q'=`var'`q2'/100-`var'0
	}
	replace `var'0=`var'0-`var'9
	keep date `var'9-`var'4
	
	merge 1:1 date using "spfcum.dta", nogenerate
	save "spfcum.dta", replace
}
foreach var in CPI CORECPI TBILL { //from qoq growth
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen date=yq(year,quarter)
	drop year quarter
	gen `var'9=log(1+`var'1/100)/4
	gen `var'0=0
	gen `var'00=log(1+`var'2/100)/4
	order date `var'9 `var'0
	foreach q of numlist 1/4 {
		local qm1=`q'-1
		local q2=`q'+2
		replace `var'`q'=log(1+`var'`q2'/100)/4+`var'`qm1'
	}
	replace `var'0=`var'00
	keep date `var'9-`var'4
	format %tq date
	merge 1:1 date using "spfcum.dta", nogenerate
	save "spfcum.dta", replace
}
tsset date
save "spfcum.dta", replace


//format 3: panel
clear
gen int date=0
gen int  adate=0
save "spfpanel.dta", replace
foreach var in PGDP RGDP RCONSUM RNRESIN UNEMP CPI CORECPI TBILL TBOND {
	import excel "SPFLevel.xls", sheet("`var'") firstrow clear
	mvdecode _all, mv(-999)
	gen int date=yq(year,quarter)
	drop year quarter
	keep date `var'1-`var'6
	reshape long `var', i(date) j(horizon)
	replace `var'=log(`var')
	gen int adate=date+horizon-2
	drop horizon
	order date adate
	format %tq date
	format %tq adate
	merge 1:1 date adate using "spfpanel.dta", nogenerate
	save "spfpanel.dta", replace
}
tsset date adate
save "spfpanel.dta", replace
*/
