// can certain estimates about forecast errors be detected with confidence in small samples?
// run simstats.do first!

local samplesize=160
local T=_N-`samplesize'


tempname sim
postfile `sim' b t using "test.dta", replacequietly {forvalues i = 1/`T' {
	local j=`i'+`samplesize'	reg Yerr Yrev if date>`i' & date<`j'	scalar b = _b[Yrev]	scalar t = _b[Yrev]/_se[Yrev]	post `sim' (b) (t)}}postclose `sim'use "test.dta", clearsummarize
