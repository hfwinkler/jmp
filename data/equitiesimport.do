// assemble all equity price data from different sources


// start with Shiller's dataset

import excel "ie_data.xls", sheet("Data") cellrange(A8:L2423) firstrow clear
drop if Date==.
drop if _n==_N
destring P RateGS10 CPI, replace
replace RateGS10=1+RateGS10/100
gen year=floor(Fraction )
gen month=(Fraction - year)*12+0.5
gen date=ym(year,month)
format date %tm
tsset date
order date
drop year month
drop Date Fraction Price-L
rename P P_Shiller
rename D Dy_Shiller
rename E Ey_Shiller
save "equities.dta", replace

// CRSP data

import excel "wrds.xlsx", sheet("CRSP") firstrow clear
tostring CalendarDate, replace
gen int day=daily(CalendarDate, "YMD")
gen int date=mofd(day)
format %tm date
drop CalendarDate day
order date
tsset date
keep date Idxw* LeveloftheSP500Index divs
rename LeveloftheSP500Index P_SP500
rename Idxwodivs P_CRSP
rename divs D_CRSP
tssmooth ma Dy_CRSP=D_CRSP, window(11 1)
replace Dy_CRSP=12*Dy_CRSP
drop Idxwdivs
merge 1:1 date using "equities.dta", nogenerate
sort date
save "equities.dta", replace

// IHS Global data
import excel "wrds.xlsx", sheet("IHS") firstrow clear
tostring Date, replace
gen day=daily(Date, "YMD")
gen int date=mofd(day)
format %tm date
drop Date day
order date
tsset date
rename extrapolateddividendfrommonre D_IHS
keep date D_IHS
tssmooth ma Dy_IHS=D_IHS, window(11 1)
replace Dy_IHS=12*Dy_IHS
merge 1:1 date using "equities.dta", nogenerate
sort date
save "equities.dta", replace

// Compustat data
import excel "wrds.xlsx", sheet("Compustat") clear firstrow
tostring DataDateIndexMonthly,replace
gen day=daily(DataDateIndexMonthly , "YMD")
gen int date=mofd(day)
format %tm date
drop DataDateIndexMonthly  day
order date
tsset date
rename BookValuePerShareIndex B
rename DividendPerShareIndex D_COMP
tssmooth ma Dy_COMP=D_COMP, window(11 1)
replace Dy_COMP=12*Dy_COMP
rename B B_COMP
drop IndicatedAnnualDividend
rename EarningsPerShare12MMInde Etemp
gen Ey_COMP=L2.Etemp
drop Etemp
rename IndexPriceCloseMonthly P_COMP
keep date-P_COMP Dy_COMP Ey_COMP
merge 1:1 date using "equities.dta", nogenerate
sort date
save "equities.dta", replace

// generate quarterly series

gen int qdate=qofd(dofm(date))
collapse (last) P_* Dy_* Ey_* CPI RateGS10 B_* (sum) D_*, by(qdate)
format %tq qdate
rename qdate date
tsset date
foreach var of varlist D_* {
	replace `var'=. if `var'==0
	}
save "equities_quarterly.dta", replace
