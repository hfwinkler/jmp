// run some tests based on SPF data
set more off
use "spfcum.dta", clear

// import actual data
merge 1:1 date using "projectdata_fullsample.dta", keep(3) nogenerate
drop *_hp

reg PD date
predict PD2, r

gen Yerr=f3.Y-Y - RGDP3
gen Yrev=RGDP3-(l.RGDP4-l.RGDP1)
gen Cerr=f3.C-C - RCONSUM3
gen Crev=RCONSUM3-(l.RCONSUM4-l.RCONSUM1)
gen Ierr=f3.IN-IN - RNRESIN3
gen Irev=RNRESIN3-(l.RNRESIN4-l.RNRESIN1)
gen Nerr=f4.EMP9-EMP9 - EMP3
gen Nrev=EMP3-(l.EMP4-l.EMP1)

gen Uerr=f4s3.UNEMP9 - UNEMP3
gen Urev=UNEMP3-(l.UNEMP4-l.UNEMP1)

gen piCerr=f.piCA+f2.piCA+f3.piCA - CPI3
gen piCrev=CPI3-(l.CPI4-l.CPI1)
gen piDerr=f.piD+f2.piD+f3.piD - PGDP3
gen piDrev=PGDP3-(l.PGDP4-l.PGDP1)
//gen piDerr=f3.piD - PGDP3+PGDP2
//gen piDrev=PGDP3-PGDP2-(l.PGDP4-l.PGDP3)
gen ierr=f.treasury_1m+f2.treasury_1m+f3.treasury_1m - TBILL3
gen irev=TBILL3-(l.TBILL4-l.TBILL1)
//gen ierr=f3.treasury_1m - TBILL3 + TBILL2
//gen irev=TBILL3-TBILL2-(l.TBILL4-l.TBILL3)

drop if Cerr==. //balanced sample, but ignores 13 years of Y and piD observations!

// regs
local varlist "Y C I N U piC piD i"
foreach var of local varlist {
	newey `var'err l3.PD, lag(4)
	newey `var'err lD.PD, lag(4)
	newey `var'err `var'rev, lag(4)
}

