%Wouter's COMOV statistics with bootstrapped confidence intervals
%
%this is a test on simulated data to analyse the method!
%


M=60; %maximum period for forecast errors
N=100; %number of simulations
laglength=1; %VAR lag length
alpha=5; %size of confidence interval
method=1; %inference method

%simulated data:
K=2;
T=20*12; %n_obs adjusted for initial lags
trange=(1-laglength):T;
exodata=[trange.^0; trange.^1; trange.^2]'; %constant, trends...
L=size(exodata,2);
exodata=reshape(kron(exodata',ones(K,1)),K,L,T+laglength);
exodata=squeeze(num2cell(exodata, [1 2]));

varx_true=vgxset('n',K,'nAR',laglength,'nX',L);
varx_true.AR{1}=[1 0.1; 0 0.5];
varx_true.b=[0; 0; 0];
varx_true.Q=[1 0; 0 1];

%generate sample paths
data=vgxproc(varx_true,reshape(randn(T+laglength,K*N)*kron(eye(N),varx_true.Q^0.5),T+laglength,K,N),exodata);

%cut exogenous data off at t==1
exodata=exodata(laglength+(1:T));

%calculate true comovement statistics
    var_true=vgxset(varx_true,'nX',0,'b',[],'bsolve',[]);
    var_ma_true = vgxma(var_true,M-1);
    covar_true=zeros(K,K,M);
    covar_true(:,:,1)=var_ma_true.Q;
    for m=2:M
        covar_true(:,:,m)=covar_true(:,:,m-1)+var_ma_true.MA{m-1}'*var_ma_true.Q*var_ma_true.MA{m-1};
    end
    for m=1:M
        covar_true(:,:,m)=corrcov(covar_true(:,:,m));
    end

%% calculate Wouter's statistic

covar=zeros(K,K,M,N);
h=waitbar(0,'Please wait..');
for n=1:N
    
    varx_spec = vgxset('n',K,'nAR',laglength,'nX',L);
    varx_est = vgxvarx(varx_spec,data(laglength+(1:T),:,n),exodata,data(1:laglength,:,n));
    resid_est = vgxinfer(varx_est,data(laglength+(1:T),:,n),exodata,data(1:laglength,:,n));

    if method==1

        %generate forecasted paths
        forecast_res=reshape(repmat(resid_est,1,T),T,K,T).*reshape(repmat(triu(ones(T),1),K,1),T,K,T);
        forecasts=zeros(M,K,T);
        for t=0:(T-1)
            Mbar=min(M,T-t);
            forecasts(1:Mbar,:,t+1)=vgxproc(varx_est,zeros(Mbar,K),exodata(t+(1:Mbar)),data(1:(laglength+t),:,n));
        end

        %generate m-period ahead forecast errors and compute correlation
        for m=1:M
            m_forecasterror=zeros(T-m+1,K);
            for k=1:K
                m_forecasterror(:,k)=data(laglength+(m:T),k,n)-squeeze(forecasts(m,k,1:(T-m+1)));
            end
            covar(:,:,m,n)=corr(m_forecasterror);
        end

    elseif method==2

        %transform into MA representation
        var_est=vgxset(varx_est,'nX',0,'b',[],'bsolve',[]);
        var_ma = vgxma(var_est,M-1);

        covar(:,:,1,n)=var_ma.Q;
        for m=2:M
            covar(:,:,m,n)=covar(:,:,m-1,n)+var_ma.MA{m-1}'*var_ma.Q*var_ma.MA{m-1};
        end
        for m=1:M
            covar(:,:,m,n)=corrcov(covar(:,:,m,n));
        end

    end
    waitbar(n/N);
end
close(h);

%confidence intervals
covar_upper=prctile(covar,100-alpha/2,4);
covar_lower=prctile(covar,alpha/2,4);

%% plot
tscale=1:M;
for k=1:K
    for l=1:k
        subplot(K,K,(k-1)*K+l);
        plot(tscale,squeeze(covar_true(k,l,:)),'r',tscale,squeeze(covar_upper(k,l,:)),'k',tscale,squeeze(covar_lower(k,l,:)),'k');
        title([num2str(k) ' / ' num2str(l)]);
    end
end
