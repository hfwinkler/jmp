// make Yale-Fuqua CFO survey dta file and some preliminary stuff

// import survey data

import excel "cfosurvey.xlsx", sheet("Sheet1") firstrow clear
gen int qdate=qofd(referencedate)
gen int date=mofd(referencedate)-1 //the information available is the one at the end of the last month, not this month!
format %tq qdate
format %tm date
replace qdate = quarterly("2002Q3","YQ") in 9
replace date = monthly("2002m9","YM") in 9
order date qdate
rename yeartreasuryyield treasury10y
rename yearlow110 low10y
rename yearbestguess exp10y
rename yearhigh110 high10y
rename G low1y
rename H exp1y
rename I high1y
save "cfosurvey.dta", replace


// merge actual observations
merge 1:1 date using "equities.dta", keepusing(P_COMP D_COMP Dy_COMP CPI) nogenerate
sort date
tsset date
rename P_COMP P
rename D_COMP D
rename Dy_COMP Dy

gen PD=log(P/Dy)
gen R=log((P+D)/L.P)
replace R=100*R
tssmooth ma Ry=R, window(11 1)
replace Ry=12*Ry
gen R1y=f12.Ry
gen R1yf=f.R1y
gen Re1y=log(1+exp1y/100)*100
gen ferr=R1y-Re1y
gen ferrf=R1yf-Re1y

tsset date

twoway (tsline PD, yaxis(1)) (tsline exp1y exp10y, yaxis(2)) if qdate~=., name("exp", replace) ytitle("log P/D ratio")

reg ferr PD, vce(robust)
reg ferr S3.PD, vce(robust)
reg Re1y PD, vce(robust)
reg R1y PD, vce(robust)

reg ferrf PD, vce(robust)
reg ferrf S3.PD, vce(robust)
reg R1yf PD, vce(robust)


tsset qdate
sureg R1y Re1y = PD
test [R1y]PD=[Re1y]PD

drop if ferr==.
save "cfosurvey.dta", replace

twoway (scatter Re1y PD) (lfit Re1y PD), legend(off) ytitle("1-year return forecast") xtitle("log P/D ratio") name("f1ypd", replace)
twoway (scatter R1y PD) (lfit R1y PD), legend(off) ytitle("1-year realised return") xtitle("log P/D ratio") name("r1ypd", replace)
twoway (scatter Re1y PD, mcolor(navy)) (lfit Re1y PD, lcolor(maroon)) (scatter R1y PD, mcolor(forest_green) msymbol(T)) (lfit R1y PD, lcolor(maroon)) if R1y>-40, /*
*/ legend(label(1 "expected") label(3 "realised") order(1 3) ring(0)) ytitle("1-year return") xtitle("log P/D ratio") name("pdscatter", replace)
graph display, xsize(5)
