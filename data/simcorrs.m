% small-sample correlation coefficients for forecast error predictability

%% import data (Matlab-generated code)
filename = '/msu/res5/m1hxw02/jmp/data/simstats.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

Verr = dataArray{:, 1};
RVerr = dataArray{:, 2};
Yerr = dataArray{:, 3};
Cerr = dataArray{:, 4};
Ierr = dataArray{:, 5};
Lerr = dataArray{:, 6};
Derr = dataArray{:, 7};
Eerr = dataArray{:, 8};
pierr = dataArray{:, 9};
ierr = dataArray{:, 10};
Vrev = dataArray{:, 11};
RVrev = dataArray{:, 12};
Yrev = dataArray{:, 13};
Crev = dataArray{:, 14};
Irev = dataArray{:, 15};
Lrev = dataArray{:, 16};
Drev = dataArray{:, 17};
Erev = dataArray{:, 18};
pirev = dataArray{:, 19};
irev = dataArray{:, 20};
pd = dataArray{:, 21};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

uerr=-Lerr;
urev=-Lrev;
%% perform computations

ynames={'RV','Y','I','C','u','i','pi'};

N=length(ynames);
tab=nan(length(ynames),4,3);

Ts=[48 123 123 123 123 123 123];

for i=1:N
    T=Ts(i);
    trange=-(T-1):0;
    disp(ynames{i});
    eval(['X1=' ynames{i} 'err;']);
    for j=1:3
        switch j
            case 1; X2=pd;
            case 2; X2=filter([1 -1],1,pd);
            case 3; eval(['X2=' ynames{i} 'rev;']);
        end
        corrset=nan(size(X1));
        for t=T:length(Cerr)
            corrset(t)=corr(X1(t+trange),X2(t+trange));
        end
        tab(i,:,j)=[corr(X1,X2,'rows','complete'),std(corrset,'omitnan'),prctile(corrset,[2.5 97.5])];
    end
end


%% plot

ylabels={'R_{t+4}^{stock}','Y_{t+3}','I_{t+3}','C_{t+3}','u_{t+3}','i_{t+3}','\pi_{t+3}'};

% surveydata=[-44 -32 -33 -26 22 -3 62; %old
%             6 22 25 21 -27 8 -1;
%             NaN 29 31 23 43 29 24] / 100;
surveydata=[-44 -26 -23 -19 11 -21 64;
            6 22 25 22 -28 5 -1;
            NaN 30 28 23 43 30 22] / 100;     
datastars={'***' '**' '*' '*' '' '' '***'; %Newey-West
           '' '*' '**' '**' '**' '' '';
           '' '***' '***' '**' '***' '***' '**'};  
        
        
for j=1:3
    figure;
    %myis=find(~isnan(surveydata(j,:)));
    myis=1:N;
    Ni=length(myis);
    set(gcf,'renderer','painters');
    errorbar(1:Ni,tab(myis,1,j),tab(myis,1,j)-tab(myis,3,j),tab(myis,4,j)-tab(myis,1,j),'bx');
    hold on; 
    plot(1:Ni,surveydata(j,myis),'or','MarkerFaceColor','r');
    plot(0:Ni+1,zeros(1,Ni+2),'k');
    xlim([0.7 Ni+0.9]);
    ylim([-1 1]);
    for i=1:Ni
        blueval=tab(myis(i),1,j);
        redval=surveydata(j,myis(i));
        if blueval>=redval
            yadjust=0.06-min(blueval-redval,0.06);
        else
            yadjust=-0.06-max(blueval-redval,-0.06);
        end
        bluetext=sprintf('%0.2f',blueval);
        redtext=sprintf('%0.2f%s',redval, datastars{j,myis(i)});
        text(i+0.1,blueval+0.7*yadjust,bluetext,'color','blue');
        text(i+0.1,redval-0.7*yadjust,redtext,'color','red');
    end
    %set(gca,'TickLabelInterpreter','latex');
    set(gca,'XTickLabel',ylabels(myis));
    set(gcf,'Position',[498   528   467   375]);
    if j==1; legend('Model','Data','RE','Location','NorthWest'); else; legend('Model','Data','RE','Location','NorthEast'); end
    export_fig(['candles' num2str(j) '.pdf']);
end
