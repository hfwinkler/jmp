// Campbell-Shiller decomposition

// PART I: DATA

use "equities_quarterly.dta", clear

gen d=log(D_COMP)
gen dy=log(Dy_COMP)
gen p=log(P_COMP)
gen pd=p-d

//quarterly
gen dd=D.d
gen r=(exp(p)+exp(d))/exp(l.p)
tabstat dd r
local rho=exp(.015975 - .0236596)
local k=8
gen regpd=f`k'.pd
gen regret=0
gen regdd=0
forvalues i=`k'(-1)1 {
replace regret=`rho'*regret+f`i'.r
replace regdd=`rho'*regdd+f`i'.dd
}
reg regret pd
reg regdd pd
reg regpd pd

//annually
gen year=year(dofq(date))
collapse (last) p dy, by(year)
gen pdy=p-dy
tsset year
drop if year==2014
gen dd=d.d
gen r=log((exp(p)+exp(dy))/exp(l.p))
tabstat dd r
local rho=exp(.0548269 - .0954845)
local k=5
gen regpdy=f`k'.pdy
gen regret=0
gen regdd=0
forvalues i=`k'(-1)1 {
replace regret=`rho'*regret+f`i'.r
replace regdd=`rho'*regdd+f`i'.dd
}
reg regret pdy
reg regdd pdy
reg regpdy pdy

// PART II: MODEL

insheet using "../my_bgg2_simlearn.csv", comma clear case
destring _all, replace
gen date=_n
format %tq date
order date
tsset date

local G=1.0158^.25
replace i=4*i
egen R0=mean(i)
replace RV=RV+R0+pi

gen CPI=0
gen trend=0
replace CPI=pi+CPI[_n-1] if _n>1
replace trend=trend+`G' if _n>1
replace D=exp(D+CPI+trend)
replace V=V+CPI+trend
tssmooth ma Dy=D, window(3 1)
replace Dy=log(4*Dy)
replace D=log(D)

gen pd=V-D
gen pdy=V-Dy

//quarterly
gen dd=D.D
tabstat dd RV pd
local rho=1-1/exp(.0130574)*(1+`G')/exp(5.036412)
local k=20
gen regpd=f`k'.pd
gen regret=0
gen regdd=0
forvalues i=`k'(-1)1 {
replace regret=`rho'*regret+f`i'.RV
replace regdd=`rho'*regdd+f`i'.dd
}
reg regret pd
reg regdd pd
reg regpd pd

/*
//annually
gen year=year(dofq(date))
tssmooth ma R1y=RV, window(3 1)
replace R1y=4*R1y
collapse (last) V Dy R1y, by(year)
tsset year
gen dD=D.Dy
