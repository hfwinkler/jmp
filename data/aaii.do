// make AAII dta file

import excel "aaii sentiment.xls", sheet("SENTIMENT") cellrange(A4:M1403)  firstrow clear
keep Date Spread Close
drop if Spread==.
rename Date ddate
sort ddate
gen date=mofd(ddate)
format %tmCCYY-NN date


collapse (mean) sentiment=Spread, by(date)

tsset date


// merge other stuff
merge 1:1 date using "equities.dta", keepusing(P_Shiller Dy_Shiller Ey_Shiller CPI)
rename P_Shiller P
rename Dy_Shiller D
rename Ey_Shiller E
gen p=P/CPI
gen d=D/CPI
gen e=E/CPI
merge 1:1 date using "frb-rates.dta", nogenerate keep(3) keepusing(treasury_12m)
drop if treasury_12m==.
sort date

foreach var of varlist P D E p d e CPI {
	replace `var'=log(`var')
}
replace treasury_12m=log(1+treasury_12m/100)

gen PD=P-D
gen R=12*(log(exp(P)+exp(D))-L.P)
gen D_P=12*D.P

tssmooth ma R6m=R, window(0 0 6)
gen Re6m=R6m-treasury_12m
tssmooth ma D_P6m=D_P, window(0 0 6)
tssmooth ma Rlastq=R, window(2 1 0)

drop if _merge~=3

save "aaii.dta", replace

reg sentiment l.sentiment PD R

reg Re6m sentiment

twoway (scatter sentiment Re6m ) (lfit sentiment Re6m ), legend(off) ytitle("AAII 6 month ahead sentiment") xtitle("S&P500 6 month ahead return") name("sentfuture", replace)
twoway (scatter sentiment R ) (lfit sentiment R ), legend(off) ytitle("AAII 6 month ahead sentiment") xtitle("S&P500 past month return") name("sentpast", replace) 
