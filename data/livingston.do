// make Livingston dta file and some preliminary stuff

// merge Livingston subsamples

import excel "livingston2_spif.xls", sheet("SPIF") firstrow clear
rename Date date
replace date=mofd(date)
format %tmCCYY-NN date
collapse (median) Forecast*, by(date)
save "livingston.dta", replace

import excel "livingston_Medians.xls", sheet("AMEDIAN") firstrow clear
rename Date date
replace date=mofd(date)
format %tmCCYY-NN date
merge 1:1 date using "livingston.dta", nogenerate


// merge Shiller PD measure
merge 1:1 date using "equities.dta", keepusing(P_Shiller Dy_Shiller)
gen PD=log(P_Shiller/Dy_Shiller)
tsset date
gen PD1m=l.PD
gen DPD=l.PD-l4.PD
keep if _merge==3
drop _merge
save "livingston.dta", replace

// merge price data
import excel "sp500-endofmonth.xls", sheet("Sheet1") firstrow clear
rename Date date
replace date=mofd(date)
format %tmCCYY-NN date
tsset date
gen Close1m=l.Close
keep date Close*
merge 1:1 date using "livingston.dta", nogenerate keep(3)

tsset date, delta(6)
drop if year(dofm(date))<1957

foreach var of varlist Close* Forecast* {
	replace `var'=log(`var')
}
gen DP6m=f.Close-Close1m
gen DP12m=f2.Close-Close1m
gen prevDP=Close1m-l.Close
gen f6_err=f.Close-Forecast6Month
gen f12_err=f2.Close-Forecast12Month
gen Forecast6MonthD=Forecast6Month-Close1m
gen Forecast12MonthD=Forecast12Month-Close1m

save "livingston.dta", replace

reg f12_err PD1m
reg f12_err Forecast12MonthD
reg DP12m Forecast12MonthD
reg Forecast12MonthD PD1m


twoway (scatter f12_err Forecast12MonthD) (lfit f12_err Forecast12MonthD), legend(off) xtitle("12 month forecast") ytitle("Forecast error") name("f12merr", replace)

twoway (tsline Forecast12MonthD, yaxis(1)) (tsline f12_err, yaxis(2)) if year(dofm(date))>1990, name("f12mtsline", replace)
