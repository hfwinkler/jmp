set more off

// assemble asset price and expectation data

use "ie_data.dta", clear
merge 1:1 date using "frb-rates.dta", nogenerate keep(3)
drop if treasury_12m==.
merge 1:1 date using "livingston.dta", nogenerate keepusing(*err)
merge 1:1 date using "cfosurvey.dta", nogenerate keepusing (forecasterror)

gen spread=corporate_baa-corporate_aaa

drop CPI Fraction CAPE corporate*

gen PD=P/D

rename date mdate
gen int date=qofd(dofm(mdate))
format %tq date
order mdate date

foreach var of varlist P D E p d e Close* Forecast* PD {
	replace `var'=log(`var')
}
foreach var of varlist RateGS10 treasury* spread {
	replace `var'=log(1+`var'/100)
}

gen R=12*(log(exp(P)+exp(D))-L.P)
gen D_P=12*D.P

tssmooth ma R1y=R, window(0 0 12)
gen Re1y=R1y-treasury_12m
tssmooth ma D_P1y=D_P, window(0 0 12)


pwcorr sentiment - Forecast12Month Re1y D_P1y PD

collapse (mean) P PD p treasury_12m-D_P1y (last) Close D E d e, by(date)
tsset date

// merge real data

merge 1:1 date using "projectdata.dta", keep(3) nogenerate


// summary stats

tabstat *, statistics(sd count) columns(statistics)

// var analysis

rename spread s
rename tfp_util z


keep date sentiment p d s I z

var d.I d.z d.p s sentiment, lags(1 2) 
irf set var_irf, replace
irf create IRF, set(var_irf) bs step(20) replace
irf graph oirf, byopts(yrescale) level(95) name(I2_p, replace)
vargranger
