// what predicts forecast errors?
set more off

// get SPF data. 
set more off
use "spfcum.dta", clear

// import macro data
merge 1:1 date using "projectdata_fullsample.dta", keep(3) nogenerate
drop *_hp B-LOANS_CPI tfp

// generate forecast errors on 3-quarter ahead growth rates, and forecast revisions
// Note: forecasters only know the data up to the last quarter
gen Yerr=f4.Y-f.Y - f.RGDP3
gen Yrev=f.RGDP3-(RGDP4-RGDP1)
gen Cerr=f4.C-f.C - f.RCONSUM3
gen Crev=f.RCONSUM3-(RCONSUM4-RCONSUM1)
gen Ierr=f4.IN-f.IN - f.RNRESIN3
gen Irev=f.RNRESIN3-(RNRESIN4-RNRESIN1)
gen Uerr=f5.UNEMP9-f2.UNEMP9 - f.UNEMP3
gen Urev=f.UNEMP3-(UNEMP4-UNEMP1)
gen piCerr=f2.piCA+f3.piCA+f4.piCA - f.CPI3
gen piCrev=f.CPI3-(CPI4-CPI1)
gen piDerr=f2.piD+f3.piD+f4.piD - f.PGDP3
gen piDrev=f.PGDP3-(PGDP4-PGDP1)
gen ierr=f2.treasury_12m+f3.treasury_12m+f4.treasury_12m - f.TBILL3
gen irev=f.TBILL3-(TBILL4-TBILL1)

drop TBILL9-PGDP4

// make first differences
foreach var of varlist FFR-tfp_util PD {
	gen d_`var'=d.`var'
}

foreach var of varlist *err {
	gen l4_`var'=l4.`var'
	}

// predictability is much higher after the Volcker disinflation:
// commenting out the following line reduces significance in most places
drop if year(dofq(date))<1982
drop if year(dofq(date))>2012


// mean bias
mean *err

// univariate predictions
foreach var in Y I C U i piC piD {
	disp "Univariates for `var'"
	pwcorr `var'err PD d.PD `var'rev 
	newey `var'err PD, lag(1) 		// PD level
	newey `var'err d.PD, lag(1) 		// PD growth rate	
	newey `var'err `var'rev, lag(1) 	// Coibion-Gorodnichenko
	//reg `var'err PD, noheader			// PD level
	//reg `var'err d.PD, noheader			// PD growth rate	
	//reg `var'err `var'rev, noheader 	// Coibion-Gorodnichenko
}	
	
/*

// kitchen sink approach - throw in everything significant
foreach var of varlist *err {
	stepwise, pe(.05): reg `var' *rev d_* PD spread
	}
	
// more sophisticated - least angre regression
foreach var of varlist *err {
	lars `var' *rev d_* PD spread
	}


// what are the three or four joint best predictors for forecast errors?


foreach var in Y C I U piC piD i {
	reg `var'err Yrev PD d_IR	
	}
	
