use "jmulti-import.dta", replace
edit

tsset respi period
xtline irfPD_ cilPD_ ciuPD_ zeroline, byopts(yrescale ixaxes legend(off)) subtitle("") title(title) xtick(0(4)24) xlabel(0(4)24)
