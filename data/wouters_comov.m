%Wouter's COMOV statistics with bootstrapped confidence intervals

M=5*12; %maximum period for forecast errors
N=20; %number of bootstrap simulations
laglength=10; %VAR lag length
alpha=20; %size of confidence interval
method=2; %inference method

%select data
from_stata=importdata('covar_data.csv',',',1);

date=1959+((1:size(from_stata.data,1))-1)/12;
dateidx = (date >= 1959.2 ) & (date <= 1997.7 );
data=from_stata.data(dateidx,[1 4]);
datalabels=from_stata.textdata(1,[2 5])

%transformations
%data=log(data);
%data(:,3)=data(:,3)./data(:,2);

%dimensions
K=size(data,2);
T=size(data,1)-laglength; %n_obs adjusted for initial lags

%constants, trends...
exodata=[(1:T).^0; (1:T).^1; (1:T).^2]';
L=size(exodata,2);
exodata=reshape(kron(exodata',ones(K,1)),K,L,T);
exodata=squeeze(num2cell(exodata, [1 2]));

%% estimate a VAR
varx_spec = vgxset('n',K,'nAR',laglength,'nX',L);
varx_est = vgxvarx(varx_spec,data(laglength+(1:T),:),exodata,data(1:laglength,:));
resid_est = vgxinfer(varx_est,data(laglength+(1:T),:),exodata,data(1:laglength,:));

%% calculate Wouter's statistic
covar=zeros(K,K,M);

if method==1
    
    %generate forecasted paths
    forecast_res=reshape(repmat(resid_est,1,T),T,K,T).*reshape(repmat(triu(ones(T),1),K,1),T,K,T);
    forecasts=zeros(M,K,T);
    for t=0:(T-1)
        Mbar=min(M,T-t);
        forecasts(1:Mbar,:,t+1)=vgxproc(varx_est,zeros(Mbar,K),exodata(t+(1:Mbar)),data(1:(laglength+t),:));
    end

    %generate m-period ahead forecast errors and compute correlation
    for m=1:M
        m_forecasterror=zeros(T-m+1,K);
        for k=1:K
            m_forecasterror(:,k)=data(laglength+(m:T),k)-squeeze(forecasts(m,k,1:(T-m+1)));
        end
        covar(:,:,m)=corr(m_forecasterror);
    end
    
elseif method==2
    
    %transform into MA representation
    var_est=vgxset(varx_est,'nX',0,'b',[],'bsolve',[]);
    var_ma = vgxma(var_est,M-1);


    covar=zeros(K,K,M);
    covar(:,:,1)=var_ma.Q;
    for m=2:M
        covar(:,:,m)=covar(:,:,m-1)+var_ma.MA{m-1}'*var_ma.Q*var_ma.MA{m-1};
    end
    for m=1:M
        covar(:,:,m)=corrcov(covar(:,:,m));
    end
    
end

%% bootstrap
h=waitbar(0,'Please wait..');
covar_star=zeros(K,K,M,N);
for i=1:N
    resid_star=resid_est(randi(T,T,1),:);
    data_star=vgxproc(varx_est,resid_star,exodata);
    varx_est_star = vgxvarx(varx_spec,data_star,exodata);
    if method==1
        resid_est_star = vgxinfer(varx_est_star,data_star,exodata);
        forecast_res_star=reshape(repmat(resid_est_star,1,T),T,K,T).*reshape(repmat(triu(ones(T),1),K,1),T,K,T);
        forecasts_star=zeros(M,K,T);
        for t=0:(T-1)
            Mbar=min(M,T-t);
            forecasts_star(1:Mbar,:,t+1)=vgxproc(varx_est_star,zeros(Mbar,K),exodata(t+(1:Mbar)),data_star(1:t,:));
        end
        for m=1:M
            m_forecasterror_star=zeros(T-m+1,K);
            for k=1:K
                m_forecasterror_star(:,k)=data_star(m:T,k)-squeeze(forecasts_star(m,k,1:(T-m+1)));
            end
            covar_star(:,:,m,i)=corr(m_forecasterror_star);
        end
    elseif method==2
        var_est_star=vgxset(varx_est_star,'nX',0,'b',[],'bsolve',[]);
        var_ma_star = vgxma(var_est_star,M-1);
        covar_star_i=zeros(K,K,M);
        covar_star_i(:,:,1)=var_ma_star.Q;
        for m=2:M
            covar_star_i(:,:,m)=covar_star_i(:,:,m-1)+var_ma_star.MA{m-1}'*var_ma_star.Q*var_ma_star.MA{m-1};
        end
        for m=1:M
            covar_star_i(:,:,m)=corrcov(covar_star_i(:,:,m));
        end
        covar_star(:,:,:,i)=covar_star_i;
    end
    waitbar(i/N);
end
close(h);

%confidence intervals
covar_upper=prctile(covar_star,100-alpha/2,4);
covar_lower=prctile(covar_star,alpha/2,4);

%% plot
tscale=(1:M)/12;
for k=1:K
    for l=1:k
        subplot(K,K,(k-1)*K+l);
        %plot(tscale,squeeze(covar(k,l,:)),'r');
        plot(tscale,squeeze(covar(k,l,:)),'r',tscale,squeeze(covar_upper(k,l,:)),'k',tscale,squeeze(covar_lower(k,l,:)),'k');
        title([datalabels{k} ' / ' datalabels{l}]);
    end
end
