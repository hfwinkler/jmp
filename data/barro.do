cd  "C:\Users\winklerh\Dropbox\PhD\finfric\data\"
use "fred-investment.dta", clear
merge 1:1 date using "stuff1.dta"
gen year=year(dofm(date))
gen inv_real_nonres=inv_nominal_nonres / CPI
drop inv_nominal_nonres
rename inv_real_all inv_all
rename inv_real_nonres inv_nonres
replace PD=log(PD)
drop year
gen qdate=qofd(dofm(date))
collapse (mean) spread PD logp treasury* (firstnm) inv*, by(qdate)
rename qdate date
format %tqCCYY-!Qq date
drop if inv_all==.
tsset date

replace inv_all=log(inv_all)
replace inv_nonres = log(inv_nonres )
tsfilter hp logp_hp=logp, smooth(1600)
tsfilter hp inv_nonres_hp=inv_nonres, smooth(1600)
tsfilter hp spread_hp=spread, smooth(1600)

label variable logp_hp "S&P 500"
label variable inv_nonres_hp "Investment"
label variable spread_hp "Corporate spread"

reg inv_nonres_hp l.inv_nonres_hp l.logp_hp l.spread_hp
estimates store m1
esttab m1 using test.tex, cells(b(star fmt(%9.3f)) se(par))      stats(r2_a N, fmt(%9.3f %9.0g) labels(R-squared))     legend label collabels(none) varlabels(_cons Constant)
