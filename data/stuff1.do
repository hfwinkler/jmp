use "stuff1.dta", clear
gen return=log(p+d)-l.logp

tssmooth ma return1y=return , window(0 0 12)
replace return1y=12*return1y
tssmooth ma return4y=return , window(0 0 48)
replace return4y=48*return4y
tssmooth ma return10y=return , window(0 0 120)
replace return10y=120*return10y
'have to manually take out the smoothed values at the end of the sample


label var return4y "Return 4 years from now"
label var return1y "Return 1 year from now"
label var return10y "Return 10 years from now"
twoway (scatter return4y PD) (lfit return4y PD) /*
	*/ if return4y~=. , legend(off) ytitle("Return 4 years from now")

graph export "4yreturn-pd.eps", as(eps) preview(off) replace

twoway (tsline spread, yaxis(1)) (tsline PD, yaxis(2)), xlabel(,format(%tmCCYY))
graph export "pd-spread.eps", as(eps) preview(off) replace

