%play around with fitting AMN

from_stata=importdata('amn_data_q.csv',',',1);
from_stata.data=from_stata.data(1:size(from_stata.data,1)-1,:); %adjust for missing dividends data
tscale=from_stata.data(:,1);
p=from_stata.data(:,2);
d=from_stata.data(:,3);
e=from_stata.data(:,4);

%d=filter(ones(1,4)/4,1,d); %seasonal adjustment of e
d_d=filter([1 -1],1,d);
pd=p-d;

t0=4*10;
T=length(tscale); 

%parameters
gamma=0;
delta=0.9932;
a=log(1.02)/4;
s=0.1765;
g=0.001; %0.0045;

mu=zeros(size(p));
p_pred=zeros(size(p));

beta_RE=delta*exp(-gamma*a+(1-gamma)*s^2/2);

std(p_pred(t0:T)-d(t0:T))

%% exercise 1: get mu from price and dividend data, construct hypothetical prices
mubar=(1-gamma)*a;
mu(1)=mubar;
for t=2:length(tscale)
    p_pred(t)=a+log(beta_RE)+d(t)-log(1-delta*exp(mu(t-1))); %non-linear
    %p_pred(t)=d(t)+(1-gamma)*a+log(beta_RE)-log(1-delta*exp(mubar))+delta*exp(mubar)/(1-delta*exp(mubar))*(mu(t-1)-mubar); %linear
    mu(t)=(1-g)*mu(t-1)+g*(p(t)-p(t-1)-gamma*(d(t)-d(t-1)));
    if delta*exp(mu(t))>=1; mu(t)=mu(t-1); end %projection for non-linear
end
p_pred(1)=p_pred(2);

figure;
plot(tscale(t0:T),p_pred(t0:T)-d(t0:T),tscale(t0:T),p(t0:T)-d(t0:T));
legend('pd_{pred}','pd','Location','Best')
p_pred_hp=p_pred-hpfilter(p_pred,1600);
p_hp=p-hpfilter(p,1600);
figure;
plot(tscale(t0:T),p_pred_hp(t0:T),tscale(t0:T),p_hp(t0:T));
legend('p_{pred}','p','Location','Best')

std(p_pred(t0:T)-d(t0:T))

%% exercise 2: get mu only from dividend data, construct hypothetical prices
mubar=(1-gamma)*a;
mu(1)=mubar;
p_pred(1)=a+log(beta_RE)+d(t0)-log(1-delta*exp(mu(t0)));
for t=2:T
    p_pred(t)=a+log(beta_RE)+d(t)-log(1-delta*exp(mu(t-1))); %non-linear
    %p_pred(t)=d(t)+(1-gamma)*a+log(beta_RE)-log(1-delta*exp(mubar))+delta*exp(mubar)/(1-delta*exp(mubar))*(mu(t-1)-mubar); %linear
    mu(t)=(1-g)*mu(t-1)+g*(p_pred(t)-p_pred(t-1)-gamma*(d(t)-d(t-1)));
    if delta*exp(mu(t))>=1; mu(t)=mu(t-1); end %projection for non-linear case
end

figure;
plot(tscale(t0:T),p_pred(t0:T)-d(t0:T),tscale(t0:T),p(t0:T)-d(t0:T));
legend('pd_{pred}','pd','Location','Best')
p_pred_hp=p_pred-hpfilter(p_pred,1600);
p_hp=p-hpfilter(p,1600);
figure;
plot(tscale(t0:T),p_pred_hp(t0:T),tscale(t0:T),p_hp(t0:T));
legend('p_{pred}','p','Location','Best')

std(p_pred(t0:T)-d(t0:T))
