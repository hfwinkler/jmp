set more off

// import quarterly data

import excel "rbc-stats.xls", sheet("Quarterly") firstrow clear
drop if DATE==.
gen int qdate=qofd(DATE)
format %tq qdate
drop DATE
rename qdate date
order date
tsset date

save "rbc-stats.dta", replace

//import monthly data and aggregate
import excel "rbc-stats.xls", sheet("Monthly") firstrow clear
drop if DATE==.
gen int qdate=qofd(DATE)
format %tq qdate
drop DATE
rename qdate date

replace CPILFESL=log(CPILFESL)
replace CPIAUCSL=log(CPIAUCSL)
replace FEDFUNDS=log(1+FEDFUNDS/100)/4
replace TB3MS=log(1+TB3MS/100)/4
collapse (mean) CE16OV PAYEMS CPILFESL CPIAUCSL FEDFUNDS TB3MS, by(date)
replace CPILFESL=exp(CPILFESL)
replace CPIAUCSL=exp(CPIAUCSL)
replace FEDFUNDS=exp(FEDFUNDS)
replace TB3MS=exp(TB3MS)

order date
tsset date

// merge
merge 1:1 date using "rbc-stats.dta", keep(3) nogenerate
save "rbc-stats.dta", replace

// additional variables
gen IDEF=GPDI/GPDIC1
gen CDEF=PCEC/PCECC96

gen PNFIC1=PNFI/IDEF
gen PRFIC1=PRFI/IDEF

gen PCDGC1=PCDG/CDEF
gen PCNDC1=PCND/CDEF
gen PCESVC1=PCESV/CDEF

gen PCNDS=PCND+PCESV
gen PCNDSC1=PCNDS/CDEF

gen pi_CPIALL=CPIAUCSL/L.CPIAUCSL
gen pi_CPILFE=CPILFESL/L.CPILFESL
gen pi_DEFL=GDPDEF/L.GDPDEF

// take logs and filter

foreach var of varlist * {
	if "`var'"~="date" {
		replace `var'=log(`var')
		tsfilter hp `var'_hp=`var'
		label var `var'_hp "`var'"
		gen d_`var'=d.`var'
		label var d_`var' "`var'"
	}
}
order d_*, last


// select sample
drop if year(dofq(date))<1970
drop if year(dofq(date))>2009



// summary stats

tabstat d_*, statistics(mean sd count) columns(statistics)

tabstat *_hp, statistics(sd count) columns(statistics)
