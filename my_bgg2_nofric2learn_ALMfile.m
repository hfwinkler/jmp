function [ zidx, dphi ] = my_bgg2_nofric2learn_ALMfile( dr,M,options )
% this function provides the local derivatives of the equilibrium conditions
% which determine the boundedly rational forecast errors
%
% this function needs to be adapted by hand for every .mod file
% this function needs to be called FNAME_ALMfile.m
%
%


%fetch parameters
for i=1:length(M.params)
    eval([deblank(M.param_names(i,:)) '=M.params(' num2str(i) ');']);
end

%get indices of endovars and exovars
vari=struct;
varj=struct;
varjx=struct;
for i=1:M.exo_nbr
    vari.(deblank(M.exo_names(i,:)))=i;
end
for j=1:M.endo_nbr
    varj.(deblank(M.endo_names(dr.order_var(j),:)))=j;
end
j0=length(dr.state_var)-size(dr.ghx,2); %with order 3, we have to take the last nx variables in state_var!
for j=1:size(dr.ghx,2)
    varjx.(deblank(M.endo_names(dr.state_var(j+j0),:)))=j;
end

%set indices of pseudo forecast errors
zidx=[vari.pred_err, vari.pred_err1];

%derivatives
dphi=struct;

n=M.endo_nbr;
nx=size(dr.ghx,2);
nz=length(zidx);
nuz=M.exo_nbr;

%weight calculations
if cumdividend
    wD=G/R0/eqprem/PD;
    wV=1-G/R0/eqprem/PD;
else
    wD=1/PD;
    wV=1-1/PD;
end
if ~entrepreneurs
    [~,jMU]=ismember('MU',M.endo_names,'rows');
    wMU=1/dr.ys(jMU);
end

%% first order

dphi.y1=zeros(nz,n);
dphi.y=zeros(nz,n);
dphi.x=zeros(nz,nx);
dphi.uz=zeros(nz,nuz);

dphi.y(2,varj.lag_pred_err)=1;
dphi.uz(2,vari.pred_err1)=-1;

dphi.y(1,varj.V)=-1;    
if ~entrepreneurs
    dphi.y(1,varj.MU)=-wMU;
end
    
if cumdividend
    dphi.y(1,varj.D)=wD;
    dphi.y1(1,varj.V)=wV;
    if entrepreneurs
        dphi.y1(1,varj.sigeps1)=wV;
    else
        dphi.y1(1,varj.sigeps1)=wV*(1-theta);
        dphi.y1(1,varj.MU)=wV;
        dphi.y(1,varj.MU)=-wV;
    end
else
    dphi.y1(1,varj.D)=wD;
    dphi.y1(1,varj.V)=wV;
    if entrepreneurs
        dphi.y1(1,varj.sigeps1)=1;
    else
        dphi.y1(1,varj.sigeps1)=1-theta;
        dphi.y1(1,varj.MU)=wMU;
    end
end


%% second order
if options.order==1; return; end

dphi.y1y1=zeros(nz,n,n);
dphi.y1y=zeros(nz,n,n);
dphi.y1x=zeros(nz,n,nx);
dphi.y1uz=zeros(nz,n,nuz);

dphi.yy=zeros(nz,n,n);
dphi.yx=zeros(nz,n,nx);
dphi.yuz=zeros(nz,n,nuz);

dphi.xx=zeros(nz,nx,nx);
dphi.xuz=zeros(nz,nx,nuz);

dphi.uzuz=zeros(nz,nuz,nuz);

dphi.yy(1,varj.V,varj.V)=-1;
if ~entrepreneurs
    dphi.yy(1,varj.V,varj.MU)=-wMU;
end

if cumdividend
    dphi.yy(1,varj.D,varj.D)=wD;
    dphi.y1y1(1,varj.V,varj.V)=wV;
    if entrepreneurs
        dphi.y1y1(1,varj.V,varj.sigeps1)=wV;
        dphi.y1y1(1,varj.sigeps1,varj.sigeps1)=wV;
    else
        dphi.y1y1(1,varj.V,varj.sigeps1)=wV*(1-theta);
        dphi.y1y1(1,varj.V,varj.wMU)=wV*wMU;
        dphi.y1y1(1,varj.sigeps1,varj.sigeps1)=-theta*(1-theta)*wV;
        dphi.y1y1(1,varj.sigeps1,varj.wMU)=(1-theta)*wMU*wV;        
    end
else
    dphi.y1y1(1,varj.D,varj.D)=wD;
    dphi.y1y1(1,varj.V,varj.V)=wV;
    if entrepreneurs
        dphi.y1y1(1,varj.V,varj.sigeps1)=wV;
        dphi.y1y1(1,varj.D,varj.sigeps1)=wD;
        dphi.y1y1(1,varj.sigeps1,varj.sigeps1)=1;  
    else
        dphi.y1y1(1,varj.V,varj.sigeps1)=wV*(1-theta);
        dphi.y1y1(1,varj.V,varj.MU)=wV*wMU;
        dphi.y1y1(1,varj.D,varj.sigeps1)=wD*(1-theta);
        dphi.y1y1(1,varj.D,varj.MU)=wD*wMU;
        dphi.y1y1(1,varj.sigeps1,varj.sigeps1)=-theta*(1-theta);
        dphi.y1y1(1,varj.sigeps1,varj.MU)=(1-theta)*wMU;  
    end
end


%% third order
if options.order==2; return; end

dphi.y1y1y1=zeros(nz,n,n,n);
dphi.y1y1y=zeros(nz,n,n,n);
dphi.y1y1x=zeros(nz,n,n,nx);
dphi.y1y1uz=zeros(nz,n,n,nuz);
dphi.y1yy=zeros(nz,n,n,n);
dphi.y1yx=zeros(nz,n,n,nx);
dphi.y1yuz=zeros(nz,n,n,nuz);
dphi.y1xx=zeros(nz,n,nx,nx);
dphi.y1xuz=zeros(nz,n,nx,nuz);
dphi.y1uzuz=zeros(nz,n,nuz,nuz);

dphi.yyy=zeros(nz,n,n,n);
dphi.yyx=zeros(nz,n,n,nx);
dphi.yyuz=zeros(nz,n,n,nuz);
dphi.yxx=zeros(nz,n,nx,nx);
dphi.yxuz=zeros(nz,n,nx,nuz);
dphi.yuzuz=zeros(nz,n,nuz,nuz);

dphi.xxx=zeros(nz,nx,nx,nx);
dphi.xxuz=zeros(nz,nx,nx,nuz);
dphi.xuzuz=zeros(nz,nx,nuz,nuz);

dphi.uzuzuz=zeros(nz,nuz,nuz,nuz);

dphi.yyy(1,varj.V,varj.V,varj.V)=-1;
if ~entrepreneurs
    dphi.yyy(1,varj.V,varj.V,varj.MU)=-wMU;
end

if cumdividend
    dphi.yyy(1,varj.D,varj.D,varj.D)=wD;
    dphi.y1y1y1(1,varj.V,varj.V,varj.V)=wV;
    if entrepreneurs
        dphi.y1y1y1(1,varj.V,varj.V,varj.sigeps1)=wV;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.sigeps1)=wV;
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.sigeps1)=wV;
    else
        dphi.y1y1y1(1,varj.V,varj.V,varj.sigeps1)=wV*(1-theta);
        dphi.y1y1y1(1,varj.V,varj.V,varj.MU)=wV*wMU;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.sigeps1)=-theta*(1-theta)*wV;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.MU)=(1-theta)*wV*wMU;
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.sigeps1)=(theta+1)*theta*(1-theta)*wV;
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.MU)=-theta*(1-theta)*wV*wMU;
    end
else
    dphi.y1y1y1(1,varj.D,varj.D,varj.D)=wD;
    dphi.y1y1y1(1,varj.V,varj.V,varj.V)=wV;
    if entrepreneurs
        dphi.y1y1y1(1,varj.D,varj.D,varj.sigeps1)=wD;
        dphi.y1y1y1(1,varj.D,varj.sigeps1,varj.sigeps1)=wD;
        dphi.y1y1y1(1,varj.V,varj.V,varj.sigeps1)=wV;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.sigeps1)=wV;
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.sigeps1)=1;
    else
        dphi.y1y1y1(1,varj.D,varj.D,varj.sigeps1)=(1-theta)*wD;
        dphi.y1y1y1(1,varj.D,varj.D,varj.MU)=wD*wMU;
        dphi.y1y1y1(1,varj.D,varj.sigeps1,varj.sigeps1)=-theta*(1-theta)*wD;
        dphi.y1y1y1(1,varj.D,varj.sigeps1,varj.MU)=(1-theta)*wD*wMU;
        dphi.y1y1y1(1,varj.V,varj.V,varj.sigeps1)=(1-theta)*wV;
        dphi.y1y1y1(1,varj.V,varj.V,varj.MU)=wV*wMU;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.sigeps1)=-theta*(1-theta)*wV;
        dphi.y1y1y1(1,varj.V,varj.sigeps1,varj.MU)=(1-theta)*wV*wMU;
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.sigeps1)=(theta+1)*theta*(1-theta);
        dphi.y1y1y1(1,varj.sigeps1,varj.sigeps1,varj.MU)=-theta*(1-theta)*wMU;
    end
end


end

