% take the solutions of my_bgg2 and plot the respective IRFs

clear functions;
clear;

delete(gcp('nocreate'));
parpool;
warning off all;
pctRunOnAll warning off all;
    
%% USER: set models and output

mlist={'learn' 'RE'};
mnames={'learning' 'RE' 'no fin. fric., RE' 'no fin. fric., learning'};
styles={'r' 'b--' 'k-.' 'g:'};
%mlist={'learn','learnreal'};
%mnames={'learning','\kappa=\kappa_w=\psi=0'};
%mlist={'learn', 'linearlearn' 'learnKLAUS'};
%styles={'r' 'k--' 'b-.'};
%mnames={'CMCE', 'AL' 'mixed RE'};


widths=[2,2,2,2];

order=[2 2 2 2]; %order of simulation

%filtering method: 0=none, 1=HP, 2=FD, 3=CF, 4=Hamilton
filtmethod=1; 

%for use with linearlearn:
linearg=0.01; 
R0scale=1;

mcolumnstr='';
for mi=1:length(mlist)
    mcolumnstr=[mcolumnstr '\t' mlist{mi} ones(1,max(0,6-length(mlist{mi})))*' '];
end

%% parse parameter file to use in functions

dynare my_bgg2_parameters onlymacro savemacro;
fin = fopen('my_bgg2_parameters-macroexp.mod');
fout = fopen('my_bgg2_parameters.m', 'wt');

tline = fgetl(fin);
count = 0;
while ischar(tline)
    tline = fgetl(fin);
    if ischar(tline) && not(sum(strncmp(strtrim(tline),{'par','var','@#line'},3)));
        fprintf(fout, '%s\n', strtrim(tline));
    end
    count = count + 1;
end
fclose(fin);
fclose(fout);
delete('my_bgg2_parameters-macroexp.mod');

%% run Dynare, save results
M=struct;
oo=struct;

for mi=1:length(mlist)
    model=mlist{mi};
    save temp;
    eval(['dynare my_bgg2_' model ' nostrict;']);
    load temp;
    options_.noprint=1;
    options_.nomoments=1;
    options_.nocorr=1;
    options_.order=order(mi);
    options_.pruning=1;
    options_.irf=0;
    options_.ar=0;
    options_.hp_filter=0;
    options_.periods=0;
    if not(isfield(oo_.dr,'ghx')) || isempty(oo_.dr.ghx)
        error(['Dynare error in model ' model]);
    end
    %retrieve state variables
    k2 = oo_.dr.kstate(find(oo_.dr.kstate(:,2) <= M_.maximum_lag+1),[1 2]);
    k2 = k2(:,1)+(M_.maximum_lag+1-k2(:,2))*M_.endo_nbr;
    oo_.dr.state_var=oo_.dr.order_var(k2);
    %save model
    M.(model)=M_;
    oo.(model)=oo_;
    %welfare loss
    options_.periods=1e3;
    [~, welf.(model)]=my_bgg2_evalpol3([],[],0,1e3,M_, oo_, options_);
    oo.(model).endolag_nbr=sum(strncmp('AUX_ENDO_LAG',cellstr(M_.endo_names),12));
    clear M_ oo_;
    clear functions;
end
warning off all;

%trending variables
trendingvars={'N','D','ED','D','B','K','V','Y','C','I','w','Ce'};
if prefs==2
    trendingvars={trendingvars{:}, 'X_L'};
end


%% generate 1st order transition matrices

for mi=1:length(mlist)
    model=mlist{mi};
    C.(model)=zeros(length(oo.(model).dr.state_var),M.(model).endo_nbr);
    for i=1:length(oo.(model).dr.state_var)
        C.(model)(i,oo.(model).dr.state_var(i))=1;
    end
    A.(model)=oo.(model).dr.ghx(oo.(model).dr.inv_order_var,:)*C.(model);
    B.(model)=oo.(model).dr.ghu(oo.(model).dr.inv_order_var,:);
end


%% get IRFs, one for each shock

T=45;
replic=1e3;
Tpre=500; %burn-in
maxfh=4; %maximum forecast horizon for learning PLM

nexo=M.(mlist{1}).exo_nbr-2*strcmp(mlist{1},'learn');
stream = RandStream.getGlobalStream;
clear x xf;
maxx=zeros(1,nexo);
for i=1:nexo
    shocksize=1;
    for mi=1:length(mlist) 
        model=mlist{mi};
        [~,jidxV]=ismember('V',M.(model).endo_names,'rows');
        [~,jidxD]=ismember('D',M.(model).endo_names,'rows');
        [~,jidxRV]=ismember('RV',M.(model).endo_names,'rows');
        [~,jidxC]=ismember('C',M.(model).endo_names,'rows');
        [~,jidxCe]=ismember('Ce',M.(model).endo_names,'rows');
        [~,jidxCa]=ismember('C_all',M.(model).endo_names,'rows');
        %don't display shocks with zero variance
        maxx(i)=max(max(abs(oo.(model).dr.ghu(:,i))),maxx(i));
        if maxx(i)<1e-8; continue; end 
        if order(mi)==1 %first order is easy
            v=zeros(M.(model).exo_nbr,T);
            v(i,1)=1;
            if strcmp(model,'learn') && maxfh>0
                ys=oo.(model).dr.ys;
                [thisx,thisxf]=simult1(oo.(model).dr.ys,oo.(model).dr,v',1,M.(model),options_,oo.learn.drPLM,maxfh);
                %adjustment for Ce, C_all
                Sminus=ones(1,T+1);
                Snew=ones(1,T+1);
                for h=1:maxfh
                    Snew(thisxf(jidxRV,:,h)>0)=1;
                    Snew(thisxf(jidxRV,:,h)<0)=1;
                    thisxf(jidxCe,:,h)=thisxf(jidxCe,:,h) ...
                        + (Sminus-1).*exp(thisxf(jidxD,:,h)) ...
                        + (Sminus-Snew).*exp(thisxf(jidxV,:,h));
                    thisxf(jidxCa,:,h)=exp(ys(jidxC)-ys(jidxCa))*(thisxf(jidxC,:,h)-ys(jidxC))...
                                                +(1-exp(ys(jidxC)-ys(jidxCa)))*(thisxf(jidxCe,:,h)/ys(jidxCe)-1) ...
                                                + ys(jidxCa);
                    Sminus=Snew;
                end
                xf.learn(:,:,:,i)=100*(thisxf(:,2:T+1,2:maxfh+1)-repmat(oo.learn.dr.ys,[1 T maxfh]));
            else
                thisx=simult1(oo.(model).dr.ys,oo.(model).dr,v',1,M.(model),options_);
            end
            x.(model)(:,:,i)=100*(thisx(:,2:T+1)-repmat(oo.(model).dr.ys,1,T));
            %generate PDy, Dy
            Dyirf.(model)(:,i)=filter(ones(1,4),4,x.(model)(jidxD,:,i));
            PDyirf.(model)(:,i)=x.(model)(jidxV,:,i)'-Dyirf.(model)(:,i);
        else %in higher order, compute conditional IRFS at stochastic SS
            xii=zeros(M.(model).endo_nbr,T,replic);
            Dysim=zeros(T,replic);
            if strcmp(model,'learn') && maxfh>0; xfii=zeros(M.(model).endo_nbr,T,maxfh,replic); end
            parfor ii=1:replic
                v1=[randn(nexo,T+Tpre-1); zeros(M.(model).exo_nbr-nexo,T+Tpre-1)];
                v2=v1; v2(i,Tpre)=v2(i,Tpre)+shocksize;                
                if strcmp(model,'learn') && maxfh>0
                    ys=oo.(model).dr.ys;
                    [xii1,xfii1]=simult1(ys,oo.(model).dr,v1',order(mi),M.(model),options_,oo.learn.drPLM,maxfh);
                    [xii2,xfii2]=simult1(ys,oo.(model).dr,v2',order(mi),M.(model),options_,oo.learn.drPLM,maxfh);
%                    %adjustment for Ce, C_all
%                     for kk=1:2
%                         if kk==1; thisxfii=xfii1; else thisxfii=xfii2; end
%                         Sminus=ones(1,T+Tpre);
%                         Snew=ones(1,T+Tpre);
%                         for h=1:maxfh-1
%                             Snew(thisxfii(jidxRV,:,h+1)>0)=1;
%                             Snew(thisxfii(jidxRV,:,h+1)<0)=1;
%                             thisxfii(jidxCe,:,h)=thisxfii(jidxCe,:,h) ...
%                                 + (Sminus-1).*exp(thisxfii(jidxD,:,h)) ...
%                                 + (Sminus-Snew).*exp(thisxfii(jidxV,:,h));
%                             thisxfii(jidxCa,:,h)=exp(ys(jidxC)-ys(jidxCa))*(thisxfii(jidxC,:,h)-ys(jidxC)) ...
%                                                 +(1-exp(ys(jidxC)-ys(jidxCa)))*(thisxfii(jidxCe,:,h)/ys(jidxCe)-1) ...
%                                                 + ys(jidxCa);
%                             Sminus=Snew;
%                         end
%                         if kk==1; xfii1=thisxfii; else xfii2=thisxfii; end
%                    end
                    xfii(:,:,:,ii)=xfii2(:,Tpre+(1:T),2:maxfh+1)-xfii1(:,Tpre+(1:T),2:maxfh+1);
                elseif strcmp(model,'linearlearn')
                    xii1=simult1_linearlearn(oo.(model).dr.ys,oo.(model),v1',1,M.(model),options_,linearg,R0scale,0);
                    xii2=simult1_linearlearn(oo.(model).dr.ys,oo.(model),v2',1,M.(model),options_,linearg,R0scale,0);
                else
                    xii1=simult1(oo.(model).dr.ys,oo.(model).dr,v1',order(mi),M.(model),options_);
                    xii2=simult1(oo.(model).dr.ys,oo.(model).dr,v2',order(mi),M.(model),options_);
                end
                xii(:,:,ii) = xii2(:,Tpre+(1:T))-xii1(:,Tpre+(1:T));
                %generate Dy
                Dysim1 = log(filter(ones(1,4),1,exp(xii1(jidxD,:))));
                Dysim2 = log(filter(ones(1,4),1,exp(xii2(jidxD,:))));
                Dysim(:,ii)=Dysim2(:,Tpre+(1:T))-Dysim1(:,Tpre+(1:T));
            end
            x.(model)(:,:,i)=100*mean(xii,3);
            if strcmp(model,'learn') && maxfh>0; xf.learn(:,:,:,i)=100*mean(xfii,4); end
            %generate PDy, Dy
            Dyirf.(model)(:,i)=100*mean(Dysim,2);
            PDyirf.(model)(:,i)=100*mean(squeeze(xii(jidxV,:,:))-Dysim,2);
        end
        %apply trend for permanent TFP shock
        if i==1
            trend=100*sigmaA1*[0 ones(1,T-1)];
            for j=1:length(trendingvars)
                [~,jidx]=ismember(trendingvars{j},M.(model).endo_names,'rows');
                x.(model)(jidx,:,1)=x.(model)(jidx,:,1)+trend;
                if strcmp(model,'learn'); xf.(model)(jidx,:,:,1)=squeeze(xf.(model)(jidx,:,:,1))+repmat(trend',1,maxfh); end
            end
        end
        fprintf('.');
    end
end
fprintf('\n');

%% plot IRFS

%varnames={'V' 'Y' 'I' 'C'};
%vartitles={'Stock prices' 'Output' 'Investment' 'Consumption'};
%varnames={'V' 'Y' 'C' 'C_all' 'N' 'E' 'Dy'};
%vartitles={'Stock prices' 'Output' 'HH consumption' 'Total consumption' 'Net worth' 'Earnings' 'Dividends'};
%varnames={'Y' 'I' 'C' 'L' 'V' 'Dy' 'pi' 'i'};
%vartitles={'Output' 'Investment' 'Consumption' 'Employment' 'Stock prices' 'Dividends' 'Inflation' 'Nominal rate'};
varnames={'Y' 'V' 'Dy' 'PDy' 'R' 'RV'};
vartitles=varnames;
%varnames={'Y' 'I' 'C' 'V' 'w' 'R'};
%vartitles={'Output' 'Investment' 'Consumption' 'Stock prices' 'Real wage' 'Real rate'};
%varnames={'K' 'B' 'R' 'w' 'V' 'ED' 'Q' 'q'};
%vartitles=varnames;
Trange=0:40;
nrows=2;
irfscaler=[1 1 5 1];

clear jidx
set(0,'defaultfigurecolor',[1 1 1]);

for i=1:nexo
    if maxx(i)<1e-8; continue; end 
    if ~isunix
        figure('Units','normalized','Position',[.33 .33 .5 .5]);
    else
        figure('Units','normalized','Position',[0.7 0.4 0.13 0.21]);
    end
    set(gcf,'name',['IRF to shock ' M.(mlist{1}).exo_names(i,:)]);


    ncols=ceil(length(varnames)/nrows);
    for j=1:length(varnames)
        subplot(nrows,ncols,j);
        h=plot(1:T,zeros(1,T),'-k','LineWidth',.5);
        set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
        hold all;
        for mi=1:length(mlist)
            model=mlist{mi};
            if strcmp(varnames{j},'PDy')
                plot(Trange+1,PDyirf.(model)(Trange+1,i)*irfscaler(i),styles{mi},'LineWidth',widths(mi));   
            elseif strcmp(varnames{j},'Dy')
                plot(Trange+1,Dyirf.(model)(Trange+1,i)*irfscaler(i),styles{mi},'LineWidth',widths(mi));   
            elseif strcmp(model,'learnKLAUS') %something wrong with the first-period IRF here... need a better fix though
                [~,jidx.(model)]=ismember(varnames{j},M.(model).endo_names,'rows');
                plot(Trange+1,x.(model)(jidx.(model),Trange+2,i)*irfscaler(i),styles{mi},'LineWidth',widths(mi));    
            else
                [~,jidx.(model)]=ismember(varnames{j},M.(model).endo_names,'rows');
                plot(Trange+1,x.(model)(jidx.(model),Trange+1,i)*irfscaler(i),styles{mi},'LineWidth',widths(mi));   
            end
        end
        set(gca,'Xtick',min(Trange):8:max(Trange));
        xlim([min(Trange) max(Trange)]);
        title(vartitles{j});
        if j==length(varnames); legend(mnames,'Location','NorthEast'); end
    end
    set(findobj('color','g'),'color',[0 0.5 0]);
    print(gcf,['irf_' deblank(M.(mlist{1}).exo_names(i,:)) '.eps'],'-depsc2');
    fix_lines(['irf_' deblank(M.(mlist{1}).exo_names(i,:)) '.eps']);
end


%% show beliefs at some point (uses IRFs computed in previous section)

if any(strcmp('learn',mlist))

    T=40;
    varnames={'Y' 'I' 'C' 'RV'};
    vartitles=varnames; %{'Output' 'Stock price' 'C'  'I'};

    i=2;
    fh=3; %fh<=maxfh must hold
    %times at which to fork off the PLM
    Tbreak=[2 3 5 10 30];

    if any(strcmp('learn',mlist))
        %compute forecast errors
        ferr=zeros(M.learn.endo_nbr,T-fh);
        for s=1:T-fh
            ferr(:,s)=x.learn(:,s+fh,i)-xf.learn(:,s,fh,i);
        end
        %compute paths
        plmpath=NaN*ones(M.learn.endo_nbr,T,length(Tbreak));
        for j=1:length(Tbreak)
            plmpath(:,Tbreak(j),j)=x.learn(:,Tbreak(j),i);
            plmpath(:,Tbreak(j)+(1:fh),j)=squeeze(xf.learn(:,Tbreak(j),1:fh,i));
        end
        %plot paths
        h=figure;
        nrows=1;
        ncols=ceil(length(varnames)/nrows);
        for j=1:length(varnames)
            subplot(nrows,ncols,j);
            hold all;
            [yn,jidx]=ismember(varnames{j},M.learn.endo_names,'rows');
            plot(1:T,squeeze(x.learn(jidx,1:T,i))','r','LineWidth',2);
            hold on;
            plot(1:T,squeeze(plmpath(jidx,1:T,:))','b','LineWidth',1);
            hold on;
            plot(1:T,zeros(1,T),'k');
            title(vartitles{j});
            axis tight;
            set(gca,'Xtick',0:4:T);
        end
        %tightfig(h);

        %plot forecast errors
        figure;
        nrows=2;
        ncols=ceil(length(varnames)/nrows);
        for j=1:length(varnames)
            subplot(nrows,ncols,j);
            hold all;
            [yn,jidx]=ismember(varnames{j},M.learn.endo_names,'rows');
            plot(2:T-fh,ferr(jidx,2:T-fh));
            title(varnames{j});
        end
    end

    drawnow;

end


%% series for export into CSV, including expectations

varnames={'A2' 'Y' 'C' 'I' 'L' 'pi' 'i' 'D' 'E' 'V' 'RV' 'R'};
T=2e4;
Tpre=500;
fh=5;

disp('Simulation and csv export...');
nexo=M.(mlist{1}).exo_nbr-2*strcmp(mlist{1},'learn');
J=length(varnames);
fspecs=[repmat('%s,',1,(fh+1)*J-1) '%s\n'];
fspecf=[repmat('%f,',1,(fh+1)*J-1) '%f\n'];

for mj=1:length(mlist)
    model=mlist{mj};
    [~,jidxV]=ismember('V',M.(model).endo_names,'rows');
    [~,jidxD]=ismember('D',M.(model).endo_names,'rows');
    [~,jidxRV]=ismember('RV',M.(model).endo_names,'rows');
    [~,jidxC]=ismember('C',M.(model).endo_names,'rows');
    [~,jidxCe]=ismember('Ce',M.(model).endo_names,'rows');
    [~,jidxCa]=ismember('C_all',M.(model).endo_names,'rows');
    
    %simulate
    stream = RandStream.getGlobalStream;
    reset(stream,0);
    v=sqrt(M.(model).Sigma_e)*[randn(nexo,T+Tpre-1); zeros(M.(model).exo_nbr-nexo,T+Tpre-1)];
    if strcmp(model,'learn');
        [x.learn,xf.learn]=simult1(oo.learn.dr.ys,oo.learn.dr,v',order(mi),M.learn,options_,oo.learn.drPLM,fh);
        %adjustment for Ce, C_all
        Sminus=ones(1,T+Tpre);
        Snew=ones(1,T+Tpre);
        for h=1:fh
            Snew(xf.learn(jidxRV,:,h)>0)=2;
            Snew(xf.learn(jidxRV,:,h)<0)=0;
            xf.learn(jidxCe,:,h)=xf.learn(jidxCe,:,h) ...
                + (Sminus-1).*exp(xf.learn(jidxD,:,h)) ...
                + (Sminus-Snew).*exp(xf.learn(jidxV,:,h));
            xf.learn(jidxCa,:,h)=log(exp(xf.learn(jidxC,:,h))+xf.learn(jidxCe,:,h));
            Sminus=Snew;
        end
    elseif strcmp(model,'linearlearn');
        [x.(model),xf.(model),simulflag]=simult1_linearlearn(oo.(model).dr.ys,oo.(model),v',order(mi),M.(model),options_,linearg,R0scale,fh);
    else
        [x.(model),xf.(model)]=simult1(oo.(model).dr.ys,oo.(model).dr,v',order(mi),M.(model),options_,oo.(model).dr,fh);
    end
    
    %cut presample
    x.(model)=x.(model)(:,Tpre+(1:T));
    xf.(model)=xf.(model)(:,Tpre+(1:T),2:fh+1);
    
    %apply trend
    trend=sigmaA1*[0 cumsum(v(1,Tpre+(1:T-1)),2)];
    for j=1:length(trendingvars)
        [~,jidx]=ismember(trendingvars{j},M.(model).endo_names,'rows');
        if jidx>0
            x.(model)(jidx,:)=x.(model)(jidx,:)+trend;
            xf.(model)(jidx,:,:)=xf.(model)(jidx,:,:)+repmat(trend,[1 1 fh]);
        end
    end


    %get indices of varnames
    C=zeros(J,M.(model).endo_nbr);
    for j=1:length(varnames)
        [~,jidx]=ismember(varnames{j},M.(model).endo_names,'rows');
        C(j,jidx)=1;
    end
    
    %fill spreadsheet
    mydata=zeros((1+fh)*J,T);
    mynames=[repmat(char(varnames),1+fh,1) ones(J*(1+fh),1)*' '];
    mydata(1:J,:)=C*x.(model);
    for i=1:fh
        mydata(i*J+(1:J),:)=C*xf.(model)(:,:,i);
        mynames(i*J+(1:J),:)=strtrim([strjust(mynames(i*J+(1:J),:),'right') ones(J,1)*num2str(i)]);
    end

    mynames=cellstr(mynames);
   

    % save to CSV
    fout = fopen( ['my_bgg2_sim' model '.csv'], 'wt' );
    fprintf( fout, fspecs, mynames{:});
    fprintf( fout, fspecf, mydata);
    fclose(fout);
    
    disp(model);
end

%% moments (uses simulations computed in previous section)

fprintf('\nMoments:\n');

clear jY jD

for mi=1:length(mlist)
    model=mlist{mi};
    nendo=M.(model).endo_nbr;
    [~,jY.(model)]=ismember('Y',M.(model).endo_names,'rows');
    %replace D with Dy
    [~,jD]=ismember('D',M.(model).endo_names,'rows');
    x.(model)(jD,:)=log(filter(ones(1,4),4,exp(x.(model)(jD,:))));
    
    switch filtmethod
        case 1 %for HP filtering
            V.(model)=cov(x.(model)'-hpfilter(x.(model),1600));
        case 3 %for CF filtering
            V.(model)=cov(bpass(x.(model)',6,32));
        case 2 %for FD filtering
            V.(model)=cov(diff(x.(model)'));
        case 0 %no filtering
            V.(model)=cov(x.(model)');
        case 4 %for Hamilton
            Trange=12:T;
            xham=zeros(size(x.(model),1),T-11);
            for i=1:size(x.(model),1)
                Xham=[x.(model)(i,Trange-8)', x.(model)(i,Trange-9)', ...
                      x.(model)(i,Trange-10)', x.(model)(i,Trange-11)', ...
                      ones(T-11,1)];
                Yham=x.(model)(i,Trange)';
                xham(i,:)=Yham-Xham*((Xham'*Xham)\(Xham'*Yham));
            end
            V.(model)=cov(xham');
    end
    
    %normalise: diagonal to get stddev, off diagonal to get correlation
    Stddiag=sqrt(diag(V.(model)));
    V.(model)=V.(model)./kron(Stddiag,ones(1,nendo))./kron(Stddiag',ones(nendo,1));
    V.(model)=V.(model)-eye(nendo)+diag(Stddiag);

end

fprintf('\nStddev(Y):\n');
for mi=1:length(mlist)
    model=mlist{mi};
    fprintf('%8s: \t%4.3f\n',model,V.(model)(jY.(model),jY.(model))*100);
end

if isfield(M,'RE') && isfield(M,'learn'); 
    maxj=M.RE.endo_nbr-oo.learn.endolag_nbr;
else
    maxj=M.(mlist{1}).endo_nbr;
end

fprintf(['\nStddev/output:' mcolumnstr]);
for j=1:maxj
    fprintf('\n\t%8s:',deblank(M.(mlist{1}).endo_names(j,:)));
    for mi=1:length(mlist)
        model=mlist{mi};
        [yn,jidx]=ismember(M.(mlist{1}).endo_names(j,:),M.(model).endo_names,'rows');
        stddev=V.(model)(jidx,jidx)/V.(model)(jY.(model),jY.(model));
        fprintf('\t%4.3f',stddev);
    end
end
disp('');

fprintf(['\nCorr w/ output:' mcolumnstr]);
for j=1:maxj
    fprintf('\n\t%8s:',deblank(M.(mlist{1}).endo_names(j,:)));
    for mi=1:length(mlist)
        model=mlist{mi};
        [~,jidx]=ismember(M.(mlist{1}).endo_names(j,:),M.(model).endo_names,'rows');
        ycorr=V.(model)(jidx,jY.(model));
        fprintf('\t%4.3f',ycorr);
    end
end
fprintf('\n\n');

% %% plot time series (uses simulations computed in previous section)
% 
% if any(strcmp('learn',mlist)) && any(strcmp('RE',mlist))
%         
%     Trange=100:1000;
% 
%     [~,jY]=ismember('Y',M.(mlist{1}).endo_names,'rows');
%     [~,jV]=ismember('V',M.(mlist{1}).endo_names,'rows');
% 
%     figure;
%     h=plot( Trange-Trange(1),(x.RE(jY,Trange)+trend(Trange)-x.RE(jY,Trange(1))),'k', ...
%             Trange-Trange(1),(x.learn(jY,Trange)+trend(Trange)-x.learn(jY,Trange(1))),'r', ...
%             Trange-Trange(1),(x.learn(jV,Trange)+trend(Trange)-x.learn(jV,Trange(1))), 'b:', ...
%          'LineWidth',1.5);
%     axis tight; legend('Y_{RE}','Y_{learn}','V_{learn}','Location','Best');
%     axlims=axis;
%     axlims=round(10*axlims)'/10;
%     hold on;
%     plot(kron((axlims(3):.1:axlims(4)),ones(length(Trange),1)),':','Color',.66*ones(1,3));
%     uistack(h,'top');
% 
% end
% 
% %% forecasts and realisations conditional on state (uses simulations computed in previous section)
% 
% if any(strcmp('learn',mlist))
%     p=0.9; %quantile of PD ratio
%     [~,jidx]=ismember('RV',M.learn.endo_names,'rows'); %variable to plot
%     %compute PD ratio
%     [~,jV]=ismember('V',M.learn.endo_names,'rows');
%     [~,jD]=ismember('D',M.learn.endo_names,'rows');
%     D=exp(x.learn(jD,:));
%     PD=x.learn(jV,:)-log(filter(ones(1,4)/4,1,D));
%     %first difference
%     %PD=filter([1 -1], 1,PD);
%     %get time indices
%     PDhigh=quantile(PD,p);
%     Thigh=find(PD>PDhigh & 1:T<T-fh);
%     %compute forecasts and realisations
%     realisation=zeros(M.learn.endo_nbr,fh+1,3);
%     forecast=zeros(M.learn.endo_nbr,fh+1,3);
%     for j=1:fh
%         realisation(:,j+1,1)=mean(x.learn(:,Thigh+j)-x.learn(:,Thigh),2);
%         realisation(:,j+1,2)=-quantile(x.learn(:,Thigh+j)-x.learn(:,Thigh),0.1,2)+realisation(:,j+1,1);
%         realisation(:,j+1,3)=quantile(x.learn(:,Thigh+j)-x.learn(:,Thigh),0.9,2)-realisation(:,j+1,1);
%         forecast(:,j+1,1)=mean(xf.learn(:,Thigh,j)-x.learn(:,Thigh),2);
%         forecast(:,j+1,2)=-quantile(xf.learn(:,Thigh,j)-x.learn(:,Thigh),0.1,2)+forecast(:,j+1,1);       
%         forecast(:,j+1,3)=quantile(xf.learn(:,Thigh,j)-x.learn(:,Thigh),0.9,2)-forecast(:,j+1,1);
%     end
%     %plot
%     figure; title(['PD>' num2str(p) ' quantile']);
%     boundedline(0:fh,forecast(jidx,:,1),squeeze(forecast(jidx,:,2:3)),'r',0:fh,realisation(jidx,:,1),squeeze(realisation(jidx,:,2:3)),'b','alpha');
%     legend('forecast','realisation');
%     hold on;
%     plot(0:fh,forecast(jidx,:,1),'r',0:fh,realisation(jidx,:,1),'b','LineWidth',1.5);
% end
% 
% %% welfare
% 
% fprintf('\nUtility:\t%%C    \t%%Ce    \t%%Ca    \n');
% for mi=1:length(mlist)
%     model=mlist{mi};
%     fprintf('%8s:\t%4.3g\t%4.3g\t%4.3g\n',model,welf.(model).Closs,welf.(model).Celoss,welf.(model).Caloss);
% end
% fprintf('\n');

%% clean up

for mi=1:length(mlist)
    model=mlist{mi};
    cleanup_dyn(['my_bgg2_' model]);
end
