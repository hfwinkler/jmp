%A variant of BGG with limited commitment
%RE solution

%load parameters from file:
@#include "my_bgg2_parameters.mod"




model;

@#include "my_bgg2_common.mod"

%entrepreneurs
@#if entrepreneurs
    #lambdae=1/R0/eqprem;
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E)-psi/2*(exp(I-I(-1)+sigeps1)/(1-kappad/nu*(exp(nu*u)-1))-1)^2;
@#else
    #lambdae=lambda;
    %#lambdae=1/R0;
    Ce=0;
@#endif

%firm side
exp(Rk)=(exp((1-markuptofirms)*q+Y)-exp(w+L))/exp(K(-1)-log(G)-sigeps1)+exp(Qd)*(1-kappad/nu*(exp(nu*u)-1));
exp(K+Q)=(1-gama+gama*omega)*(exp(N)-c*exp(E))+exp(B);
exp(w)=(1-alphaa)*exp(q+Y-L);
@#if cumdividend
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(lambdae*G*exp(V(+1)+sigeps1(+1))+exp(B)));
@#else    
exp(B)=xi*((1-xr)*lambda*exp(Q(+1)+K)+xr*(exp(V)+exp(B)));
@#endif
exp(N)=( exp(Rk+K(-1))-exp(R+B(-1)) ) / (G*exp(sigeps1));
exp(D)=gama*exp(N)+(1-gama)*c*exp(E)-1*(exp(q)-1)*(0*gama+1*(1-gama)*c)*exp(Y);
%exp(D)=gama*exp(N)+(1-gama)*c*exp(E);

%capacity choice and adjustment costs
@#if varutil
    kappad*exp(nu*u)*exp(Qd+K(-1)-log(G)-sigeps1)=alphaa*exp(q+Y);
@#else    
    u=0;
@#endif

%asset pricing
@#if cumdividend
    exp(V)=exp(D)+G*lambdae*(1-gama)/(1-gama+gama*omega)*exp(V(+1)+sigeps1(+1));
    exp(RV)=G*exp(V-sigeps1)/(exp(V(-1))-exp(D(-1)))*(1-gama)/(1-gama+gama*omega)/R0;
@#else
    exp(V)=G*lambdae*(exp(D(+1)+sigeps1(+1))+(1-gama)/(1-gama+gama*omega)*exp(V(+1)+sigeps1(+1)));
    exp(RV)=G*(exp(D+sigeps1-V(-1))+(1-gama)/(1-gama+gama*omega)*exp(V+sigeps1-V(-1)))/R0;
@#endif

%learning equivalents
mu=V(+1)+sigeps1(+1)-V;

end;

steady_state_model;

Rk=log(Rk0);

@#include "my_bgg2_commonSS.mod"

@#if entrepreneurs
    Ce=gama*(1-omega)*exp(N)+(1-gama+gama*omega)*c*exp(E);
@#else
    Ce=0;
@#endif
Welfaree=Ce;
C_all=log(exp(C)+Ce);

end;

@#include "my_bgg2_commonend.mod"


