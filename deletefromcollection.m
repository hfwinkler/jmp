function [ ] = deletefromcollection( collectionname, policyname )
%DELETEFROMCOLLECTION( collectionname, policyname )
%deletes policy from collection

load([collectionname '.mat']); 

policies=rmfield(policies,policyname);

save([collectionname '.mat'],'policies');

fprintf('\nThe file ''%s.mat'' now contains the following policies:\n',collectionname);
disp(fieldnames(policies));

end

