#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\begin_modules
theorems-ams
\end_modules
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 2.5cm
\rightmargin 2.5cm
\bottommargin 2.5cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
The objective of this note is to define as precisely as possible the equilibrium
 concept used with asset price learning when beliefs about some endogenous
 variables are 
\begin_inset Quotes eld
\end_inset

almost consistent
\begin_inset Quotes erd
\end_inset

 - a new concept relative to AMN.
\end_layout

\begin_layout Subsection*
General setup
\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $\left(\Omega,\sigma_{0},\mathcal{P}_{0}\right)$
\end_inset

 be a probability space and 
\begin_inset Formula $\left(u_{t}\right)_{t\in\mathbb{N}}$
\end_inset

 an 
\begin_inset Formula $\mathbb{R}^{n}$
\end_inset

-valued discrete-time stochastic process on 
\begin_inset Formula $\left(\Omega,\sigma,\mathcal{P}_{0}\right)$
\end_inset

.
 This stochastic process collects the 
\emph on
exogenous variables
\emph default
 of the model economy.
\end_layout

\begin_layout Standard
I will set up a purely competitive economy in which agents' actions depend
 only on the prices of goods, but these restrictions can be relaxed easily.
 The economy consists of a set 
\begin_inset Formula $K$
\end_inset

 of goods traded in competitive spot markets at each point in time.
 The 
\emph on
price
\emph default
 of good 
\begin_inset Formula $k\in K$
\end_inset

 at time 
\begin_inset Formula $t$
\end_inset

 is 
\begin_inset Formula $p_{kt}$
\end_inset

.
 I also assume the existence of a numeraire good 
\begin_inset Formula $k_{0}$
\end_inset

 for which 
\begin_inset Formula $p_{k_{0}t}=1$
\end_inset

.
 The goods are traded by a set 
\begin_inset Formula $I$
\end_inset

 of agents.
 The 
\emph on
excess demand
\emph default
 of agent 
\begin_inset Formula $i$
\end_inset

 for good 
\begin_inset Formula $k$
\end_inset

 at time 
\begin_inset Formula $t$
\end_inset

 is 
\begin_inset Formula $x_{kti}$
\end_inset

.
 On top of this, I also denote by 
\begin_inset Formula $y_{jt},j\in J$
\end_inset

 a number of relevant real-valued 
\emph on
actions 
\emph default
that agents can take each period, and the subset of actions taken by agent
 
\begin_inset Formula $i$
\end_inset

 as 
\begin_inset Formula $y_{ti}\subset\left\{ y_{jt},j\in J\right\} $
\end_inset

.
\end_layout

\begin_layout Standard
One usually describes prices and actions as stochastic processes which are
 measurable with respect to the exogenous process 
\begin_inset Formula $u_{t}$
\end_inset

.
 I will do the same in equilibrium, but not when agents are forming expectations
 about future outcomes.
 Doing so already implies a high degree of knowledge on behalf of agents
 when defining their optimisation problem, namely that they understand that
 prices and other outcomes will only depend on 
\begin_inset Formula $u_{t}$
\end_inset

 because of the equilibrium concept ultimately imposed.
 To keep the definition more general, I assume that beliefs about prices
 
\begin_inset Formula $p_{kt}^{e}$
\end_inset

 and actions 
\begin_inset Formula $y_{t}^{e}$
\end_inset

 are stochastic processes defined on the measure space 
\begin_inset Formula $\left(\mathbb{R}^{\left(K\cup J\right)\times\mathbb{N}},\sigma\left(\mathbb{R}^{K\times\mathbb{N}}\right)\right)$
\end_inset

.
 Denote the product space of exogenous variables, prices and actions by
 
\begin_inset Formula $\left(\tilde{\Omega},\tilde{\sigma}\right)=\left(\Omega\times\mathbb{R}^{\left(K\cup J\right)\times\mathbb{N}},\sigma_{0}\otimes\sigma\left(\mathbb{R}^{\left(K\cup J\right)\times\mathbb{N}}\right)\right)$
\end_inset

.
\end_layout

\begin_layout Standard
Each agent is assumed to know the distribution of the exogenous process
 
\begin_inset Formula $u_{t}$
\end_inset

, but can entertain arbitrary subjective beliefs about prices and actions.
 This motivates the following defintion.
\end_layout

\begin_layout Definition
A subjective belief about prices and actions is a probability measure 
\begin_inset Formula $\mathcal{P}_{i}$
\end_inset

 on 
\begin_inset Formula $\left(\tilde{\Omega},\tilde{\sigma}\right)$
\end_inset

 such that there exists a Markov kernel 
\begin_inset Formula $\kappa_{i}:\,\Omega\times\sigma\left(\mathbb{R}^{\left(K\cup J\right)\times\mathbb{N}}\right)\rightarrow\left[0,1\right]$
\end_inset

 with 
\begin_inset Formula $\mathcal{P}_{i}=\mathcal{P}_{0}\otimes\kappa_{i}$
\end_inset

.
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
For a probability space 
\begin_inset Formula $\left(\Omega_{1},\sigma_{1},\mathcal{P}_{1}\right)$
\end_inset

 and a measure space 
\begin_inset Formula $\left(\Omega_{2},\sigma_{2}\right)$
\end_inset

, a function 
\begin_inset Formula $\kappa:\,\Omega_{1}\times\sigma_{2}\rightarrow\left[0,1\right]$
\end_inset

 is a Markov kernel if 
\begin_inset Formula $\omega_{1}\mapsto\kappa\left(\omega_{1},\mathcal{A}_{2}\right)$
\end_inset

 is 
\begin_inset Formula $\sigma_{1}$
\end_inset

-measurable for all 
\begin_inset Formula $\mathcal{A}_{2}\in\sigma_{2}$
\end_inset

 and 
\begin_inset Formula $\mathcal{A}_{2}\mapsto\kappa\left(\omega_{1},\mathcal{A}_{2}\right)$
\end_inset

 is a probability measure for all 
\begin_inset Formula $\omega_{1}$
\end_inset

.
 The product probability measure 
\begin_inset Formula $\mathcal{P}\otimes\kappa$
\end_inset

 is defined as the measure on the product space 
\begin_inset Formula $\left(\Omega_{1}\times\Omega_{2},\sigma_{1}\otimes\sigma_{2}\right)$
\end_inset

 for which 
\begin_inset Formula $\mathcal{P}\otimes\kappa\left(\mathcal{A}_{1}\right)=\mathcal{P}\left(\mathcal{A}_{1}\right)$
\end_inset

 and 
\begin_inset Formula $\mathcal{P}\otimes\kappa\left(\mathcal{A}_{2}\mid\omega_{1}\right)=\kappa\left(\omega_{1},\mathcal{A}_{2}\right)$
\end_inset

 .
\end_layout

\end_inset

 
\end_layout

\begin_layout Definition
Further, every agent is assumed to observe fundamentals, prices and actions,
 and so his information set is described by the
\emph on
 filtration
\emph default
 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

 induced by the processes 
\begin_inset Formula $u_{t}$
\end_inset

, 
\begin_inset Formula $p_{kt}^{e}$
\end_inset

 and 
\begin_inset Formula $y_{t}^{e}$
\end_inset

.
\end_layout

\begin_layout Standard
The next step is to carefully describe an agent's optimisation problem given
 the information and belief structure.
 Each agent 
\begin_inset Formula $i$
\end_inset

 needs to choose a stochastic process 
\begin_inset Formula $x_{kti}$
\end_inset

 on the measure space 
\begin_inset Formula $\left(\tilde{\Omega},\tilde{\sigma}\right)$
\end_inset

which solves an infinite-horizon, recursive discounted utility problem of
 the form:
\begin_inset Formula 
\begin{equation}
\max_{x_{kti,}y_{ti}}E^{\mathcal{P}_{i}}\left[\sum_{t=0}^{\infty}\beta_{i}^{s}U_{i}\left(x_{kti},y_{ti},y_{jt}^{e}\right)\right]\label{eq:problem}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{eqnarray*}
\text{such that }\text{x}_{kti}\text{ is }\mathcal{F}_{t}\text{-measurable}\\
\text{and }\mbox{\mathcal{P}}_{i}\text{-almost surely: }G_{i}\left(x_{kti},p_{kt}^{e},y_{ti},y_{jt}^{e}\right) & = & 0
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
With this fairly general structure of an economy, one can now proceed to
 define various forms of equilibria.
 I start with the rational expectations equilibrium, which is the most restricti
ve.
\end_layout

\begin_layout Subsection*
Rational expectations equilibrium
\end_layout

\begin_layout Standard
The rational expectations hypothesis as described by Muth (1961) states
 that agent's beliefs about conditional distributions of model variables
 coincides with the actual conditional distribution in a stationary environment.
 In the setup of this note, the hypothesis translates into the following
 definition.
\end_layout

\begin_layout Definition
A 
\emph on
rational expectations equilibrium 
\emph default
consists of a pricing function 
\begin_inset Formula $f_{p}:\,\Omega\rightarrow\mathbb{R}^{K\times\mathbb{N}}$
\end_inset

, an action function 
\begin_inset Formula $f_{y}:\,\Omega\rightarrow\mathbb{R}^{J\times\mathbb{N}}$
\end_inset

 and 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

-measurable allocations 
\begin_inset Formula $\left(x_{kti}^{\ast}\right)$
\end_inset

 on 
\begin_inset Formula $\left(\Omega,\sigma,\mathcal{P}_{0}\right)$
\end_inset

 such that
\end_layout

\begin_deeper
\begin_layout Enumerate
The price process defined by the pricing function 
\begin_inset Formula $p_{kt}^{\ast}:\,\omega\mapsto f_{p}\left(\omega\right)$
\end_inset

 and the actions process 
\begin_inset Formula $y_{jt}^{\ast}:\,\omega\mapsto f_{y}\left(\omega\right)$
\end_inset

 are 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

-measurable;
\end_layout

\begin_layout Enumerate
for each agent 
\begin_inset Formula $i\in I$
\end_inset

, the processes 
\begin_inset Formula $\left(x_{kti}^{\ast},y_{ti}^{\ast}\right)$
\end_inset

 are a solution to the decision problem 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:problem"

\end_inset

 under the belief measure 
\begin_inset Formula $\mathcal{P}^{\ast}=\mathcal{P}_{0}\otimes\kappa^{\ast}$
\end_inset

 where 
\begin_inset Formula 
\[
\kappa^{\ast}\left(\omega,\mathcal{A}\right)=\begin{cases}
1 & \text{ if }f\left(\omega\right)\in\mathcal{A}\\
0 & \text{ otherwise}
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Enumerate
all competitive markets clear: 
\begin_inset Formula $\forall k\forall t:\,\sum_{i}x_{kti}^{\ast}\leq0$
\end_inset

 almost surely with respect to 
\begin_inset Formula $\mathcal{P}_{0}$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Subsection*
Internal rationality (Adam, Marcet & Nicolini 2011)
\end_layout

\begin_layout Definition
An 
\emph on
internally rational equilibrium 
\emph default
consists of a pricing function 
\begin_inset Formula $f_{p}:\,\Omega\rightarrow\mathbb{R}^{K\times\mathbb{N}}$
\end_inset

, an action function 
\begin_inset Formula $f_{y}:\,\Omega\rightarrow\mathbb{R}^{J\times\mathbb{N}}$
\end_inset

, 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

-measurable allocations 
\begin_inset Formula $\left(x_{kti}^{\ast}\right)$
\end_inset

 on 
\begin_inset Formula $\left(\Omega,\sigma,\mathcal{P}_{0}\right)$
\end_inset

 and a collection of subjective probability measures 
\begin_inset Formula $\mathcal{P}_{i}$
\end_inset

 such that
\end_layout

\begin_deeper
\begin_layout Enumerate
The price process defined by the pricing function 
\begin_inset Formula $p_{kt}^{\ast}:\,\omega\mapsto f_{p}\left(\omega\right)$
\end_inset

 and the actions process 
\begin_inset Formula $y_{jt}^{\ast}:\,\omega\mapsto f_{y}\left(\omega\right)$
\end_inset

 are 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

-measurable;
\end_layout

\begin_layout Enumerate
all competitive markets clear: 
\begin_inset Formula $\forall k\forall t:\,\sum_{i}x_{kti}^{\ast}\leq0$
\end_inset

 almost surely with respect to 
\begin_inset Formula $\mathcal{P}_{0}$
\end_inset

;
\end_layout

\begin_layout Enumerate
for each agent 
\begin_inset Formula $i\in I$
\end_inset

, there exists a solution 
\begin_inset Formula $\left(x_{kti},y_{ti}\right)$
\end_inset

 to the decision problem 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:problem"

\end_inset

 under the belief measure 
\begin_inset Formula $\mathcal{P}_{i}$
\end_inset

 which satisfies 
\begin_inset Formula $x_{kti}\left(\omega,f\left(\omega\right)\right)=x_{kti}^{\ast}\left(\omega\right)$
\end_inset

 and 
\begin_inset Formula $y_{ti}\left(\omega,f\left(\omega\right)\right)=y_{jt}^{\ast}\left(\omega\right)$
\end_inset

 for 
\begin_inset Formula $\mathcal{P}_{0}$
\end_inset

-almost all 
\begin_inset Formula $\omega\in\Omega$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Standard
In particular, agents' subjective beliefs about prices are such that they
 anticipate to choose allocations for which markets do not clear in some
 future states of the world.
 But in equilibrium, these states are never reached.
\end_layout

\begin_layout Subsection*
Special case: almost-consistent beliefs
\end_layout

\begin_layout Standard
In my paper, I make further special assumptions about the shape of subjective
 beliefs which limit the degrees of freedom in models with many different
 prices.
 
\end_layout

\begin_layout Standard
I only allow for arbitrary specification of subjective beliefs about the
 price of a single good 
\begin_inset Formula $k_{1}\in K$
\end_inset

.
 In my paper, this is the price of equities.
 The beliefs about all other prices will be determined by consistency with
 equilibrium prices, up to market clearing in two goods: the good with subjectiv
e price beliefs 
\begin_inset Formula $k_{1}$
\end_inset

 and the numeraire 
\begin_inset Formula $k_{0}$
\end_inset

.
\end_layout

\begin_layout Definition
An 
\emph on
almost-consistent belief set 
\emph default
is a collection of subjective probability measures
\begin_inset Formula $\left(\mathcal{P}_{i}\right)_{i\in I}$
\end_inset

 such that
\end_layout

\begin_deeper
\begin_layout Enumerate
there exists a pricing function 
\begin_inset Formula $g_{p}:\,\Omega\times\mathbb{R}^{\mathbb{N}}\rightarrow\mathbb{R}^{K\backslash\left\{ k_{1}\right\} \times\mathbb{N}}$
\end_inset

 and an action function 
\begin_inset Formula $g_{y}:\,\Omega\times\mathbb{R}^{\mathbb{N}}\rightarrow\mathbb{R}^{J\times\mathbb{N}}$
\end_inset

 such that, for each agent 
\begin_inset Formula $i\in I$
\end_inset

, there exists a Markov kernel 
\begin_inset Formula $\tilde{\kappa}_{i}:\,\Omega\times\sigma\left(\mathbb{R}^{\mathbb{N}}\right)\rightarrow\left[0,1\right]$
\end_inset

 for which 
\begin_inset Formula $\mathcal{P}_{i}=\left(\mathcal{P}_{0}\otimes\kappa_{i}\right)\otimes\kappa_{g}$
\end_inset

 ,where 
\begin_inset Formula $\kappa_{g}$
\end_inset

 is a Markov kernel defined as 
\begin_inset Formula 
\[
\kappa_{g}:\,\left(\omega,\omega_{k_{1}},\mathcal{A}\right)=\begin{cases}
1 & \text{ if }\left(g_{p}\left(\omega,\omega_{k_{1}}\right),g_{y}\left(\omega,\omega_{k_{1}}\right)\right)\in\mathcal{A}\\
0 & \text{ otherwise}
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Enumerate
The price process defined by the pricing function 
\begin_inset Formula $p_{kt}^{e}:\,\left(\omega,\omega_{k_{1}}\right)\mapsto\left(p_{k_{0}t}\left(\omega_{k_{1}}\right),g_{p}\left(\omega,\omega_{k_{1}}\right)\right)$
\end_inset

 and the action process 
\begin_inset Formula $y_{kt}^{e}:\,\left(\omega,\omega_{k_{1}}\right)\mapsto g_{y}\left(\omega,\omega_{k_{1}}\right)$
\end_inset

 are 
\begin_inset Formula $\mathcal{F}_{t}$
\end_inset

-measurable; 
\end_layout

\begin_layout Enumerate
for each agent 
\begin_inset Formula $i\in I$
\end_inset

, there exists a solution 
\begin_inset Formula $\left(x_{kti},y_{ti}\right)$
\end_inset

 to the decision problem 
\begin_inset CommandInset ref
LatexCommand eqref
reference "eq:problem"

\end_inset

 under the belief measure 
\begin_inset Formula $\mathcal{P}_{i}$
\end_inset

 such that 
\end_layout

\begin_deeper
\begin_layout Enumerate
all competitive markets clear except for good 
\begin_inset Formula $k_{1}$
\end_inset

 and the numeraire 
\begin_inset Formula $k_{0}$
\end_inset

: 
\begin_inset Formula $\forall k\neq k_{0},k_{1}\forall t:\,\sum_{i}x_{kti}\leq0$
\end_inset

 almost surely with respect to 
\begin_inset Formula $\mathcal{P}_{i}$
\end_inset

;
\end_layout

\begin_layout Enumerate
actions coincide with beliefs about actions by everyone else: 
\begin_inset Formula $y_{ti}\left(\omega,f\left(\omega\right)\right)=y_{ti}^{e}\left(\omega\right)$
\end_inset

 for 
\begin_inset Formula $\mathcal{P}_{i^{\prime}}$
\end_inset

-almost all 
\begin_inset Formula $\omega\in\Omega$
\end_inset

 and 
\begin_inset Formula $i^{\prime}\in I$
\end_inset

.
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
An internally rational equilibrium with almost-consistent beliefs has the
 property that the equilibrium pricing function 
\begin_inset Formula $f$
\end_inset

 coincides with the pricing function 
\begin_inset Formula $g$
\end_inset

 under subjective beliefs on the equilibrium path: 
\begin_inset Formula $f\left(\omega\right)=\left(f_{k_{1}}\left(\omega\right),g\left(\omega,f_{k_{1}}\left(\omega\right)\right)\right)$
\end_inset

.
 In this sense, beliefs are close to rational expectations: Conditional
 on the price of good 
\begin_inset Formula $k_{1}$
\end_inset

 being on the equilibrium path, their beliefs about all other prices coincide
 with the equilibrium prices as well.
 The only difference to rational expectations is that agents do not know
 that the price of good 
\begin_inset Formula $k_{1}$
\end_inset

 will be such that markets clear, and entetertain subjective beliefs about
 it.
 The way to specify beliefs about other prices off the equilibrium path
 is by imposing consistency with a partial equilibrium in which all markets
 but those for 
\begin_inset Formula $k_{0}$
\end_inset

 and 
\begin_inset Formula $k_{1}$
\end_inset

 clear.
\end_layout

\begin_layout Subsection*
Application to the simple model
\end_layout

\begin_layout Standard
The simple model in the paper contains two agents - a representative household
 and a representative firm - and four goods: final goods, labour, debt and
 equity.
 The numeraire 
\begin_inset Formula $k_{0}$
\end_inset

 is the final good.
 All goods are traded in competitive markets.
 The only exogenous process is the productivity process:
\begin_inset Formula 
\begin{equation}
\log A_{t}=\log G+\log A_{t-1}+\varepsilon_{t},\,\varepsilon_{t}\sim\mathcal{N}\left(-\frac{\sigma^{2}}{2},\sigma^{2}\right)\, iid
\end{equation}

\end_inset

The household problem is as follows:
\begin_inset Formula 
\[
\max_{C_{t},S_{t},B_{t}}E_{0}^{\mathcal{P}}\sum_{t=0}^{\infty}\beta^{t}C_{t}
\]

\end_inset


\begin_inset Formula 
\begin{eqnarray*}
\text{s.t. }C_{t}+S_{t}V_{t}+B_{t} & = & w_{t}+S_{t-1}\left(V_{t}+D_{t}\right)+R_{t-1}B_{t-1}\\
S_{t} & \in & \left[0,\bar{S}\right],\, S_{-1},B_{-1}
\end{eqnarray*}

\end_inset

where 
\begin_inset Formula $V_{t}$
\end_inset

 is the price of equities, 
\begin_inset Formula $R_{t-1}$
\end_inset

 is the predetermined gross interest rate on bonds (alternatively, 
\begin_inset Formula $1/R_{t}$
\end_inset

 is the price of bonds today), 
\begin_inset Formula $w_{t}$
\end_inset

 is the wage rate and the price of consumption goods is normalised to one.
 
\end_layout

\begin_layout Standard
The firm problem is as follows: 
\begin_inset Formula 
\[
\max_{K_{t},L_{t},D_{t}}E_{0}^{\mathcal{P}}\sum_{t=0}^{\infty}\beta^{t}D_{t}
\]

\end_inset


\begin_inset Formula 
\begin{eqnarray*}
\text{s.t. }D_{t} & = & K_{t-1}^{\alpha}\left(A_{t}L_{t}\right)^{1-\alpha}-w_{t}L_{t}+\left(1-\delta-R_{t-1}\right)K_{t-1}\\
K_{t} & \leq & \frac{\xi}{1-\xi}V_{t}\\
K_{t} & \geq & 0,\, K_{-1}
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
Finally, the four market clearing conditions are given by: 
\begin_inset Formula 
\begin{eqnarray*}
C_{t}+K_{t}-\left(1-\delta\right)K_{t-1} & = & K_{t-1}^{\alpha}\left(A_{t}L_{t}\right)^{1-\alpha}\\
S_{t} & = & 1\\
B_{t} & = & K_{t}\\
L_{t} & = & 1
\end{eqnarray*}

\end_inset


\end_layout

\begin_layout Standard
The rational expectations equilibrium maps the fundamentals of the economy
 into prices as follows:
\begin_inset Formula 
\[
\left(\begin{array}{c}
V_{t}\\
R_{t}\\
w_{t}
\end{array}\right)=f\left(A_{t},A_{t-1}\right)=\left(\begin{array}{c}
\bar{v}_{RE}A_{t}\\
\bar{R}\\
\left(1-\alpha\right)\bar{k}_{RE}A_{t}^{1-\alpha}A_{t-1}^{\alpha}
\end{array}\right)
\]

\end_inset

Agents' beliefs about prices are consistent with this mapping - they expect
 prices to be exactly described by the process defined by the mapping 
\begin_inset Formula $f$
\end_inset

.
\end_layout

\begin_layout Standard
An internally rational equilibrium in the sense of AMN could have any arbitrary
 subjective belief structure about all three prices, but I impose almost-consist
ent beliefs up to beliefs about the price of equities 
\begin_inset Formula $V_{t}$
\end_inset

.
 It is instructive to look in more detail at the optimisation problems involved.
 Take the firm problem.
 The optimal choice of labour and the capital stock are determined by the
 first order conditions:
\begin_inset Formula 
\begin{eqnarray*}
w_{t} & = & \left(1-\alpha\right)\left(K_{t}/L_{t}\right)^{\alpha}A_{t}^{1-\alpha}\\
0 & \leq & \beta E_{t}^{\mathcal{P}}\left[\alpha\left(K_{t}/L{}_{t+1}\right)^{\alpha-1}A_{t+1}^{1-\alpha}+1-\delta-R_{t}\right]\\
K_{t} & \leq & \frac{\xi}{1-\xi}V_{t}
\end{eqnarray*}

\end_inset

and a complementary slackness condition for the last two inequalities.
 In particular, the firm needs to have a belief about both future productivity
 
\begin_inset Formula $A_{t+1}$
\end_inset

 and the wage rate 
\begin_inset Formula $w_{t+1}$
\end_inset

 to determine a contingent plan for its employment choice 
\begin_inset Formula $L_{t+1}$
\end_inset

 tomorrow, which in turn determines its optimal choice of capital today.
 Internal rationality already prescribes that agents have correct beliefs
 about fundmantals, in this case productiviy 
\begin_inset Formula $A_{t}$
\end_inset

.
 But one could still prescribe arbitrary beliefs about wages which would
 lead to possibly different choices in equilibrium.
 
\end_layout

\begin_layout Standard
Almost-consistent beliefs prescribe that the beliefs about interest rates
 and wages are consistent with market clearing in the bond and labour markets.
 To derive them, note that market clearing in the bond market prescribes
 
\begin_inset Formula $R_{t}=\beta^{-1}$
\end_inset

.
 Thus, almost-consistent beliefs will state that interest rates are constant
 and equal to their equilibrium value.
 Next, equilibrium in the labour market prescribes 
\begin_inset Formula $L_{t}=1$
\end_inset

.
 Substituting this condition into the optimal choice conditions of the firm
 leads to
\begin_inset Formula 
\begin{eqnarray*}
w_{t} & = & \left(1-\alpha\right)K_{t}^{\alpha}A_{t}^{1-\alpha}\\
K_{t} & = & K\left(A_{t},V_{t}\right)=\min\left\{ \frac{\xi}{1-\xi}V_{t},\left(\frac{\alpha\tilde{G}A_{t}^{1-\alpha}}{R_{t}-1+\delta}\right)^{\frac{1}{1-\alpha}}\right\} 
\end{eqnarray*}

\end_inset

This defines a pricing function for interest rates and wages under subjective
 beliefs as follows:
\begin_inset Formula 
\[
\left(\begin{array}{c}
R_{t}\\
w_{t}
\end{array}\right)=g_{p}\left(A_{t},A_{t-1},V_{t-1}\right)=\left(\begin{array}{c}
\beta^{-1}\\
\left(1-\alpha\right)K\left(A_{t-1},V_{t-1}\right)^{\alpha}A_{t}^{1-\alpha}
\end{array}\right)
\]

\end_inset

The firm only cares about prices, but the household, in pricing equity,
 also cares about the actions taken by the firm, because this matters for
 dividend payouts.
 It is easiest in this context to define the dividend payout 
\begin_inset Formula $D_{t}$
\end_inset

 as the single action in the sense of the definitions above.
 Again, it would be possible to specify arbitrary subjective beliefs about
 dividends, but when beliefs are almost-consistent, then beliefs are pinned
 down by the action function:
\begin_inset Formula 
\begin{eqnarray*}
D_{t} & = & g_{y}\left(A_{t},A_{t-1},V_{t-1}\right)\\
 & = & \alpha K\left(A_{t-1},V_{t-1}\right)^{\alpha}A_{t}^{1-\alpha}+\left(1-\delta-\beta^{-1}\right)K\left(A_{t-1},V_{t-1}\right)
\end{eqnarray*}

\end_inset

The expected return on equity under almost-consistent beliefs is then
\begin_inset Formula 
\[
E_{t}^{\mathcal{P}}\left[\frac{V_{t+1}+D_{t+1}}{V_{t}}\right]=\exp\left(\hat{\mu}_{t}\right)+\frac{E_{t}g_{y}\left(A_{t+1},A_{t},V_{t}\right)}{V_{t}}
\]

\end_inset

On the equilibrium path of the internally rational equilibrium with almost-consi
stent beliefs, this return has to be equal to the inverse of the discount
 factor, which can be used to back out actual prices 
\begin_inset Formula $V_{t}$
\end_inset

 and the law of motion for beliefs 
\begin_inset Formula $\hat{\mu}_{t}$
\end_inset

 about prices.
\end_layout

\end_body
\end_document
